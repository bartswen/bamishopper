Optional
--------
    - tune BlockingSyncService MAX_LOCK_TIME_SEC n.a.v. langste sync actie
    - Performance BoLiXmlParser streaming instead of String
    - Bug: move item in shop works but producess unnecessary <shopItemMoved>...<oldIndex>1</oldIndex><newIndex>1</newIndex></shopItemMoved>
    - Landscape
    - Fix exception causes local data lost
    - Improve drag and drop in shop (workaround: edit bamishopper.xml then full pull on all devices)
    - Test events and visitor methods in boli and event.compensate()
    - BoodschappenActivity at item long click in empty area shows context menu with 'New Boodschap'
    - Translate to EN (largely done)
        - Naming EN for classes, packages, methods, 
    - Do not restart MainActivity if data changes (after an undo action)
             Move main menu from MainActivity to generic class and reference it from MakeList- and ShoppingActivity
             The activity can now call notifyDataSetChanged on its adapter
    - Improve style
         http://developer.android.com/guide/topics/ui/themes.html
    - Gracefully handle unfixable errors in boli xml 
    - Avoid using "px" as units; use "dp" instead
    - BoliFixer enhancements
    - Dropdown items are clickable, not only the name, see long click menu shopping item OK
    - Abstract id generator from BoLi, separate concern, improves testability

Release 1.5 - No crash when syncing without network, new item in shop position
-------
    v Position of item in shop before first shop-item in same category
        - on add item to shop
        - also adjust position if item-category changes? no
    v fix sync without network foutafhandeling
        v fix no network sync crash
                !? Use function to convert shared to syncexc ==> 
                    02-10 21:56:47.929: E/bs.bamishopper.sync.DropboxShareService(3811): com.dropbox.client2.exception.DropboxSSLException: 
                    javax.net.ssl.SSLPeerUnverifiedException: No peer certificate
    v Bug: Delete item does not generate remove from shop events
        - Undo delete item is broken, does not restore shop-bindings
    v Undo stacksize 10 and improved notification
    v events short toString; use in popups, zie comp error
    v No 'consumed event' notification after undo
    v Clean start: clear events initial shoppinglist
    x Rename and delete shop
    x Show categories in shop

Release 1.4 - Bugfix new item disappears from shop
-------
    v Bug nieuw item verdwijnt uit winkel soms: bug in boli.merge(), fix references from bs and wnk to boli

Release 1.3 - Smart merge improvements
-------
    v peek before lock: if no events exist and nothing to share then dont lock
    v Fix SyncServiceTest, hangt in Wait.until
    v Fix logging in BlockingSyncService en Wait, uncomment en abstraheer android logger, SyncServiceTest moet slagen (rename)
    v Aggregation of all notifications in one sync action
    v bug: unjustified itemCategoryChanged if no category
    v Statistics in sync dialog: laatste sync actie, langste sync actie
    v clean up events periodically if sync disabled
    v refresh screen after full pull shoppinglist
    v clear undo- and publish-events after full pull
    v boli.unregisterEventsProcessedListener arg verwijderen

Release 1.2 - Initial smart merge
-------
    v Hourglass visible during full Dropbox sync
    v Smart merge initial
         v sync service abstraction
         v subscribe/unsubscribe in bamishopper-subscriptions.txt in SyncService
             v SyncService verwijderen, SyncServiceTest fix
         v implement smart sync
             v push
                 v push in onPause () eerst alleen MainActivity
                     - append phone id in events-file name bamishopper-events-ss1293821.xml
                 v always write bamishopper.xml (without events)
             v pull
                 - append phone id in events-file name bamishopper-events-ss1293821.xml
                     - means that ss1293821 has processed the events-file
                 v if no remaining subscribers remove events-file 
                 v else append phone name to events-file name
         v lock dropbox folder
         v synchronize in background
         v subscribe en unsubscribe moeten synchroon
         v reinit bamishopperfactory na subscribe en unsubscribe in prefsactivity
         v share pull en sync retourneren boolean geslaagd/locked-aborted
         v test offline (un)subscribe en sync
         v remove push, include sync function in Synchronize view
         v refresh screen after sync
             v registreer change listeners op BoLi
             v refresh view from same thread
             v test background sync refreshes view
         v notificaties naar gebruiker bij updates

Release 1.1 - Bugfix
-------
    v Fix bug NPE on change shop
    v Device-id in about box

Release 1.0 - major release, niet backward compatible
-------
    v Simplify XML format bamischopper.xml
    v Implement events
    v Local (contains undo stack and events) and shared bamishopper.xml format
    v Undo
    v Use ids
    v Migrate: converter to convert bamishopper.xml to new format once

Release 0.4:
-------
    v Refactor: all preference keys in android resource values/strings
    v Store current position in shopping-items and shopping lists, 
    v Jump to shopping item after added new
    v Store active shop
    v Text inputfields single-line, action key press (enter) closes keyboard
    v Adding existing shoppingitem or shop

Release 0.3:
-------
    v Update view na sync
    v Change Local storage dir en file name from subo/subo.xml to BaMiShopper/bamishopper.xml
    v Fix persistence, save no longer corrupts file when interrupted during app kill
    v Remove auto sort while shopping

Release 0.2:
-------
    v Order items while shopping
    v About box, shows version and build
    v Dropbox sync on/off preference, default off
    v Store Dropbox key

Release 0.1:
-------
    v Naam: BaMiShopper
    v Select bs for a shop
    v Multiplayer basic sync dropbox

Dropped
-------
    x integreer bolimanager in bamishopperfactory; never mind
    x createinitialboli hoort niet in bolipersister; never mind
    x In Android Market / Play store
         - Code obfuscate (protects dropbox appkeypair)
         - Encrypt DB credentials in prefs (protects dropbox user accesstokenpair)
    x full pull after subscribe. cannot do this, .BamiShopper dir possibly not yet shared
    x use xmlunit library for xml diffs in BoLiXmlParserTest
    x manage conflicts in the boli processEvent; known issue, errors may occasionally occur
    x prevent pending events after unsubscribe phone [not a problem, will probably consume events already when preferences opens]
    x create bound service with periodical initially dummy syncing in the background
    x Done button in preferences
    
