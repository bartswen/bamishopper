package bs.beandiff;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class CollectionSizeDifference extends Difference {
    private final int leftSize, richtSize;

    public CollectionSizeDifference(int leftSize, int richtSize) {
        super();
        this.leftSize = leftSize;
        this.richtSize = richtSize;
    }

    @Override
    public int size() {
        return 1;
    }
    
    @Override
    public String toString() {
        return super.toString();
    }

@Override
    public Set<String> toStringDiff() {
        return new HashSet<String>(Arrays.asList(String.format("size[left:%d, right:%d]", leftSize, richtSize)));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + leftSize;
        result = prime * result + richtSize;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CollectionSizeDifference other = (CollectionSizeDifference) obj;
        if (leftSize != other.leftSize)
            return false;
        if (richtSize != other.richtSize)
            return false;
        return true;
    }
}
