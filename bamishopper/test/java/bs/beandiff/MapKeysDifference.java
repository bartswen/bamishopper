package bs.beandiff;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Transformer;

public class MapKeysDifference extends Difference {
    private final CollectionDifference keysDiff;

    public MapKeysDifference(CollectionDifference keysDiff) {
        this.keysDiff = keysDiff;
    }

    @Override
    public int size() {
        return keysDiff.size();
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public Set<String> toStringDiff() {
        Transformer prependMessage = new Transformer() {
            public Object transform(Object input) {
                String keyDiffStr = (String) input;
                return "map key difference, " + keyDiffStr;
            }
        };

        final Set<String> diffStrings = new HashSet<String>(CollectionUtils.collect(keysDiff.toStringDiff(), prependMessage));

        return diffStrings;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((keysDiff == null) ? 0 : keysDiff.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        MapKeysDifference other = (MapKeysDifference) obj;
        if (keysDiff == null) {
            if (other.keysDiff != null)
                return false;
        } else if (!keysDiff.equals(other.keysDiff))
            return false;
        return true;
    }
}
