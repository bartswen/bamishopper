package bs.beandiff;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

public class ValueDifference extends Difference {
    private static final int MAX_CONTEXT_LENGTH = 20;
    private final Object left, right;

    /**
     * @param left can be null
     * @param right can be null
     */
    public <T> ValueDifference(T left, T right) {
        this.left = left;
        this.right = right;
    }

    @Override
    public int size() {
        return 1;
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public Set<String> toStringDiff() {
        final Pair<String, String> values;
        if (left instanceof String) {
            values = new StringComparisonCompactor(MAX_CONTEXT_LENGTH, (String) left, (String) right).compact();
        } else {
            values = Pair.of(toString(left), toString(right));
        }
        return new HashSet<String>(Arrays.asList(String.format("value mismatch[left: %s, right: %s]",
                toStringQuoteIfBlank(values.getLeft()), toStringQuoteIfBlank(values.getRight()))));
    };

    /** Allows null */
    private static String toString(Object o) {
        if (o == null) {
            return null;
        } else {
            return o.toString();
        }
    }

    private static String toStringQuoteIfBlank(Object o) {
        if (o == null) {
            return "<null>";
        }
        String s = o.toString();
        return StringUtils.isBlank(s) ? "'" + s + "'" : s;
    }

    // @Override
    public int hashCodeOld() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((left == null) ? 0 : left.hashCode());
        result = prime * result + ((right == null) ? 0 : right.hashCode());
        return result;
    }

    // @Override
    public boolean equalsOld(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ValueDifference other = (ValueDifference) obj;
        if (left == null) {
            if (other.left != null)
                return false;
        } else if (!left.equals(other.left))
            return false;
        if (right == null) {
            if (other.right != null)
                return false;
        } else if (!right.equals(other.right))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        if (left.getClass().isArray() && right.getClass().isArray()) {
            result = prime * result + Arrays.hashCode((Object[]) left);
            result = prime * result + Arrays.hashCode((Object[]) right);
        } else if (!left.getClass().isArray() && !right.getClass().isArray()) {
            result = prime * result + ((left == null) ? 0 : left.hashCode());
            result = prime * result + ((right == null) ? 0 : right.hashCode());
        } else {
            throw new AssertionError();
        }
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ValueDifference other = (ValueDifference) obj;
        if (left != null && left.getClass().isArray()) {
            if (!Arrays.equals((Object[]) left, (Object[]) other.left))
                return false;
            if (!Arrays.equals((Object[]) right, (Object[]) other.right))
                return false;
        } else {
            if (left == null) {
                if (other.left != null)
                    return false;
            } else if (!left.equals(other.left))
                return false;
            if (right == null) {
                if (other.right != null)
                    return false;
            } else if (!right.equals(other.right))
                return false;
        }
        return true;
    }
}
