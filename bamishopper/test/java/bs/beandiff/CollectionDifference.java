package bs.beandiff;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.collections.Closure;
import org.apache.commons.collections.CollectionUtils;

/**
 * Describes differences in objects of a Collection subtype where only Collection semantics are relevant for
 * the difference.
 * 
 * <p>
 * For example a Set. There is no index, mapping or anything. There is nothing special in the description of
 * the differences in two sets, it describes differences in just two collections.
 */
public class CollectionDifference extends Difference {
    final private Collection missingInLeft;
    final private Collection missingInRight;

    public CollectionDifference() {
        super();
        missingInLeft = new ArrayList();
        missingInRight = new ArrayList();
    }

    public CollectionDifference(Collection<?> misInLeft, Collection<?> misInRight) {
        missingInLeft = misInLeft;
        missingInRight = misInRight;
    }

    public void addMissingInRight(Object o) {
        missingInRight.add(o);
    }

    public void addMissingInLeft(Object o) {
        missingInLeft.add(o);
    }

    @Override
    public int size() {
        return missingInLeft.size()+missingInRight.size();
    }
    
    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public Set<String> toStringDiff() {
        final Set<String> diffStrings = new HashSet<String>(missingInRight.size() + missingInLeft.size());

        Closure misInRightToStringClosure = new Closure() {
            public void execute(Object input) {
                diffStrings.add(String.format("missing in right: %s", input));
            }
        };

        Closure misInLeftToStringClosure = new Closure() {
            public void execute(Object input) {
                diffStrings.add(String.format("missing in left: %s", input));
            }
        };

        CollectionUtils.forAllDo(missingInRight, misInRightToStringClosure);
        CollectionUtils.forAllDo(missingInLeft, misInLeftToStringClosure);

        return diffStrings;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((missingInLeft == null) ? 0 : missingInLeft.hashCode());
        result = prime * result + ((missingInRight == null) ? 0 : missingInRight.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CollectionDifference other = (CollectionDifference) obj;
        if (missingInLeft == null) {
            if (other.missingInLeft != null)
                return false;
        } else if (!missingInLeft.equals(other.missingInLeft))
            return false;
        if (missingInRight == null) {
            if (other.missingInRight != null)
                return false;
        } else if (!missingInRight.equals(other.missingInRight))
            return false;
        return true;
    }
}
