package bs.beandiff;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.Closure;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Transformer;

public class MapValuesDifference extends Difference {
    /** All map entry differences. Maps keys to difference. */
    private Map<Object, Difference> diffs = new HashMap<Object, Difference>();
    
    public MapValuesDifference(Map<Object, Difference> diffs) {
        super();
        this.diffs = diffs;
    }

    public MapValuesDifference() {
        super();
    }

    public void addChild(Object mapKey, Difference diff) {
        diffs.put(mapKey, diff);
    }

    @Override
    public int size() {
        return diffs.size();
    }
    
    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public Set<String> toStringDiff() {
        final Set<String> mapEntriesDiffs = new HashSet<String>(diffs.size());
        
        Closure mapEntryToStringClosure = new Closure() {
            public void execute(final Object key) {
                Difference diff = diffs.get(key);
                Set<String> mapEntryDiffs = diff.toStringDiff();
                
                Transformer prependObjectToDiff = new Transformer() {
                    public Object transform(Object input) {
                        String mapEntryDiff = (String) input;
                        return key.toString() + "-->" + mapEntryDiff;
                    }
                };
                
                Collection qualifiedMapEntryDiffs = CollectionUtils.collect(mapEntryDiffs, prependObjectToDiff);
                
                mapEntriesDiffs.addAll(qualifiedMapEntryDiffs);
            }
        };
        
        CollectionUtils.forAllDo(diffs.keySet(), mapEntryToStringClosure);
        
        return mapEntriesDiffs;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((diffs == null) ? 0 : diffs.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        MapValuesDifference other = (MapValuesDifference) obj;
        if (diffs == null) {
            if (other.diffs != null)
                return false;
        } else if (!diffs.equals(other.diffs))
            return false;
        return true;
    }
}
