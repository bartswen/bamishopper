package bs.beandiff;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import junit.framework.Assert;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.ListUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.collections.SetUtils;
import org.junit.Test;

import bs.beandiff.BeanDiff.Builder;

/**
 * Compares two objects by their properties or equals method returning an object representing the difference
 * between the two objects.
 * <p>
 * Use the <b>static compare()</b> for simple comparison using equals method of the java beans. Use the
 * <b>BeanDiffTest.Builder</b> to configure the BeanDiff to do a properties-compare for specific packages.
 * 
 */
public class BeanDiffTest {

    /** Uses Object.equals() */
    public static class PersonNoEquals {
        private String name;

        public PersonNoEquals(String name) {
            this.name = name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        @Override
        public String toString() {
            return "Person [name=" + name + "]";
        }
    }

    /* Needs to be public to be a readable javabean */
    public static class PersonImplementingEquals {
        private String name;

        public PersonImplementingEquals(String name) {
            this.name = name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        @Override
        public String toString() {
            return "Person [name=" + name + "]";
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((name == null) ? 0 : name.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            PersonImplementingEquals other = (PersonImplementingEquals) obj;
            if (name == null) {
                if (other.name != null)
                    return false;
            } else if (!name.equals(other.name))
                return false;
            return true;
        }
    }

    /* Needs to be public to be a readable javabean */
    public static class ListContainer {
        public List<?> items;

        public List<?> getItems() {
            return items;
        }
    }

    public static class SelfReferrer {
        private final String name;
        private SelfReferrer sr;

        public SelfReferrer(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public SelfReferrer getSelfReferrer() {
            return sr;
        }

        public void setSelfReferrer(SelfReferrer sr) {
            this.sr = sr;
        }
    }

    public static class BeanWithIndexedProp {
        public Object getIndexedProp(int index) {
            return null;
        }
    }

    public enum AddressType {
        SHIPPING, BILLING;
    }

    @Test
    public void testCompareByIntrospectionEqual() {
        PersonNoEquals left = new PersonNoEquals("Chelsea");
        PersonNoEquals right = new PersonNoEquals("Chelsea");

        BeanDiff sut = new BeanDiff.Builder().introspectPackagesRegexes(PersonNoEquals.class.getPackage().getName() + ".*")
                .build();
        Difference actual = sut.compare(left, right);

        Assert.assertNull(actual);
    }

    @Test
    public void testCompareByIntrospectionNotEqual() {
        PersonNoEquals left = new PersonNoEquals("Chelsea");
        PersonNoEquals right = new PersonNoEquals("Karleigh");

        BeanDiff sut = new BeanDiff.Builder().introspectPackagesRegexes(PersonNoEquals.class.getPackage().getName() + ".*")
                .build();
        Difference actual = sut.compare(left, right);

        BeanDifference expected = new BeanDifference();
        expected.addFieldDiff("name", new ValueDifference("Chelsea", "Karleigh"));
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCompareByEqualsNotEqual() {
        PersonNoEquals left = new PersonNoEquals("Chelsea");
        PersonNoEquals right = new PersonNoEquals("Chelsea");

        Difference actual = BeanDiff.compareUsingEquals(left, right);

        Difference expected = new ValueDifference(left, right);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCompareByEqualsEqual() {
        PersonImplementingEquals left = new PersonImplementingEquals("Chelsea");
        PersonImplementingEquals right = new PersonImplementingEquals("Chelsea");

        Difference actual = BeanDiff.compareUsingEquals(left, right);

        Assert.assertNull(actual);
    }

    @Test
    public void testCompareListsSizeDiffer() {
        List<String> left = Arrays.asList("aaa", "bbb", "ccc");
        List<String> right = Arrays.asList("aaa", "bbb");

        Difference actual = BeanDiff.compareUsingEquals(left, right);

        CollectionSizeDifference expected = new CollectionSizeDifference(3, 2);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCompareListsRightNull() {
        List<String> left = Arrays.asList("item");
        List<String> right = null;

        Difference actual = BeanDiff.compareUsingEquals(left, right);

        ValueDifference expected = new ValueDifference(Arrays.asList("item"), null);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCompareListsTwoItemsNotEqual() {
        List<String> left = Arrays.asList("aaa", "bbb", "ccc");
        List<String> right = Arrays.asList("aaa", "ccc", "bbb");

        Difference actual = BeanDiff.compareUsingEquals(left, right);

        ListDifference expected = new ListDifference();
        expected.addDiff(1, new ValueDifference("bbb", "ccc"));
        expected.addDiff(2, new ValueDifference("ccc", "bbb"));
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCompareListsTypesDontMatch() {
        ListContainer l = new ListContainer();
        l.items = Arrays.asList("aaa");

        ListContainer r = new ListContainer();
        r.items = Arrays.asList(new Integer(1));

        BeanDiff sut = new BeanDiff.Builder().introspectPackagesRegexes(ListContainer.class.getPackage().getName() + ".*")
                .build();
        Difference actual = sut.compare(l, r);

        Difference td = new TypeDifference(String.class, Integer.class);
        ListDifference ld = new ListDifference();
        ld.addDiff(0, td);
        BeanDifference expectedBD = new BeanDifference();
        expectedBD.addFieldDiff("items", ld);
        Assert.assertEquals(expectedBD, actual);
    }

    @Test
    public void testCompareSetsSizeDiffer() {
        Set<String> left = new HashSet<String>(Arrays.asList("aaa", "bbb", "ccc"));
        Set<String> right = new HashSet<String>(Arrays.asList("aaa", "bbb"));

        Difference actual = BeanDiff.compareUsingEquals(left, right);

        CollectionSizeDifference expected = new CollectionSizeDifference(3, 2);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCompareSetsRightNull() {
        Set<String> left = new HashSet<String>(Arrays.asList("item"));
        Set<String> right = null;

        Difference actual = BeanDiff.compareUsingEquals(left, right);

        ValueDifference expected = new ValueDifference(new HashSet<String>(Arrays.asList("item")), null);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCompareSetsEqual() {
        Set<String> left = new HashSet<String>(Arrays.asList("aaa", "bbb", "ccc"));
        Set<String> right = new HashSet<String>(Arrays.asList("aaa", "ccc", "bbb"));

        Difference actual = BeanDiff.compareUsingEquals(left, right);

        Assert.assertNull(actual);
    }

    @Test
    public void testCompareSetsNotEqual() {
        Set<String> left = new HashSet<String>(Arrays.asList("bbb", "bbb", "ccc"));
        Set<String> right = new HashSet<String>(Arrays.asList("bbb", "bbb", "ddd"));

        Difference actual = BeanDiff.compareUsingEquals(left, right);

        CollectionDifference expected = new CollectionDifference(Arrays.asList("ddd"), Arrays.asList("ccc"));
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCompareMapsRightNull() {
        Map<String, String> left = MapUtils.putAll(new HashMap<String, String>(), new String[] { "akey", "avalue" });
        Map<String, String> right = null;

        Difference actual = BeanDiff.compareUsingEquals(left, right);

        ValueDifference expected = new ValueDifference(MapUtils.putAll(new HashMap<String, String>(), new String[] { "akey",
                "avalue" }), null);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCompareMapsEqual() {
        Map<String, String> left = MapUtils.putAll(new HashMap<String, String>(), new String[] { "akey", "avalue", "bkey",
                "bvalue" });
        Map<String, String> right = MapUtils.putAll(new HashMap<String, String>(), new String[] { "akey", "avalue", "bkey",
                "bvalue" });

        Difference actual = BeanDiff.compareUsingEquals(left, right);

        Assert.assertNull(actual);
    }

    @Test
    public void testCompareMapsNotEqualSize() {
        Map<String, String> left = MapUtils.putAll(new HashMap<String, String>(), new String[] { "akey", "avalue", "bkey",
                "bvalue" });
        Map<String, String> right = MapUtils.putAll(new HashMap<String, String>(), new String[] { "akey", "avalue" });

        Difference actual = BeanDiff.compareUsingEquals(left, right);

        CollectionSizeDifference expected = new CollectionSizeDifference(2, 1);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCompareMapsWithDifferentKeys() {
        Map<String, String> left = MapUtils.putAll(new HashMap<String, String>(), new String[] { "akey", "avalue", "bkey",
                "bvalue" });
        Map<String, String> right = MapUtils.putAll(new HashMap<String, String>(), new String[] { "akey", "avalue", "Xkey",
                "bvalue" });

        Difference actual = BeanDiff.compareUsingEquals(left, right);

        MapKeysDifference expected = new MapKeysDifference(new CollectionDifference(Arrays.asList("Xkey"), Arrays.asList("bkey")));

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCompareMapsWithDifferentValues() {
        Map<String, String> left = MapUtils.putAll(new HashMap<String, String>(), new String[] { "akey", "avalue", "bkey",
                "bvalue" });
        Map<String, String> right = MapUtils.putAll(new HashMap<String, String>(), new String[] { "akey", "avalue", "bkey",
                "Xvalue" });

        Difference actual = BeanDiff.compareUsingEquals(left, right);

        MapValuesDifference expected = new MapValuesDifference();
        expected.addChild("bkey", new ValueDifference("bvalue", "Xvalue"));

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCompareMapsWithNullKey() {
        Map<String, String> left = MapUtils.putAll(new HashMap<String, String>(), new String[] { null, "avalue" });
        Map<String, String> right = MapUtils.putAll(new HashMap<String, String>(), new String[] { "akey", "avalue" });

        Difference actual = BeanDiff.compareUsingEquals(left, right);

        ArrayList<String> expMisInRight = new ArrayList<String>();
        expMisInRight.add(null);
        MapKeysDifference expected = new MapKeysDifference(new CollectionDifference(Arrays.asList("akey"), expMisInRight));

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCompareMapsWithNullValue() {
        Map<String, String> left = MapUtils.putAll(new HashMap<String, String>(), new String[] { "akey", null });
        Map<String, String> right = MapUtils.putAll(new HashMap<String, String>(), new String[] { "akey", "avalue" });

        Difference actual = BeanDiff.compareUsingEquals(left, right);

        ArrayList<String> expMisInRight = new ArrayList<String>();
        expMisInRight.add(null);
        MapValuesDifference expected = new MapValuesDifference(MapUtils.putAll(new HashMap<String, Difference>(), new Object[] {
                "akey", new ValueDifference(null, "avalue") }));

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCompareArraysSizeDiffer() {
        String[] left = { "aaa", "bbb", "ccc" };
        String[] right = { "aaa", "bbb" };

        Difference actual = BeanDiff.compareUsingEquals(left, right);

        CollectionSizeDifference expected = new CollectionSizeDifference(3, 2);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCompareArraysRightNull() {
        String[] left = { "item" };
        String[] right = null;

        Difference actual = BeanDiff.compareUsingEquals(left, right);

        ValueDifference expected = new ValueDifference(new String[] { "item" }, null);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCompareArraysTwoItemsNotEqual() {
        String[] left = { "aaa", "bbb", "ccc" };
        String[] right = { "aaa", "ccc", "bbb" };

        Difference actual = BeanDiff.compareUsingEquals(left, right);

        ListDifference expected = new ListDifference();
        expected.addDiff(1, new ValueDifference("bbb", "ccc"));
        expected.addDiff(2, new ValueDifference("ccc", "bbb"));
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCompareEnumsNotEqual() {
        AddressType left = AddressType.BILLING;
        AddressType right = AddressType.SHIPPING;

        Difference actual = BeanDiff.compareUsingEquals(left, right);

        Object expected = new ValueDifference(AddressType.BILLING, AddressType.SHIPPING);
        Assert.assertEquals(expected, actual);

    }

    @Test
    public void testCompareEnumsEqual() {
        AddressType left = AddressType.BILLING;
        AddressType right = AddressType.BILLING;

        Difference actual = BeanDiff.compareUsingEquals(left, right);

        Assert.assertNull(actual);

    }

    @Test
    public void testCompareEnumsRightNull() {
        AddressType left = AddressType.BILLING;
        AddressType right = null;

        Difference actual = BeanDiff.compareUsingEquals(left, right);

        Object expected = new ValueDifference(AddressType.BILLING, null);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testIgnoresecondEncounter() {
        SelfReferrer left = new SelfReferrer("left");
        left.setSelfReferrer(left);

        SelfReferrer right = new SelfReferrer("right");
        right.setSelfReferrer(right);

        BeanDiff sut = new BeanDiff.Builder().introspectPackagesRegexes(SelfReferrer.class.getPackage().getName() + ".*").build();
        Difference actual = sut.compare(left, right);

        BeanDifference expected = new BeanDifference();
        expected.addFieldDiff("name", new ValueDifference("left", "right"));
        Assert.assertEquals(expected, actual);
    }

    @Test(expected = RuntimeException.class)
    public void testBuilderPropertyRegexMatchesExcludeClassnameFailNoRegex() {
        // missing call to introspectPackagesRegexes
        new BeanDiff.Builder().excludeProperty(Object.class, "dummy").build();
    }

    @Test(expected = RuntimeException.class)
    public void testBuilderPropertyRegexMatchesExcludeClassnameFailNoMatchingRegex() {
        Builder sut = new BeanDiff.Builder();
        sut.introspectPackagesRegexes("java.lanX");
        sut.excludeProperty(Object.class, "dummy").build();
        sut.build();
    }

    @Test
    public void testBuilderPropertyRegexMatchesExcludeClassnameSuccess() {
        Builder sut = new BeanDiff.Builder();
        sut.introspectPackagesRegexes("java.lang.*");
        sut.excludeProperty(Object.class, "dummy");
        try {
            sut.build();
        } catch (RuntimeException e) {
            Assert.fail("No runtime expected: " + e);
        }
    }

    @Test
    public void testCompareIgnoreProperty() {
        PersonNoEquals left = new PersonNoEquals("Chelsea");
        PersonNoEquals right = new PersonNoEquals("Karleigh");
        BeanDiff.Builder b = new BeanDiff.Builder();
        b.introspectPackagesRegexes(PersonNoEquals.class.getPackage().getName() + ".*");
        b.excludeProperty(PersonNoEquals.class, "name");
        BeanDiff sut = b.build();

        Difference actual = sut.compare(left, right);

        Assert.assertNull(actual);
    }

    @Test
    public void testIgnoresIndexedProperties () {
        BeanWithIndexedProp left=new BeanWithIndexedProp();
        BeanWithIndexedProp right=new BeanWithIndexedProp();
        BeanDiff sut = new BeanDiff.Builder().introspectPackagesRegexes(BeanWithIndexedProp.class.getPackage().getName()+".*").build();
        sut.compare(left, right);
        
    }
}
