package bs.beandiff;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.Closure;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Transformer;

/**
 * Describes the difference between two ordered collections, e.g. List or array.
 */
public class ListDifference extends Difference {
    final private Map<Integer, Difference> listDiffs;

    public ListDifference(Map<Integer, Difference> listDiffs) {
        this.listDiffs=listDiffs;
    }

    public ListDifference() {
        listDiffs = new HashMap<Integer, Difference>();
    }

    public void addDiff(Integer sourceIndex, Difference diff) {
        listDiffs.put(sourceIndex, diff);
    }

    @Override
    public int size() {
        return listDiffs.size();
    }
    
    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public Set<String> toStringDiff() {
        final Set<String> diffStrings = new HashSet<String>(listDiffs.size());

        Closure listDiffToStringClosure = new Closure() {
            public void execute(Object input) {
                final Integer srcIndex = (Integer) input;
                Difference diffAtIndex = listDiffs.get(srcIndex);
                Set<String> fieldDiffs = diffAtIndex.toStringDiff();

                Transformer prependIndexToDiff = new Transformer() {
                    public Object transform(Object input) {
                        String fieldDiff = (String) input;
                        return String.format("[%d].%s", srcIndex, fieldDiff);
                    }
                };

                Collection qualifiedListDiffs = CollectionUtils.collect(fieldDiffs, prependIndexToDiff);

                diffStrings.addAll(qualifiedListDiffs);
            }
        };

        CollectionUtils.forAllDo(listDiffs.keySet(), listDiffToStringClosure);

        return diffStrings;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((listDiffs == null) ? 0 : listDiffs.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ListDifference other = (ListDifference) obj;
        if (listDiffs == null) {
            if (other.listDiffs != null)
                return false;
        } else if (!listDiffs.equals(other.listDiffs))
            return false;
        return true;
    }
}
