package bs.beandiff;

import org.apache.commons.lang3.tuple.Pair;

/** Inspired by JUnit */
public class StringComparisonCompactor {
    private static final String ELLIPSIS= "...";
    private static final String DELTA_END= "]";
    private static final String DELTA_START= "[";
    
    private int fContextLength;
    private String left;
    private String right;
    private int fPrefix;
    private int fSuffix;

    public StringComparisonCompactor(int contextLength, String expected, String actual) {
        fContextLength= contextLength;
        left= expected;
        right= actual;
    }

    public Pair<String, String> compact() {
        if (left == null || right == null || areStringsEqual())
            return Pair.of(left, right);

        findCommonPrefix();
        findCommonSuffix();
        String leftCompact= compactString(left);
        String rightCompact= compactString(right);
        return Pair.of(leftCompact, rightCompact);
    }

    private String compactString(String source) {
        String result= DELTA_START + source.substring(fPrefix, source.length() - fSuffix + 1) + DELTA_END;
        if (fPrefix > 0)
            result= computeCommonPrefix() + result;
        if (fSuffix > 0)
            result= result + computeCommonSuffix();
        return result;
    }

    private void findCommonPrefix() {
        fPrefix= 0;
        int end= Math.min(left.length(), right.length());
        for (; fPrefix < end; fPrefix++) {
            if (left.charAt(fPrefix) != right.charAt(fPrefix))
                break;
        }
    }

    private void findCommonSuffix() {
        int expectedSuffix= left.length() - 1;
        int actualSuffix= right.length() - 1;
        for (; actualSuffix >= fPrefix && expectedSuffix >= fPrefix; actualSuffix--, expectedSuffix--) {
            if (left.charAt(expectedSuffix) != right.charAt(actualSuffix))
                break;
        }
        fSuffix=  left.length() - expectedSuffix;
    }

    private String computeCommonPrefix() {
        return (fPrefix > fContextLength ? ELLIPSIS : "") + left.substring(Math.max(0, fPrefix - fContextLength), fPrefix);
    }

    private String computeCommonSuffix() {
        int end= Math.min(left.length() - fSuffix + 1 + fContextLength, left.length());
        return left.substring(left.length() - fSuffix + 1, end) + (left.length() - fSuffix + 1 < left.length() - fContextLength ? ELLIPSIS : "");
    }

    private boolean areStringsEqual() {
        return left.equals(right);
    }

}
