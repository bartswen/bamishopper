package bs.beandiff;

import java.beans.IndexedPropertyDescriptor;
import java.beans.PropertyDescriptor;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.collections.Closure;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.collections.Transformer;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

/**
 * Compares two objects by introspection and returns the difference.
 * <p>
 * About <b>compare</b>. The term compare in the context of the beandiff library has little to do with the
 * general Java compare as it is used in classes like Comparator. Java compare is all about ordering while the
 * Beandiff compare returns the difference between objects.
 * <p>
 * About <b>equal en equivalent</b>. We need to explicitly distinguish between equal en equivalent because
 * these terms mean something different in the context of this BeanDiff library. Equal means the equal method
 * returns true. Equivalent means that the BeanDiff compare returns no difference.
 * 
 */
public final class BeanDiff {

    private static String REGEX_POSTFIX = ".*";

    public static class Builder {
        private final Collection<String> ipr = new ArrayList<String>();
        private final Map<Class<?>, Set<String>> excludeProperties = new HashMap<Class<?>, Set<String>>();

        /**
         * Package that the BeanDiff should compare by means of introspection instead of equals().
         */
        public Builder introspectPackage(Package pkg) {
            this.ipr.addAll(Arrays.asList(pkg.getName() + REGEX_POSTFIX));
            return this;
        }

        /**
         * Regular expressions matching the packages or qualified classnames that the BeanDiff should compare
         * by means of introspection instead of equals().
         */
        public Builder introspectPackagesRegexes(String... regexes) {
            this.ipr.addAll(Arrays.asList(regexes));
            return this;
        }

        /**
         * Specify a property to be excluded from the comparison. The qualified name of the class must match a
         * regular expression passed to introspectPackagesRegexes(). In other words, to be able to exclude
         * properties from the comparison, the class must be configured to be compared by introspection.
         * <p>
         * Can be invoked multiple times, possibly passing the same class.
         */
        public Builder excludeProperty(Class<?> clazz, String propertyname) {
            Set<String> propnames = this.excludeProperties.get(clazz);
            if (propnames == null) {
                propnames = new HashSet<String>();
                this.excludeProperties.put(clazz, propnames);
            }
            propnames.add(propertyname);
            return this;
        }

        public BeanDiff build() {
            validate();
            BeanDiff bd = new BeanDiff();
            bd.introspectPackagesRegexes.addAll(ipr);
            bd.excludeProperties.putAll(excludeProperties);
            return bd;
        }

        private void validate() {
            Predicate classMatchesNoPackageRegex = new Predicate() {
                public boolean evaluate(Object object) {
                    Class<?> cls = (Class<?>) object;
                    String classname = cls.getName();
                    for (String packageRegex : ipr) {
                        if (classname.matches(packageRegex)) {
                            return false;
                        }
                    }
                    return true;
                }
            };

            Collection<Class<?>> classesNotMatchingAnyRegex = CollectionUtils.select(
                    excludeProperties.keySet(), classMatchesNoPackageRegex);
            if (!classesNotMatchingAnyRegex.isEmpty()) {
                String cls = classesNotMatchingAnyRegex.size() == 1 ? "class" : "classes";
                String doo = classesNotMatchingAnyRegex.size() == 1 ? "does" : "do";
                throw new RuntimeException(
                        String.format(
                                "Not all classes to exclude a property from match a introspectPackagesRegex(). "
                                        + "You need to call introspectPackagesRegexes() with a regex that matches the package "
                                        + "of the class passed to excludeProperties(). Offending %s: %s, %s not match any of the regexes: %s",
                                cls, classesNotMatchingAnyRegex, doo, ipr));
            }
        }
    }

    private static class CompareContext {
        private IdentityHashMap<Object, Object> encounteredObjects = new IdentityHashMap<Object, Object>();

        public void encounter(Object o) {
            encounteredObjects.put(o, o);
        }

        public boolean hasEncountered(Object o) {
            return encounteredObjects.containsKey(o);
        }

        public void resetEncounter(Object o) {
            encounteredObjects.remove(o);
        }
    }

    private final Collection<String> introspectPackagesRegexes = new ArrayList<String>();

    private final Map<Class<?>, Set<String>> excludeProperties = new HashMap<Class<?>, Set<String>>();

    /**
     * Compares two java beans based on their equals method and returns the difference. This is a convenience
     * function when the BeanDiff needs no special configuration.
     * 
     * @param <T> type of the leftside to match
     * @param <K> type of the rightside to match. This should be equal to <T> or a super type.
     * @return The difference between left and richt object, null if left and right are both null or left and
     *         right are equal
     */
    public static <T, K extends T> Difference compareUsingEquals(T left, K right) {
        return new BeanDiff().compareLogic(left, right, new CompareContext());
    }

    /**
     * Compares two java beans based on their properties or equals method as specified options passed to the
     * Builder, and returns the difference.
     * 
     * @param <T> type of the leftside to match
     * @param <K> type of the rightside to match. This should be equal to <T> or a super type.
     * @return The difference between left and richt object, null if left and right are both null or left and
     *         right are equal
     */
    public <T, K extends T> Difference compare(T left, K right) {
        return compareLogic(left, right, new CompareContext());
    }

    private <T> Difference compareLogic(T left, T right, CompareContext ctx) {
        final Difference result;
        if (left == null && right == null) {
            result = null;
        } else if (left == null || right == null) {
            result = new ValueDifference(left, right);
        } else {
            if (ctx.hasEncountered(left)) {
                result = null;
            } else {
                ctx.encounter(left);

                Class<?> leftClass = left.getClass();

                if (leftClass.isEnum()) {
                    result = compareEnums((Enum<?>) left, (Enum<?>) right);
                } else if (left instanceof Set) {
                    result = compareSets((Set<?>) left, (Set<?>) right, ctx);
                } else if (left instanceof List) {
                    result = compareLists((List<?>) left, (List<?>) right, ctx);
                } else if (left instanceof Map) {
                    result = compareMaps((Map<?, ?>) left, (Map<?, ?>) right, ctx);
                } else if (leftClass.isArray()) {
                    result = compareArrays(left, right, ctx);
                } else if (shouldIntrospectClass(left.getClass().getName())) {
                    result = compareBeansByIntrospection(left, right, ctx);
                } else {
                    result = compareByEquals(left, right, ctx);
                }

                ctx.resetEncounter(left);
            }
        }

        return result;
    }

    private <T> Difference compareArrays(T left, T right, CompareContext ctx) {
        List<T> leftList = new ArrayList<T>();
        CollectionUtils.addAll(leftList, (Object[]) left);
        List<T> rightList = new ArrayList<T>();
        CollectionUtils.addAll(rightList, (Object[]) right);

        Difference result = compareLists(leftList, rightList, ctx);

        return result;
    }

    private <T extends Enum<?>> Difference compareEnums(T left, T right) {
        if (left == right) {
            return null;
        } else {
            return new ValueDifference(left, right);
        }
    }

    private <T extends Set<?>> Difference compareSets(final T left, final T right, final CompareContext ctx) {
        final Difference result;
        if (left.size() != right.size()) {
            result = new CollectionSizeDifference(left.size(), right.size());
        } else {
            result = compareEqualSizedSets(left, right, ctx);
        }
        return result;
    }

    private <T extends Set<?>> CollectionDifference compareEqualSizedSets(final T left, final T right,
            final CompareContext ctx) {

        final CollectionDifference result;
        Predicate findMisInRight = new Predicate() {
            public boolean evaluate(final Object leftItem) {
                Predicate matches = new Predicate() {
                    public boolean evaluate(Object rightItem) {
                        return compareLogic(leftItem, rightItem, ctx) == null;
                    }
                };
                return !CollectionUtils.exists(right, matches);
            }
        };
        Predicate findMisInLeft = new Predicate() {
            public boolean evaluate(final Object rightItem) {
                Predicate matches = new Predicate() {
                    public boolean evaluate(Object leftItem) {
                        return compareLogic(rightItem, leftItem, ctx) == null;
                    }
                };
                return !CollectionUtils.exists(left, matches);
            }
        };

        final Collection<Object> misInRight = CollectionUtils.select(left, findMisInRight);
        final Collection<Object> misInLeft = CollectionUtils.select(right, findMisInLeft);

        if (misInLeft.isEmpty() && misInRight.isEmpty()) {
            result = null;
        } else {
            result = new CollectionDifference(misInLeft, misInRight);
        }

        return result;
    }

    private <T extends List<?>> Difference compareLists(T left, T right, CompareContext ctx) {
        final Difference result;
        if (left.size() != right.size()) {
            result = new CollectionSizeDifference(left.size(), right.size());
        } else {
            Map<Integer, Difference> listDiffs = new HashMap<Integer, Difference>();

            List<Pair<?, ?>> pairedLists = pairLists(left, right);

            for (int i = 0; i < pairedLists.size(); i++) {
                Pair<?, ?> pair = pairedLists.get(i);
                Difference itemDiff = compareLogic(pair.getLeft(), pair.getRight(), ctx);
                if (itemDiff != null) {
                    listDiffs.put(i, itemDiff);
                }
            }

            if (listDiffs.isEmpty()) {
                result = null;
            } else {
                result = new ListDifference(listDiffs);
            }
        }
        return result;
    }

    /**
     * Returns true if this BeanDiff must compare the specified class using introspection, returns false if it
     * must use equals() for the comparison.
     */
    private boolean shouldIntrospectClass(final String fqClassname) {
        return CollectionUtils.exists(introspectPackagesRegexes, new Predicate() {
            public boolean evaluate(Object object) {
                String introspectPackageRegex = (String) object;
                boolean result = fqClassname.matches(introspectPackageRegex);
                return result;
            }
        });
    }

    /** Returns true if this differ should take this property into account */
    private boolean shouldIntrospectProperty(Class<?> clazz, PropertyDescriptor descriptor) {
        if (descriptor instanceof IndexedPropertyDescriptor) {
            return false;
        }

        if ("class".equals(descriptor.getName())) {
            return false;
        }

        Set<String> excludePropsForClass = excludeProperties.get(clazz);
        if (excludePropsForClass == null) {
            return true;
        }

        final boolean result;
        if (excludePropsForClass.contains(descriptor.getName())) {
            result = false;
        } else {
            result = true;
        }
        return result;
    }

    private static List<Pair<?, ?>> pairLists(List<?> left, List<?> right) {
        if (left.size() != right.size()) {
            throw new AssertionError();
        }

        final List<Pair<?, ?>> result = new ArrayList<Pair<?, ?>>(left.size());

        for (Iterator<?> iterL = left.iterator(), iterR = right.iterator(); iterL.hasNext();) {
            result.add(ImmutablePair.of(iterL.next(), iterR.next()));
        }

        return result;
    }

    private <T extends Map<?, ?>> Difference compareMaps(T left, T right, CompareContext ctx) {
        final Difference result;
        if (left.size() != right.size()) {
            result = new CollectionSizeDifference(left.size(), right.size());
        } else {
            CollectionDifference keysDiff = compareEqualSizedSets(left.keySet(), right.keySet(), ctx);
            if (keysDiff != null) {
                result = new MapKeysDifference(keysDiff);
            } else {
                Map<Object, Difference> mapValuesDiffs = compareMapValues(left, right, ctx);
                if (!mapValuesDiffs.isEmpty()) {
                    result = new MapValuesDifference(mapValuesDiffs);
                } else {
                    result = null;
                }
            }
        }
        return result;
    }

    /**
     * Precondition: the keysets of both maps are equivalent
     */
    private Map<Object, Difference> compareMapValues(final Map<?, ?> left, final Map<?, ?> right,
            final CompareContext ctx) {
        final Map<Object, Difference> result = new HashMap<Object, Difference>();

        Closure collectValueDiffs = new Closure() {
            public void execute(Object mapKey) {
                Difference valueDiff = compareLogic(left.get(mapKey), right.get(mapKey), ctx);
                if (valueDiff != null) {
                    result.put(mapKey, valueDiff);
                }
            }
        };

        CollectionUtils.forAllDo(left.keySet(), collectValueDiffs);

        return result;
    }

    private <T> Difference compareBeansByIntrospection(final T left, final T right, final CompareContext ctx) {
        final Difference result;
        final Map<String, Difference> propDiffs = new HashMap<String, Difference>();

        Class<?> leftType = left.getClass();
        Class<?> rightType = right.getClass();
        if (!leftType.isAssignableFrom(rightType)) {
            result = new TypeDifference(left.getClass(), right.getClass());
        } else {
            PropertyDescriptor[] propertyDescriptors = PropertyUtils.getPropertyDescriptors(left.getClass());

            Closure comparePropertiesClosure = new Closure() {
                public void execute(Object input) {
                    PropertyDescriptor descr = (PropertyDescriptor) input;

                    if (!shouldIntrospectProperty(left.getClass(), descr)) {
                        return;
                    }

                    final String name = descr.getName();
                    if (PropertyUtils.isReadable(left, name)) {
                        final Object leftProp;
                        final Object rightProp;
                        try {
                            leftProp = PropertyUtils.getProperty(left, name);
                            rightProp = PropertyUtils.getProperty(right, name);
                        } catch (Exception e) {
                            throw new RuntimeException(e);
                        }

                        Difference propDiff = compareLogic(leftProp, rightProp, ctx);
                        if (propDiff != null) {
                            propDiffs.put(descr.getName(), propDiff);
                        }
                    }
                }
            };

            CollectionUtils.forAllDo(Arrays.asList(propertyDescriptors), comparePropertiesClosure);

            if (!propDiffs.isEmpty()) {
                result = new BeanDifference(propDiffs);
            } else {
                result = null;
            }
        }

        return result;
    }

    private static <T> Difference compareByEquals(T left, T right, CompareContext ctx) {
        final Difference result;
        Class<?> leftType = left.getClass();
        Class<?> rightType = right.getClass();

        if (!leftType.isAssignableFrom(rightType)) {
            result = new TypeDifference(left.getClass(), right.getClass());
        } else if (left.equals(right)) {
            return null;
        } else {
            result = new ValueDifference(left, right);
        }

        return result;
    }
}
