package bs.beandiff;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.Closure;
import org.apache.commons.collections.CollectionUtils;

public abstract class Difference {
    private final String newline;

    public abstract Set<String> toStringDiff();

    public abstract int size();

    public Difference() {
        super();
        newline = createNewline();
    }

    protected String createNewline() {
        return System.getProperty("line.separator");
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public String toString() {
        Set<String> diffStrs = toStringDiff();
        List<String> diffStrList = new ArrayList<String>(diffStrs);
        Collections.sort(diffStrList);
        final StringBuilder sb = new StringBuilder(newline);

        for (String diff : diffStrList) {
            sb.append(diff).append(newline);
        }

        return sb.toString();
    }

}
