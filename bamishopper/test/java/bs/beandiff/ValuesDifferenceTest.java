package bs.beandiff;

import junit.framework.Assert;

import org.junit.Test;

public class ValuesDifferenceTest {
    
    /** Testable platform-independent ValueDifference always uses linefeed instead of system specified */
    private static class ValueDifferenceUsingLF extends ValueDifference {
        public <T> ValueDifferenceUsingLF(T left, T right) {
            super(left, right);
        }

        @Override
        protected String createNewline() {
            return "\n";
        }
    };
    
    @Test
    public void testToStringWithStringValues() {
        String left = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat";
        String right = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt Xt labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat";
        ValueDifference vd = new ValueDifferenceUsingLF(left, right);
        String actual = vd.toString();
        String expected = "\nvalue mismatch[left: ...d tempor incididunt [u]t labore et dolore m..., right: ...d tempor incididunt [X]t labore et dolore m...]\n";
        Assert.assertEquals(expected, actual);
    }
}
