package bs.beandiff;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.Closure;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Transformer;

public class BeanDifference extends Difference {
    /** All field differences of a bean. Maps fieldnames to difference. */
    private Map<String, Difference> diffs = new HashMap<String, Difference>();
    
    public BeanDifference() {
    }

    public BeanDifference(Map<String, Difference> propDiffs) {
        diffs.putAll(propDiffs);
    }

    public void addFieldDiff(String fieldName, Difference diff) {
        diffs.put(fieldName, diff);
    }
    
    @Override
    public int size() {
        return diffs.size();
    }
    
    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public Set<String> toStringDiff() {
        final Set<String> fieldsDiffs = new HashSet<String>(diffs.size());
        
        Closure fieldsToStringClosure = new Closure() {
            public void execute(Object input) {
                final String fieldname = (String) input;
                Difference diff = diffs.get(fieldname);
                Set<String> fieldDiffs = diff.toStringDiff();
                
                Transformer prependFieldnameToDiff = new Transformer() {
                    public Object transform(Object input) {
                        String fieldDiff = (String) input;
                        return fieldname + "." + fieldDiff;
                    }
                };
                
                Collection qualifiedFieldDiffs = CollectionUtils.collect(fieldDiffs, prependFieldnameToDiff);
                
                fieldsDiffs.addAll(qualifiedFieldDiffs);
            }
        };
        
        CollectionUtils.forAllDo(diffs.keySet(), fieldsToStringClosure);
        
        return fieldsDiffs;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((diffs == null) ? 0 : diffs.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        BeanDifference other = (BeanDifference) obj;
        if (diffs == null) {
            if (other.diffs != null)
                return false;
        } else if (!diffs.equals(other.diffs))
            return false;
        return true;
    }
    
}
