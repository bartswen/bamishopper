package bs.beandiff;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class TypeDifference extends Difference {
    private final Class<?> left, right;

    public TypeDifference(Class<?> left, Class<?> right) {
        this.left = left;
        this.right = right;
    }

    @Override
    public int size() {
        return 1;
    }
    
    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public Set<String> toStringDiff() {
        return new HashSet<String>(Arrays.asList(String.format(
                "type mismatch[left: %s, right: %s] - left should be the same or a subtype of right", left, right)));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((left == null) ? 0 : left.hashCode());
        result = prime * result + ((right == null) ? 0 : right.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        TypeDifference other = (TypeDifference) obj;
        if (left == null) {
            if (other.left != null)
                return false;
        } else if (!left.equals(other.left))
            return false;
        if (right == null) {
            if (other.right != null)
                return false;
        } else if (!right.equals(other.right))
            return false;
        return true;
    };
}
