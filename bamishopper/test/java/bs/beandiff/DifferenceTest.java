package bs.beandiff;

import junit.framework.Assert;

import org.junit.Test;

public class DifferenceTest {

    /** TODO Refactor Eager Test */
    @Test
    public void testBeanDifference() {
        BeanDifference bd = new BeanDifference();
        bd.addFieldDiff("lastname", new ValueDifference(null, "Obama"));
        bd.addFieldDiff("phone", new ValueDifference("0647383726", " "));
        
        MapValuesDifference md=new MapValuesDifference();
        ValueDifference vd1 = new ValueDifference("P.O. Box 634, 7427 Non Rd.   Clovis  A6C 9V6", "759 Ac Street    Pass Christian  45109");
        md.addChild("shipping", vd1);
        ValueDifference vd2 = new ValueDifference("846-4887 Velit Road  Saint Cloud L5U 1C4", "P.O. Box 585, 5440 Vestibulum Avenue Kansas City 96851");
        md.addChild("billing", vd2);
        bd.addFieldDiff("addresses", md);
        
        CollectionDifference cd = new CollectionDifference();
        cd.addMissingInLeft("Ursula");
        cd.addMissingInRight("Kelsey");
        cd.addMissingInLeft("Piper");
        cd.addMissingInRight("Cassandra");
        bd.addFieldDiff("pats", cd);
        
        CollectionSizeDifference csd=new CollectionSizeDifference(0, 3);
        bd.addFieldDiff("children", csd);

        ListDifference ld = new ListDifference();
        ld.addDiff(3, bd);

        String actual = ld.toString();

        String expected = "\n[3].addresses.billing-->value mismatch[left: [846-4887 Velit Road  Saint Cloud L5U 1C4], right: [P.O. Box 585, 5440 Vestibulum Avenue Kansas City 96851]]\n[3].addresses.shipping-->value mismatch[left: [P.O. Box 634, 7427 Non Rd.   Clovis  A6C 9V6], right: [759 Ac Street    Pass Christian  45109]]\n[3].children.size[left:0, right:3]\n[3].lastname.value mismatch[left: <null>, right: Obama]\n[3].pats.missing in left: Piper\n[3].pats.missing in left: Ursula\n[3].pats.missing in right: Cassandra\n[3].pats.missing in right: Kelsey\n[3].phone.value mismatch[left: [0647383726], right: [ ]]\n";
        Assert.assertEquals(expected, actual);
    }
    
}
