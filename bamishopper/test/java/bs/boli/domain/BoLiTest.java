package bs.boli.domain;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import junit.framework.Assert;

import org.apache.commons.lang3.ObjectUtils;
import org.joda.time.DateTime;
import org.junit.Test;

import bs.beandiff.BeanDiff;
import bs.beandiff.BeanDiff.Builder;
import bs.beandiff.Difference;
import bs.boli.domain.ItemInShop;
import bs.boli.domain.test.util.BoLiTestUtils;

public class BoLiTest {
    private static final String BS_BOLI_REGEX = "bs\\.boli.*";

    // testAddBoToWnkPositionbyCategory
    // testChangeCategoryRepostionInShops
    // testRemoveCategoryRemainPositionInShops

    @Test
    public void testAddBs() {
        BoLi bl = new BoLi();

        bl.addBo(new Bo("item3"));
        bl.addBo(new Bo("item2"));
        bl.addBo(new Bo("item1"));

        Assert.assertEquals(3, bl.size());
    }

    @Test
    public void testAddBoTwice() {
        BoLi bl = new BoLi();

        bl.addBo(new Bo("item1"));
        bl.addBo(new Bo("item1"));

        Assert.assertEquals(1, bl.size());
    }

    @Test
    public void testAddBsUniqueId() {
        BoLi bl = new BoLi();

        // Same consonants, items generates same suggest id for each
        bl.addBo(new Bo("item"));
        bl.addBo(new Bo("atem"));
        bl.addBo(new Bo("utem"));

        List<String> actualAlfaOrderedIds = CollectionUtils.collect(bl.getBsAlphabetic(),
                new CollectionUtils.Transformer<Bo, String>() {
                    @Override
                    public String transform(Bo t) {
                        return t.getId();
                    }
                });

        List<String> expectedIds = Arrays.asList("tm1", "tm", "tm2");
        Assert.assertEquals(expectedIds, actualAlfaOrderedIds);
    }

    @Test
    public void testDeleteBo() {
        // prepare
        Date d = new DateTime("2013-02-03T14:30").toDate();
        BoLi bl = BoLiTestUtils.createBoLiWithTwoFilledShopsAndThreeCategorizedBs(d);
        BoLiTestUtils.clearEvents(bl);

        String id = bl.getBoByNaam("item2").getId(); // need later

        // exercise
        bl.deleteBoByNaam("item2");

        // assert
        Assert.assertTrue(bl.getBoByNaam("item2") == null);
        Assert.assertFalse(bl.getWnk("shop1").heeft("item2"));
        Assert.assertFalse(bl.getWnk("shop2").heeft("item2"));
    }

    @Test
    public void testDeleteBoGeneratedEvent() {
        // prepare
        Date d = new DateTime("2013-02-03T14:30").toDate();
        BoLi bl = BoLiTestUtils.createBoLiWithTwoFilledShopsAndThreeCategorizedBs(d);
        BoLiTestUtils.clearEvents(bl);

        String id = bl.getBoByNaam("item2").getId(); // need later

        // exercise
        bl.deleteBoByNaam("item2");

        // prepare assert
        BoLiEvent actual = bl.getEventsToPublish().get(0);
        Assert.assertNotNull(actual);

        Bo deletedBo = new Bo("item2", "cat2");
        deletedBo.setId(id);
        HashSet<ItemInShop> expectedRemovedShopItems = new HashSet<ItemInShop>(
                Arrays.asList(new ItemInShop("shp1", 1), new ItemInShop(
                        "shp", 1)));

        BoLiEvent expected = new ItemDeletedEvent(d, deletedBo, expectedRemovedShopItems);

        // assert
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testDeleteBoUndo() {
        // prepare
        Date d = new DateTime("2013-02-03T14:30").toDate();
        BoLi bl = BoLiTestUtils.createBoLiWithTwoFilledShopsAndThreeCategorizedBs(d);
        BoLiTestUtils.clearEvents(bl);

        String id = bl.getBoByNaam("item2").getId(); // need later

        // exercise
        bl.deleteBoByNaam("item2");
        bl.undo();

        // assert
        Assert.assertFalse(bl.getBoByNaam("item2") == null);
        //TODO check position also
        Assert.assertTrue(bl.getWnk("shop1").heeft("item2"));
        Assert.assertTrue(bl.getWnk("shop2").heeft("item2"));
    }

    @Test
    public void testAddWnkUniqueId() {
        BoLi bl = BoLiTestUtils.createBoLiWithThreeBs();

        bl.addWnk(new Wnk("shop1"));
        bl.addWnk(new Wnk("shop2"));
        List<String> actualIds = CollectionUtils.collect(bl.getWnks(),
                new CollectionUtils.Transformer<Wnk, String>() {
                    @Override
                    public String transform(Wnk w) {
                        return w.getId();
                    }
                });

        List<String> expectedIds = Arrays.asList("shp", "shp1");
        Assert.assertEquals(expectedIds, actualIds);
    }

    @Test
    public void testAddWnk() {
        BoLi bl = BoLiTestUtils.createBoLiWithThreeBs();

        bl.addWnk(new Wnk("shop1"));
        bl.addWnk(new Wnk("shop2"));
        List<Wnk> actual = bl.getWnks();

        List<Wnk> expected = Arrays.asList(new Wnk("shop1"), new Wnk("shop2"));
        BoLiTestUtils.assertEqualsValues(expected, actual);
    }

    @Test
    public void testAddWnkTwice() {
        BoLi bl = BoLiTestUtils.createBoLiWithThreeBs();

        bl.addWnk(new Wnk("shop1"));
        bl.addWnk(new Wnk("shop1"));
        int actual = bl.getWnks().size();

        Assert.assertEquals(1, actual);
    }

    @Test
    public void testDeleteWnk() {
        BoLi bl = BoLiTestUtils.createBoLiWithTwoFilledShopsAndThreeBs();

        bl.deleteWnk("shop1");
        List<Wnk> actual = bl.getWnks();

        Assert.assertEquals(1, actual.size());
        Assert.assertEquals("shop2", actual.get(0).getNaam());
    }

    @Test
    public void testDeleteWnkDoesNotExist() {
        BoLi bl = BoLiTestUtils.createBoLiWithTwoFilledShopsAndThreeBs();

        bl.deleteWnk("X");
        List<Wnk> actual = bl.getWnks();

        Assert.assertEquals(2, actual.size());
    }

    @Test
    public void testAddBoToWnkInOrder() {
        BoLi bl = BoLiTestUtils.createBoLiWithThwoShopsAndThreeBs();

        bl.addBoToWnk("item3", "shop1");
        bl.addBoToWnk("item1", "shop1");
        bl.addBoToWnk("item2", "shop1");

        List<Bo> actual = bl.getBsForWnk("shop1");

        List<Bo> expected = Arrays.asList(new Bo("item2"), new Bo("item1"), new Bo("item3"));
        BoLiTestUtils.assertEqualsValues(expected, actual);
    }

    @Test
    public void testRemoveBoFromWnk() {
        BoLi bl = BoLiTestUtils.createBoLiWithTwoFilledShopsAndThreeBs();

        bl.removeBoFromWnk("item1", "shop1");

        List<Bo> actual = bl.getBsForWnk("shop1");

        List<Bo> expected = Arrays.asList(new Bo("item2"), new Bo("item3"));
        BoLiTestUtils.assertEqualsValues(expected, actual);
    }

    @Test
    public void testAddBoToWnkDoesNotExistsInBsli() {
        BoLi bl = BoLiTestUtils.createBoLiWithThwoShopsAndThreeBs();

        bl.addBoToWnk("X", "shop1");

        List<Bo> actual = bl.getBsForWnk("shop1");
        Assert.assertTrue(actual.isEmpty());
    }

    @Test
    public void testAddBoToWnkPositionBeforeFirstInCategory() {
        BoLi bl = BoLiTestUtils.createBoLiWithTwoFilledShopsAndThreeCategorizedBs();

        Bo newitem = new Bo("newitem", "cat2");
        bl.addBo(newitem);
        bl.addBoToWnk("newitem", "shop1");

        List<String> actual = BoLiTestUtils.asItemNames(bl.getBsForWnk("shop1"));
        List<String> expected=Arrays.asList("item1", "newitem", "item2","item3");
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testAddBoToWnkAlreadyExistsInWnk() {
        BoLi bl = BoLiTestUtils.createBoLiWithThwoShopsAndThreeBs();

        bl.addBoToWnk("item3", "shop1");
        bl.addBoToWnk("item3", "shop1");

        List<Bo> actual = bl.getBsForWnk("shop1");

        List<Bo> expected = Arrays.asList(new Bo("item3"));
        BoLiTestUtils.assertEqualsValues(expected, actual);
    }

    // @Test
    // public void testUpFirst() {
    // BoLi bl = BoLiTestUtils.createBoLiWithTwoFilledShopsAndThreeBs();
    //
    // Wnk wnk = bl.getWnk("shop1");
    // wnk.up("item2");
    // List<Bo> actual = bl.getBsForWnk("shop1");
    //
    // List<Bo> expected = Arrays.asList(new Bo("item2"), new Bo("item1"), new Bo("item3"));
    // Assert.assertEquals(expected, actual);
    // }
    //
    // @Test
    // public void testUpNonExistent() {
    // BoLi bl = BoLiTestUtils.createBoLiWithTwoFilledShopsAndThreeBs();
    //
    // Wnk wnk = bl.getWnk("shop1");
    // wnk.up("X");
    // List<Bo> actual = bl.getBsForWnk("shop1");
    //
    // List<Bo> expected = Arrays.asList(new Bo("item2"), new Bo("item1"), new Bo("item3"));
    // Assert.assertEquals(expected, actual);
    // }
    //
    // @Test
    // public void testDown() {
    // BoLi bl = BoLiTestUtils.createBoLiWithTwoFilledShopsAndThreeBs();
    //
    // Wnk wnk = bl.getWnk("shop1");
    // wnk.down("item1");
    // List<Bo> actual = bl.getBsForWnk("shop1");
    //
    // List<Bo> expected = Arrays.asList(new Bo("item2"), new Bo("item3"), new Bo("item1"));
    // Assert.assertEquals(expected, actual);
    // }
    //
    // @Test
    // public void testDownLast() {
    // BoLi bl = BoLiTestUtils.createBoLiWithTwoFilledShopsAndThreeBs();
    //
    // Wnk wnk = bl.getWnk("shop1");
    // wnk.down("item3");
    // List<Bo> actual = bl.getBsForWnk("shop1");
    //
    // List<Bo> expected = Arrays.asList(new Bo("item2"), new Bo("item1"), new Bo("item3"));
    // Assert.assertEquals(expected, actual);
    // }
    //
    // @Test
    // public void testDownNonExistent() {
    // BoLi bl = BoLiTestUtils.createBoLiWithTwoFilledShopsAndThreeBs();
    //
    // Wnk wnk = bl.getWnk("shop1");
    // wnk.up("X");
    // List<Bo> actual = bl.getBsForWnk("shop1");
    //
    // List<Bo> expected = Arrays.asList(new Bo("item2"), new Bo("item1"), new Bo("item3"));
    // Assert.assertEquals(expected, actual);
    // }
    //
    @Test
    public void testToIndex() {
        BoLi bl = BoLiTestUtils.createBoLiWithTwoFilledShopsAndThreeBs();

        Wnk wnk = bl.getWnk("shop1");
        wnk.toIndex("item2", 2);
        List<Bo> actual = bl.getBsForWnk("shop1");

        List<Bo> expected = Arrays.asList(new Bo("item1"), new Bo("item3"), new Bo("item2"));
        BoLiTestUtils.assertEqualsValues(expected, actual);
    }

    @Test
    public void testToIndexOutOfBounds() {
        BoLi bl = BoLiTestUtils.createBoLiWithTwoFilledShopsAndThreeBs();

        Wnk wnk = bl.getWnk("shop1");
        wnk.toIndex("item2", 3);
        List<Bo> actual = bl.getBsForWnk("shop1");

        List<Bo> expected = Arrays.asList(new Bo("item2"), new Bo("item1"), new Bo("item3"));
        BoLiTestUtils.assertEqualsValues(expected, actual);
    }

    @Test
    public void testToIndexNonExistent() {
        BoLi bl = BoLiTestUtils.createBoLiWithTwoFilledShopsAndThreeBs();

        Wnk wnk = bl.getWnk("shop1");
        wnk.toIndex("X", 1);
        List<Bo> actual = bl.getBsForWnk("shop1");

        List<Bo> expected = Arrays.asList(new Bo("item2"), new Bo("item1"), new Bo("item3"));
        BoLiTestUtils.assertEqualsValues(expected, actual);
    }

    @Test
    public void testRemoveBo() {
        BoLi bl = BoLiTestUtils.createBoLiWithTwoFilledShopsAndThreeBs();

        bl.removeBoFromWnk("item2", "shop1");
        List<Bo> actual = bl.getBsForWnk("shop1");

        List<Bo> expected = Arrays.asList(new Bo("item1"), new Bo("item3"));
        BoLiTestUtils.assertEqualsValues(expected, actual);
    }

    @Test
    public void testRemoveBoNonExistent() {
        BoLi bl = BoLiTestUtils.createBoLiWithTwoFilledShopsAndThreeBs();

        bl.removeBoFromWnk("X", "shop1");
        List<Bo> actual = bl.getBsForWnk("shop1");

        List<Bo> expected = Arrays.asList(new Bo("item2"), new Bo("item1"), new Bo("item3"));
        BoLiTestUtils.assertEqualsValues(expected, actual);
    }

    @Test
    public void testGetBsAlphabetic() {
        BoLi bl = new BoLi();

        bl.addBo(new Bo("a"));
        bl.addBo(new Bo("c"));
        bl.addBo(new Bo("b"));
        List<Bo> actual = bl.getBsAlphabetic();

        List<Bo> expected = Arrays.asList(new Bo("a"), new Bo("b"), new Bo("c"));
        BoLiTestUtils.assertEqualsValues(expected, actual);
    }

    @Test
    public void testGetBsGroupByCat() {
        BoLi bl = new BoLi();

        bl.addBo(new Bo("item3", "cat1"));
        bl.addBo(new Bo("item2", "catx"));
        bl.addBo(new Bo("item1", "catx"));
        bl.addBo(new Bo("ongecategoriseerd"));

        Map<String, List<Bo>> actual = bl.getBsGroupByCat();

        Map<String, List<Bo>> expected = putAllInMap(new HashMap<String, List<Bo>>(), new Object[] { null,
                Arrays.asList(new Bo("ongecategoriseerd")), "cat1", Arrays.asList(new Bo("item3", "cat1")),
                "catx", Arrays.asList(new Bo("item1", "catx"), new Bo("item2", "catx")) });

        // Assert.assertEquals(expected, actual);
        BeanDiff beandiff = new BeanDiff.Builder().introspectPackagesRegexes(Bo.class.getName())
                .excludeProperty(Bo.class, "id").build();
        Difference difference = beandiff.compare(expected, actual);
        Assert.assertNull(ObjectUtils.toString(difference), difference);
    }

    @Test
    public void testGetWnkIgnoreCase() {
        BoLi boli = BoLiTestUtils.createBoLiWithOneShopAndFourBs();
        Wnk found = boli.getWnkIgnoreCase("ShOp1");
        Assert.assertNotNull(found);
    }

    @Test
    public void testGetWnkForBo() {
        BoLi boli = BoLiTestUtils.createBoLiWithTwoFilledShopsAndThreeBs();
        List<Wnk> actual = boli.getWnksForBo("item3");

        Assert.assertEquals(1, actual.size());
        String expected = "shop1";
        Assert.assertEquals(expected, actual.get(0).getNaam());
    }

    /**
     * Bad performance, use getBoByNaam() if possible.
     */
    @Test
    public void getBoByNaamIgnoreCase() {
        BoLi boli = BoLiTestUtils.createBoLiWithFourBs();
        Bo actual = boli.getBoByNaamIgnoreCase("ItEm1");
        Assert.assertNotNull(actual);
    }

    @Test
    public void testTeKopenVoor() {
        BoLi boli = BoLiTestUtils.createBoLiWithTwoFilledShopsAndThreeBs();
        boli.incrementBoByNaam("item1");
        boli.incrementBoByNaam("item3");
        List<Bo> actual = boli.teKopenVoor("shop1");

        List<Bo> expected = Arrays.asList(boli.getBoByNaam("item1"), boli.getBoByNaam("item3"));
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testChangeBoNaamOccursInWnks() {
        BoLi actual = BoLiTestUtils.createBoLiWithTwoFilledShopsAndThreeBs();

        actual.changeBoNaam("item1", "item1-changed");

        BoLi expected = createExpectedBoLiForTestChangeBoNaamOccursInWnks();
        BeanDiff beandiff = new BeanDiff.Builder()
                .introspectPackagesRegexes(BoLi.class.getPackage().getName() + ".*")
                .excludeProperty(Bo.class, "id").excludeProperty(Wnk.class, "id")
                .excludeProperty(BoLi.class, "eventsToPublish").build();
        Difference difference = beandiff.compare(expected, actual);
        Assert.assertNull(ObjectUtils.toString(difference), difference);
    }

    @Test
    public void testAddAndRetrieveCats() {
        BoLi boli = BoLiTestUtils.createBoLiWithThreeBs();
        Bo fluti = boli.getBoByNaam("item3");
        fluti.setCategorie("cat1");
        Bo aard = boli.getBoByNaam("item2");
        aard.setCategorie("cat2");

        List<String> actual = boli.getCategories();
        List<String> expected = Arrays.asList("cat1", "cat2");
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCanUndo() {
        BoLi b = BoLiTestUtils.createBoLiWithThreeBs();
        Assert.assertTrue(b.canUndo());
    }

    @Test
    public void testCanUndoNothingToUndo() {
        BoLi b = BoLiTestUtils.createBoLiWithThreeBs();
        b.undo();
        b.undo();
        b.undo();
        Assert.assertFalse(b.canUndo());
    }

    @Test
    public void testUndoCompensates() {
        BoLi b = BoLiTestUtils.createBoLiWithThreeBs();
        b.undo();
        List<String> actualIds = collectBoIds(b.getBsAlphabetic());
        List<String> expectedIds = Arrays.asList("tm1", "tm");
        Assert.assertEquals(expectedIds, actualIds);
    }

    @Test
    public void testUndoProducesNewEvent() {
        BoLi b = BoLiTestUtils.createBoLiWithThreeBs();
        b.undo();
        Assert.assertEquals(4, b.getEventsToPublish().size());
    }

    // TODO testUndo for each eventtype

    private static List<String> collectBoIds(List<Bo> bs) {
        return CollectionUtils.collect(bs, new CollectionUtils.Transformer<Bo, String>() {
            @Override
            public String transform(Bo t) {
                return t.getId();
            }
        });
    }

    @Test
    public void testMerge() {
        BoLi bl = BoLiTestUtils.createBoLiWithTwoFilledShopsAndThreeBs();

        BoLi toMerge = new BoLi();
        toMerge.addBo(new Bo("item4"));
        toMerge.addWnk(new Wnk("shopX"));
        toMerge.addBoToWnk("item4", "shopX");

        bl.merge(toMerge);

        Assert.assertEquals(toMerge, bl);
    }

    private static BoLi createExpectedBoLiForTestChangeBoNaamOccursInWnks() {
        BoLi boli = new BoLi();
        boli.addBo(new Bo("item3"));
        boli.addBo(new Bo("item2"));
        boli.addBo(new Bo("item1-changed"));

        boli.addWnk(new Wnk("shop1"));
        boli.addWnk(new Wnk("shop2"));

        boli.addBoToWnk("item3", "shop1");
        boli.addBoToWnk("item1-changed", "shop1");
        boli.addBoToWnk("item2", "shop1");

        boli.addBoToWnk("item2", "shop2");
        boli.addBoToWnk("item1-changed", "shop2");

        return boli;
    }

    @SuppressWarnings("unchecked")
    private static Map<String, List<Bo>> putAllInMap(Map<String, List<Bo>> map, Object[] objects) {
        for (int i = 0; i < objects.length; i = i + 2) {
            map.put((String) objects[i], (List<Bo>) objects[i + 1]);
        }
        return map;
    }

    @Test
    public void testGetEventsToPublish() {
        BoLi bl = BoLiTestUtils.createBoLiWithThreeBs();
        bl.incrementBoByNaam("item1");

        List<BoLiEvent> actual = bl.getEventsToPublish();

        BoLiEvent icce = new ItemCountChangedEvent(null, bl.getBoByNaam("item1").getId(), 0, 1);
        BoLiEvent iae3 = new ItemAddedEvent(null, new Bo("item3"));
        BoLiEvent iae2 = new ItemAddedEvent(null, new Bo("item2"));
        BoLiEvent iae1 = new ItemAddedEvent(null, new Bo("item1"));
        List<BoLiEvent> expected = Arrays.asList(iae3, iae2, iae1, icce);

        BeanDiff beandiff = createBeanDiffIgnoringEventTime();

        Difference difference = beandiff.compare(expected, actual);
        Assert.assertNull(ObjectUtils.toString(difference), difference);

    }

    @Test
    public void testRemoveEventsToPublish() {
        BoLi bl = BoLiTestUtils.createBoLiWithThreeBs();
        bl.incrementBoByNaam("item1"); // generate event

        List<BoLiEvent> events = bl.getEventsToPublish();
        bl.removeEventsToPublish(events);

        List<BoLiEvent> actual = bl.getEventsToPublish();
        Assert.assertTrue(actual.isEmpty());
    }

    @Test
    public void testProcessItemCountChangedEvent() {
        BoLi bl = BoLiTestUtils.createBoLiWithThreeBs();

        BoLiEvent icce = new ItemCountChangedEvent(null, bl.getBoByNaam("item1").getId(), 0, 1);
        bl.processEvents(Arrays.asList(icce));
        Bo actual = bl.getBoByNaam("item1");

        int incrementedItemCount = 1;
        Bo expected = new Bo("item1", incrementedItemCount);
        BeanDiff beandiff = createBeanDiffIgnoringBoId();

        Difference difference = beandiff.compare(expected, actual);
        Assert.assertNull(ObjectUtils.toString(difference), difference);
    }

    private static BeanDiff createBeanDiffIgnoringEventTime() {
        Builder b = createBeanDiffBuilder();
        b.excludeProperty(ItemCountChangedEvent.class, "eventTime");
        b.excludeProperty(Bo.class, "id");
        return b.build();
    }

    private static BeanDiff createBeanDiffIgnoringBoId() {
        Builder b = createBeanDiffBuilder();
        b.excludeProperty(Bo.class, "id");
        return b.build();
    }

    private static Builder createBeanDiffBuilder() {
        Builder b = new BeanDiff.Builder();
        b.introspectPackagesRegexes(BS_BOLI_REGEX);
        return b;
    }

}
