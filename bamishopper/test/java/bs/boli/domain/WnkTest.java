package bs.boli.domain;

import junit.framework.Assert;

import org.junit.Test;

import bs.boli.domain.Bo;
import bs.boli.domain.BoLi;
import bs.boli.domain.Wnk;
import bs.boli.domain.test.util.BoLiTestUtils;

public class WnkTest {
    @Test
    public void testMoveFirstAfterSecond() {
        // item 1, 2, 3, 4 in shop1
        BoLi boli = BoLiTestUtils.createBoLiWithOneFilledShopAndFourBs();

        Wnk wnk = boli.getWnk("shop1");
        wnk.moveFirstAfterSecond("item1", "item3");

        // item 2, 3, 1, 4 in shop1
        Wnk expected = createExpectedWnkForTestFirstAfterSecond();
        Assert.assertEquals(expected, wnk);

    }

    @Test
    public void testMoveFirstAfterSecondNoEffect() {
        // item 1, 2, 3, 4 in shop1
        BoLi boli = BoLiTestUtils.createBoLiWithOneFilledShopAndFourBs();

        Wnk wnk = boli.getWnk("shop1");
//        wnk.moveFirstAfterSecond("item3", "item1");

        // item 1, 2, 3, 4 in shop1
        Wnk expected = BoLiTestUtils.createBoLiWithOneFilledShopAndFourBs().getWnk("shop1");
        Assert.assertEquals(expected, wnk);

    }

    private static Wnk createExpectedWnkForTestFirstAfterSecond() {
        BoLi bl = new BoLi();
        bl.addBo(new Bo("item1"));
        bl.addBo(new Bo("item2"));
        bl.addBo(new Bo("item3"));
        bl.addBo(new Bo("item4"));
        bl.addWnk(new Wnk("shop1"));
        bl.addBoToWnk("item4", "shop1");
        bl.addBoToWnk("item1", "shop1");
        bl.addBoToWnk("item3", "shop1");
        bl.addBoToWnk("item2", "shop1");
        
        return bl.getWnk("shop1");
    }
}
