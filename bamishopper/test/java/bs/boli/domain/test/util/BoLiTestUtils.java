package bs.boli.domain.test.util;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.Assert;

import bs.boli.domain.Bo;
import bs.boli.domain.BoLi;
import bs.boli.domain.Clock;
import bs.boli.domain.CollectionUtils;
import bs.boli.domain.EqualsByValues;
import bs.boli.domain.Wnk;

public class BoLiTestUtils {
    /**
     * Returns BoLi with: <code>
     * bs: [item3, item2, item1]
     * wnk: [shop1 [item2, item1, item3],
     *      shop2 [item1, item2]]
     * </code>
     */
    public static BoLi createBoLiWithTwoFilledShopsAndThreeCategorizedBs() {
        BoLi bl = createBoLiWithThwoShopsAndThreeCategorizedBs();
        addFiveBsToTwoShops(bl);
        return bl;
    }

    public static BoLi createBoLiWithTwoFilledShopsAndThreeCategorizedBs(Date now) {
        BoLi bl = createBoLi(now);
        fillBoLiWithThwoShopsAndThreeCategorizedBs(bl);
        addFiveBsToTwoShops(bl);
        return bl;
    }

    public static void fillBoLiWithTwoFilledShopsAndThreeCategorizedBs(BoLi bl) {
        fillBoLiWithThwoShopsAndThreeCategorizedBs(bl);
        addFiveBsToTwoShops(bl);
    }

    public static void addFiveBsToTwoShops(BoLi bl) {
        bl.addBoToWnk("item3", "shop1");
        bl.addBoToWnk("item2", "shop1");
        bl.addBoToWnk("item1", "shop1");

        bl.addBoToWnk("item2", "shop2");
        bl.addBoToWnk("item1", "shop2");
    }

    /**
     * Returns BoLi with: <code>
     * [shop1 [item1, item2, item3, item4]]
     * </code>
     */
    public static BoLi createBoLiWithOneFilledShopAndFourBs() {
        BoLi bl = createBoLiWithOneShopAndFourBs();
        bl.addBoToWnk("item4", "shop1");
        bl.addBoToWnk("item3", "shop1");
        bl.addBoToWnk("item2", "shop1");
        bl.addBoToWnk("item1", "shop1");

        return bl;
    }

    /**
     * Returns BoLi with: <code>
     * bs: [item3, item2, item1]
     * wnk: [shop1 [item2, item1, item3],
     *      shop2 [item1, item2]]
     * </code>
     */
    public static BoLi createBoLiWithTwoFilledShopsAndThreeBs() {
        BoLi bl = createBoLiWithThwoShopsAndThreeBs();
        bl.addBoToWnk("item3", "shop1");
        bl.addBoToWnk("item1", "shop1");
        bl.addBoToWnk("item2", "shop1");

        bl.addBoToWnk("item2", "shop2");
        bl.addBoToWnk("item1", "shop2");

        return bl;
    }

    public static BoLi createBoLiWithOneShopAndFourBs() {
        BoLi boli = createBoLiWithFourBs();
        boli.addWnk(new Wnk("shop1"));
        return boli;
    }

    public static BoLi createBoLiWithThwoShopsAndThreeBs() {
        BoLi boli = createBoLiWithThreeBs();
        fillBoLiWithThwoShops(boli);
        return boli;
    }

    public static BoLi createBoLiWithThwoShopsAndThreeCategorizedBs() {
        BoLi boli = createBoLiWithThreeCategorizedBs();
        fillBoLiWithThwoShops(boli);
        return boli;
    }

    public static void fillBoLiWithThwoShopsAndThreeCategorizedBs(BoLi bl) {
        fillBoLiWithThreeCategorizedBs(bl);
        fillBoLiWithThwoShops(bl);
    }

    static void fillBoLiWithThwoShops(BoLi boli) {
        boli.addWnk(new Wnk("shop1"));
        boli.addWnk(new Wnk("shop2"));
    }

    public static BoLi createBoLiWithThreeBs() {
        BoLi bl = createBoLi();
        fillBoLiWithThreeBs(bl);
        return bl;
    }

    static void fillBoLiWithThreeBs(BoLi bl) {
        bl.addBo(new Bo("item3"));
        bl.addBo(new Bo("item2"));
        bl.addBo(new Bo("item1"));
    }

    public static BoLi createBoLiWithFourBs() {
        BoLi bl = createBoLi();
        fillBoLiWithFourBs(bl);
        return bl;
    }

    static void fillBoLiWithFourBs(BoLi bl) {
        bl.addBo(new Bo("item1"));
        bl.addBo(new Bo("item2"));
        bl.addBo(new Bo("item3"));
        bl.addBo(new Bo("item4"));
    }

    public static BoLi createBoLiWithThreeCategorizedBs() {
        BoLi bl = createBoLi();
        fillBoLiWithThreeCategorizedBs(bl);
        return bl;
    }

    static void fillBoLiWithThreeCategorizedBs(BoLi bl) {
        bl.addBo(new Bo("item1", "cat1"));
        bl.addBo(new Bo("item2", "cat2"));
        bl.addBo(new Bo("item3", "cat2"));
    }

    static BoLi createBoLi() {
        Clock clock = createDummyClock();
        BoLi bl = new BoLi(clock);
        return bl;
    }

    public static Clock createDummyClock() {
        return new Clock() {
            @Override
            public Date now() {
                return new GregorianCalendar(2013, 01, 01).getTime();
            }
        };
    }

    public static Clock createFixedClock(final Date now) {
        return new Clock() {
            @Override
            public Date now() {
                return now;
            }
        };
    }

    public static BoLi createBoLi(final Date now) {
        Clock clock = createFixedClock(now);
        BoLi boli = new BoLi(clock);
        return boli;
    }

    public static void generateAllBoLiEvents(BoLi bl) {
        bl.incrementBoByNaam("item1");
        bl.changeBoNaam("item2", "newname");
        bl.getBoByNaam("item1").setCategorie("newcat");
        bl.deleteBoByNaam("item3");
        bl.deleteWnk("shop1");
        bl.addBoToWnk("item3", "shop2");
        bl.getWnk("shop2").moveFirstAfterSecond("item1", "newname");
        bl.removeBoFromWnk("newname", "shop2");
        bl.getWnk("shop2").setNaam("newshopname");

    }

    /**
     * Compare by values, not id.
     */
    public static <U extends EqualsByValues<U>> void assertEqualsValues(List<U> expected, List<U> actual) {
        if (expected.size() != actual.size()) {
            Assert.fail(String.format("Expected %d elements, but was %d elements", expected.size(),
                    actual.size()));
        }

        for (int i = 0; i < actual.size(); i++) {
            if (!expected.get(i).equalsValues(actual.get(i))) {
                Assert.fail(String.format("Elements #%d (0-based) are not equal. Expected %s, but was %s)",
                        i, expected.get(i), actual.get(i)));
            }
        }
    }

    /**
     * Compares two maps for equality. Do not use Android v10 HashMap.equals(). It contains a bug, will
     * incorrectly return false on two equal maps. This implementation is derived from Apache Harmony v1.5
     * AbstractMap.
     */
    public static boolean equalsMaps(Map<?, ?> left, Object right) {
        if (left == right) {
            return true;
        }
        if (right instanceof Map) {
            Map<?, ?> map = (Map<?, ?>) right;
            if (left.size() != map.size()) {
                return false;
            }

            try {
                for (Entry<?, ?> entry : left.entrySet()) {
                    Object key = entry.getKey();
                    Object mine = entry.getValue();
                    Object theirs = map.get(key);
                    if (mine == null) {
                        if (theirs != null || !map.containsKey(key)) {
                            return false;
                        }
                    } else if (!mine.equals(theirs)) {
                        return false;
                    }
                }
            } catch (NullPointerException ignored) {
                return false;
            } catch (ClassCastException ignored) {
                return false;
            }
            return true;
        }
        return false;
    }

    public static void clearEvents(BoLi bl) {
        bl.removeEventsToPublish(bl.getEventsToPublish());
    }

    public static List<String> asItemNames(List<Bo> items) {
        return CollectionUtils.collect(items, new CollectionUtils.Transformer<Bo, String>() {
            @Override
            public String transform(Bo t) {
                return t.getNaam();
            }
        });

    }
}
