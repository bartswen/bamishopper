package bs.boli.domain.test.util;

import java.util.HashMap;

import org.junit.Assert;
import org.junit.Test;

public class BoLiTestUtilsTest {
    @Test
    public void testEqualsMaps() {
        HashMap<String, String> left = new HashMap<String, String>();
        left.put("key", "value");

        HashMap<String, String> right = new HashMap<String, String>();
        right.put("key", "value");

        Assert.assertTrue(BoLiTestUtils.equalsMaps(left, right));
    }

    @Test
    public void testEqualsMapsNotEqual() {
        HashMap<String, String> left = new HashMap<String, String>();
        left.put("key", "value");

        HashMap<String, String> right = new HashMap<String, String>();
        right.put("key", "XXX");

        Assert.assertFalse(BoLiTestUtils.equalsMaps(left, right));
    }
}
