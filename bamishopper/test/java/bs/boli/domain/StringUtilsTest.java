package bs.boli.domain;

import junit.framework.Assert;

import org.junit.Test;

import bs.boli.domain.StringUtils;

public class StringUtilsTest {
    @Test
    public void testFilterConsonants() {
        String actual = StringUtils.filterConsonants("19BanAnen");
        Assert.assertEquals("Bnnn", actual);
    }
}
