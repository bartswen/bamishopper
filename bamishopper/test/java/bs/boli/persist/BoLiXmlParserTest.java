package bs.boli.persist;

import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import junit.framework.Assert;

import org.apache.commons.lang3.ObjectUtils;
import org.junit.Test;

import bs.beandiff.BeanDiff;
import bs.beandiff.Difference;
import bs.boli.domain.BoLi;
import bs.boli.domain.BoLiEvent;
import bs.boli.domain.Utils;
import bs.boli.domain.test.util.BoLiTestUtils;

public class BoLiXmlParserTest {
    @Test
    public void testToLocalXml() throws IOException {
        // 2012-11-28 19:30
        GregorianCalendar calendar = new GregorianCalendar(2012, Calendar.NOVEMBER, 28, 19, 30, 0);
        calendar.setTimeZone(TimeZone.getTimeZone("UTC"));

        BoLi boli = BoLiTestUtils.createBoLiWithTwoFilledShopsAndThreeCategorizedBs(calendar.getTime());
        BoLiTestUtils.generateAllBoLiEvents(boli);

        String actual = BoLiXmlParser.toLocalXml(boli);

        String expected = Utils.toString(getClass()
                .getResourceAsStream("boliXmlParserTest.local.xstream.xml"));
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testFromLocalXml() throws IOException {
        // prepare input
        String localXml = Utils.toString(getClass()
                .getResourceAsStream("boliXmlParserTest.local.xstream.xml"));

        // action
        BoLi actual = BoLiXmlParser.fromLocalXml(localXml);

        // prepare expected
        GregorianCalendar calendar = new GregorianCalendar(2012, Calendar.NOVEMBER, 28, 19, 30, 0);
        calendar.setTimeZone(TimeZone.getTimeZone("UTC"));

        BoLi expected = BoLiTestUtils.createBoLiWithTwoFilledShopsAndThreeCategorizedBs(calendar.getTime());
        BoLiTestUtils.generateAllBoLiEvents(expected);

        // assert
        BeanDiff beandiff = new BeanDiff.Builder().introspectPackage(BoLi.class.getPackage()).build();
        Difference difference = beandiff.compare(expected, actual);
        Assert.assertNull(ObjectUtils.toString(difference), difference);
    }

    @Test
    public void testFromEventsXml() throws IOException {
        // prepare input
        String eventsXml = Utils.toString(getClass()
                .getResourceAsStream("boliXmlParserTest.events.xstream.xml"));

        // action
        List<BoLiEvent> actual = BoLiXmlParser.fromEventsXml(eventsXml);

        // prepare expected
        GregorianCalendar calendar = new GregorianCalendar(2012, Calendar.NOVEMBER, 28, 19, 30, 0);
        calendar.setTimeZone(TimeZone.getTimeZone("UTC"));

        BoLi expectedBoli = BoLiTestUtils.createBoLiWithTwoFilledShopsAndThreeCategorizedBs(calendar.getTime());
        BoLiTestUtils.generateAllBoLiEvents(expectedBoli);
        List<BoLiEvent> expectedEvents = expectedBoli.getEventsToPublish();

        // assert
        BeanDiff beandiff = new BeanDiff.Builder().introspectPackage(BoLi.class.getPackage()).build();
        Difference difference = beandiff.compare(expectedEvents, actual);
        Assert.assertNull(ObjectUtils.toString(difference), difference);
    }

    @Test
    public void testToSharedXml() throws IOException {
        BoLi boli = BoLiTestUtils.createBoLiWithTwoFilledShopsAndThreeCategorizedBs();

        String actual = BoLiXmlParser.toSharedXml(boli);

        String expected = Utils.toString(getClass().getResourceAsStream(
                "boliXmlParserTest.shared.xstream.xml"));
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testFromSharedXml() throws IOException {
        String shopListXml = Utils.toString(getClass().getResourceAsStream(
                "boliXmlParserTest.shared.xstream.xml"));

        BoLi actual = BoLiXmlParser.fromSharedXml(shopListXml);

        BoLi expected = BoLiTestUtils.createBoLiWithTwoFilledShopsAndThreeCategorizedBs();
        expected.removeEventsToPublish(expected.getEventsToPublish());

        BeanDiff beandiff = new BeanDiff.Builder().introspectPackage(BoLi.class.getPackage()).build();
        Difference difference = beandiff.compare(expected, actual);
        Assert.assertNull(ObjectUtils.toString(difference), difference);
    }

    @Test
    public void testToEventsXml() throws IOException {
        // 2012-11-28 19:30
        GregorianCalendar calendar = new GregorianCalendar(2012, Calendar.NOVEMBER, 28, 19, 30, 0);
        calendar.setTimeZone(TimeZone.getTimeZone("UTC"));

        BoLi boli = BoLiTestUtils.createBoLiWithTwoFilledShopsAndThreeCategorizedBs(calendar.getTime());
        BoLiTestUtils.generateAllBoLiEvents(boli);

        // action
        String actual = BoLiXmlParser.toEventsXml(boli.getEventsToPublish(), boli);

        // assert
        String expected = Utils.toString(getClass().getResourceAsStream(
                "boliXmlParserTest.events.xstream.xml"));
        Assert.assertEquals(expected, actual);
    }
}
