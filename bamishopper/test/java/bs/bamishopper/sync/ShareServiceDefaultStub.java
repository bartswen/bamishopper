package bs.bamishopper.sync;

import java.util.Collection;

import bs.boli.domain.Clock;

public class ShareServiceDefaultStub implements ShareService{

    @Override
    public void share(String filename, String content) throws ShareException {
    }

    @Override
    public void shareNewFile(String filename, String content) throws ShareException {
    }

    @Override
    public String pull(String filename) throws ShareException {
        return null;
    }

    @Override
    public String pullOrCreate(String filename) throws ShareException {
        return null;
    }

    @Override
    public void delete(String filename) throws ShareException {
    }

    @Override
    public void rename(String from, String to) throws ShareException {
    }

    @Override
    public Collection<String> listFilenames(String dir) {
        return null;
    }

    @Override
    public long giveAge(String file, Clock clock) {
        return 0;
    }
}
