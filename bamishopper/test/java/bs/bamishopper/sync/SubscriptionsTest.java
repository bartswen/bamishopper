package bs.bamishopper.sync;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import junit.framework.Assert;

import org.junit.Test;

public class SubscriptionsTest {
    @Test
    public void testParse() {
        Subscriptions sut = new Subscriptions();
        Set<String> actual = sut.parse("subs1,subs2");

        Set<String> expected = new HashSet<String>(Arrays.asList("subs1", "subs2"));
        Assert.assertEquals(expected, actual);

    }

    @Test
    public void testEncode() {
        Subscriptions sut = new Subscriptions();
        String actual = sut.encode(new HashSet<String>(Arrays.asList("subs1", "subs2")));

        String expected = "subs1,subs2";
        Assert.assertEquals(expected, actual);
    }
}
