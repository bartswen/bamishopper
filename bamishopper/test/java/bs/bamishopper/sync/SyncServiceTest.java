package bs.bamishopper.sync;

import java.util.HashMap;
import java.util.Map;

import junit.framework.Assert;

import org.junit.Ignore;
import org.junit.Test;

import bs.boli.domain.Clock;
import bs.boli.domain.StringUtils;
import bs.boli.domain.VoidLogger;
import bs.boli.domain.test.util.BoLiTestUtils;

public class SyncServiceTest {
    @Test
    public void testSubscribe() throws ShareException, InterruptedException {
        // prepare
        final Map<String, String> filenameToContent = new HashMap<String, String>();

        ShareService shareMock = new ShareServiceDefaultStub() {

            @Override
            public void share(String filename, String content) throws ShareException {
                // record sut behaviour
                String currentContent = filenameToContent.get(filename);
                if (currentContent == null) {
                    currentContent = StringUtils.EMPTY;
                }
                filenameToContent.put(filename, currentContent+content);
            }

            @Override
            public String pullOrCreate(String filename) throws ShareException {
                // stub, indirect input for sut
                return "dev1,dev2";
            }
            
            @Override
            public long giveAge(String file, Clock clock) {
                int fileDoesNotExist = -1;
                return fileDoesNotExist;
            }
        };

        // excercise
        SyncService sut = new SyncService(shareMock, BoLiTestUtils.createDummyClock(), new VoidLogger());
        sut.subscribe("newdevice");

        // assert
        Assert.assertNotNull("Not written to expected file", filenameToContent.get(".BaMiShopper/subscriptions.txt"));
        Assert.assertEquals("Unexpected content", "newdevice,dev1,dev2", filenameToContent.get(".BaMiShopper/subscriptions.txt"));
    }

    @Test @Ignore
    public void testUnsubscribe() throws ShareException {
        // prepare
        final Map<String, String> filenameToContent = new HashMap<String, String>();

        ShareService shareMock = new ShareServiceDefaultStub() {

            @Override
            public void share(String filename, String content) throws ShareException {
                // record sut behaviour
                String currentContent = filenameToContent.get(filename);
                if (currentContent == null) {
                    currentContent = StringUtils.EMPTY;
                }
                filenameToContent.put(filename, currentContent+content);
            }

            @Override
            public String pullOrCreate(String filename) throws ShareException {
                // stub, indirect input for sut
                return "dev1,dev2";
            }
            
            @Override
            public long giveAge(String file, Clock clock) {
                int fileDoesNotExist = -1;
                return fileDoesNotExist;
            }
        };

        // excercise
        SyncService sut = new SyncService(shareMock, BoLiTestUtils.createDummyClock(), new VoidLogger());
        sut.unsubscribe("dev1");

        // assert
        Assert.assertNotNull("Not written to expected file", filenameToContent.get(".BaMiShopper/subscriptions.txt"));
        Assert.assertEquals("Unexpected content", "dev2", filenameToContent.get(".BaMiShopper/subscriptions.txt"));
    }

    @Test
    public void testShareEvents() {
        // TODO
    }
}
