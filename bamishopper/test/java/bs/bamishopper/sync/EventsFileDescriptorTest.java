package bs.bamishopper.sync;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import junit.framework.Assert;

import org.junit.Test;

public class EventsFileDescriptorTest {
    @Test
    public void testFromFilename() {
        String filename = "events-2013-01-06T20.26.19-dummy1-dummy2.xml";
        EventsFileDescriptor actual = EventsFileDescriptor.fromFileName(filename);

        Date expectedTime = new GregorianCalendar(2013, Calendar.JANUARY, 6, 20, 26, 19).getTime();
        EventsFileDescriptor expected = new EventsFileDescriptor(expectedTime, Arrays.asList("dummy1",
                "dummy2"));

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testToFilename() {
        Date time = new GregorianCalendar(2013, Calendar.JANUARY, 6, 20, 26, 19).getTime();
        EventsFileDescriptor sut = new EventsFileDescriptor(time, Arrays.asList("dummy1",
                "dummy2"));

        String actual = sut.toFilename();
        
        String expected = "events-2013-01-06T20.26.19-dummy1-dummy2.xml";
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testAddDeviceId() {
        Date time = new GregorianCalendar(2013, Calendar.JANUARY, 6, 20, 26, 19).getTime();
        EventsFileDescriptor sut = new EventsFileDescriptor(time, Arrays.asList("dummy1",
                "dummy2"));
        EventsFileDescriptor actual = sut.addDeviceId("newDev");

        EventsFileDescriptor expected = new EventsFileDescriptor(time, Arrays.asList("dummy1",
                "dummy2","newDev"));
        
        Assert.assertEquals(expected, actual);
    }
}
