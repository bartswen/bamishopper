package bs.bamishopper.sync;

import junit.framework.Assert;

import org.joda.time.DateTime;
import org.junit.Test;

public class DateDiffTest {
    @Test
    public void testDiffJustNow() {
        DateTime first = new DateTime("2013-01-27T21:00");
        DateTime latest = first.plusSeconds(9);
        String actual = DateDiff.diff(first.toDate(), latest.toDate());
        String expected = "just now";
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testDiffSecondsLower() {
        DateTime first = new DateTime("2013-01-27T21:00");
        DateTime latest = first.plusSeconds(10);
        String actual = DateDiff.diff(first.toDate(), latest.toDate());
        String expected = "10 seconds ago";
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testDiffSecondsMed() {
        DateTime first = new DateTime("2013-01-27T21:00");
        DateTime latest = first.plusSeconds(25);
        String actual = DateDiff.diff(first.toDate(), latest.toDate());
        String expected = "20 seconds ago";
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testDiffSecondsUpper() {
        DateTime first = new DateTime("2013-01-27T21:00");
        DateTime latest = first.plusSeconds(59);
        String actual = DateDiff.diff(first.toDate(), latest.toDate());
        String expected = "50 seconds ago";
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testDiffMinutesLower() {
        DateTime first = new DateTime("2013-01-27T21:00");
        DateTime latest = first.plusMinutes(1);
        String actual = DateDiff.diff(first.toDate(), latest.toDate());
        String expected = "1 minute ago";
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testDiffMinutesMed() {
        DateTime first = new DateTime("2013-01-27T21:00");
        DateTime latest = first.plusMinutes(33);
        String actual = DateDiff.diff(first.toDate(), latest.toDate());
        String expected = "33 minutes ago";
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testDiffMinutesUpper() {
        DateTime first = new DateTime("2013-01-27T21:00");
        DateTime latest = first.plusMinutes(59).plusSeconds(59);
        String actual = DateDiff.diff(first.toDate(), latest.toDate());
        String expected = "59 minutes ago";
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testDiffHoursLower() {
        DateTime first = new DateTime("2013-01-27T21:00");
        DateTime latest = first.plusHours(1);
        String actual = DateDiff.diff(first.toDate(), latest.toDate());
        String expected = "1 hour ago";
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testDiffHoursMed() {
        DateTime first = new DateTime("2013-01-27T21:00");
        DateTime latest = first.plusHours(12);
        String actual = DateDiff.diff(first.toDate(), latest.toDate());
        String expected = "12 hours ago";
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testDiffHoursUpper() {
        DateTime first = new DateTime("2013-01-27T21:00");
        DateTime latest = first.plusHours(23).plusMinutes(59).plusSeconds(59);
        String actual = DateDiff.diff(first.toDate(), latest.toDate());
        String expected = "23 hours ago";
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testDiffDaysLower() {
        DateTime first = new DateTime("2013-01-27T21:00");
        DateTime latest = first.plusDays(1);
        String actual = DateDiff.diff(first.toDate(), latest.toDate());
        String expected = "yesterday";
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testDiffDaysMed() {
        DateTime first = new DateTime("2013-01-27T21:00");
        DateTime latest = first.plusDays(33);
        String actual = DateDiff.diff(first.toDate(), latest.toDate());
        String expected = "33 days ago";
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testRoundDifftoSeconds() {
        DateTime first = new DateTime("2013-01-27T21:00");
        DateTime latest = first.plusMinutes(1).plusSeconds(12);
        long actual = DateDiff.roundDiffToSeconds(first.toDate(), latest.toDate());
        long expected = 72;
        Assert.assertEquals(expected, actual);
    }
}
