package bs.bamishopper.sync;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import junit.framework.Assert;

import org.junit.Test;

public class SubscribtionsTest {
    @Test
    public void testParse() {
        String subscribtionsStr = "dev1,dev2";
        Subscriptions sut = new Subscriptions();
        
        Set<String> actual = sut.parse(subscribtionsStr);

        Set<String> expected = createSet("dev1", "dev2");
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testEncode() {
        Set<String> subscriptionsSet = createSet("dev1", "dev2");
        Subscriptions sut = new Subscriptions();
        
        String actual = sut.encode(subscriptionsSet);
        
        String expected = "dev1,dev2";
        Assert.assertEquals(expected, actual);
    }
    
    private static Set<String> createSet(String... items) {
        Set<String> expected = new HashSet<String>();
        expected.addAll(Arrays.asList(items));
        return expected;
    }

}
