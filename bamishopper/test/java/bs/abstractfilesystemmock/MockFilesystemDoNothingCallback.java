package bs.abstractfilesystemmock;

public class MockFilesystemDoNothingCallback implements MockFilesystemCallback {

    @Override
    public void write() {
    }

    @Override
    public void renameTo(String from, String to) {
    }

    @Override
    public void createNewFile(String name) {
    }

    @Override
    public void deleteFile(String name) {
    }
}
