package bs.abstractfilesystemmock;

import bs.abstractfilesystem.AbstractFilenameFilter;
import bs.abstractfilesystem.Directory;

public class DirectoryTestDouble implements Directory<FileTestDouble> {

    @Override
    public FileTestDouble[] listFiles(AbstractFilenameFilter filter) {
        return new FileTestDouble[] {};
    }

}
