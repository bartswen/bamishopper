package bs.abstractfilesystemmock;

import bs.abstractfilesystem.FileSystemFactory;

public class MockFileSystemFactory implements FileSystemFactory<DirectoryTestDouble, FileTestDouble, OutputStreamTestDouble> {

    /** nullable */
    private MockFilesystemCallback callback = null;

    public MockFileSystemFactory() {
        super();
        this.callback = null;
    }

    @Override
    public DirectoryTestDouble createDirectory(String dirname) {
        return new DirectoryTestDouble();
    }

    @Override
    public FileTestDouble createFile(String filename) {
        return new FileTestDouble(callback, filename);
    }

    @Override
    public OutputStreamTestDouble createOutputStream(FileTestDouble file) {
        return new OutputStreamTestDouble(file, callback);
    }

    public void setCallback(MockFilesystemCallback callback) {
        this.callback = callback;
    }
}
