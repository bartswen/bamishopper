package bs.abstractfilesystemmock;

import java.io.IOException;

import bs.abstractfilesystem.AbstractFileOutputStream;

public class OutputStreamTestDouble extends AbstractFileOutputStream<FileTestDouble> {

    /** nullable */
    private final MockFilesystemCallback callback;
    private final FileTestDouble file;

    public OutputStreamTestDouble(FileTestDouble file) {
        this(file, null);
    }

    public OutputStreamTestDouble(FileTestDouble file, MockFilesystemCallback callback) {
        super();
        this.file=file;
        this.callback = callback;
    }
    
    @Override
    public void write(int b) throws IOException {
        if (callback != null) {
            callback.createNewFile(file.name);
            callback.write();
        }
    }
}
