package bs.abstractfilesystemmock;

import bs.abstractfilesystem.AbstractFilenameFilter;

public class FilenameFilterMock implements AbstractFilenameFilter {

    public boolean accept(String filename) {
        return true;
    }

}
