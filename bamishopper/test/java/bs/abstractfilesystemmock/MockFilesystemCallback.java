package bs.abstractfilesystemmock;

public interface MockFilesystemCallback {
    public void renameTo(String from, String to);
    public void write();
    public void createNewFile(String name);
    public void deleteFile(String name);
}
