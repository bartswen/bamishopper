package bs.abstractfilesystemmock;

import bs.abstractfilesystem.AbstractFile;

public class FileTestDouble implements AbstractFile<FileTestDouble> {
    final String name;

    /** nullable */
    private MockFilesystemCallback callback = null;

    public FileTestDouble(String name) {
        super();
        this.name = name;
    }

    public FileTestDouble(MockFilesystemCallback callback, String name) {
        super();
        this.callback = callback;
        this.name = name;
    }

    @Override
    public long lastModified() {
        return 0;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public boolean delete() {
        if (callback != null) {
            callback.deleteFile(name);
        }
        return true;
    }

    @Override
    public boolean renameTo(FileTestDouble dest) {
        if (callback != null) {
            callback.createNewFile(dest.name);
            callback.deleteFile(name);
        }
        return true;
    }

    @Override
    public void createNewFile() {
        if (callback != null) {
            callback.createNewFile(name);
        }
    }

    @Override
    public boolean exists() {
        return false;
    }
}
