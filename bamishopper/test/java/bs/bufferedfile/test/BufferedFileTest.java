package bs.bufferedfile.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TimerTask;

import junit.framework.Assert;

import org.apache.commons.collections.ListUtils;
import org.apache.commons.collections.set.ListOrderedSet;
import org.junit.Test;

import bs.abstractfilesystemmock.FileTestDouble;
import bs.abstractfilesystemmock.MockFileSystemFactory;
import bs.abstractfilesystemmock.MockFilesystemCallback;
import bs.abstractfilesystemmock.MockFilesystemDoNothingCallback;
import bs.bufferedfile.BufferedFile;

public class BufferedFileTest {
    private static final String FILE_BUFFER = "dummyfile.buffer";
    private static final String FILE_LOCK = "dummyfile.lock";
    private static final String FILENAME = "dummyfile";

    @Test
    public void testWriteCreatesLockAndBuffer() {
        // prepare
        final MockFileSystemFactory fact = new MockFileSystemFactory();
        final List<String> expectedCreatedFilesOnWrite = Arrays.asList(FILE_LOCK, FILE_BUFFER);
        
        // use set prevents buffer getting added twice (writer close invokes write on the outputstream)
        final ListOrderedSet actualCreatedFiles = new ListOrderedSet(); 
        MockFilesystemCallback callback = new MockFilesystemDoNothingCallback() {

            @Override
            public void createNewFile(String name) {
                actualCreatedFiles.add(name);
            };

            @Override
            public void write() {
                Assert.assertEquals("expected files should have been created during write in the expected order",
                        expectedCreatedFilesOnWrite, actualCreatedFiles.asList());
            }
        };
        
        fact.setCallback(callback);

        BufferedFile sut = new BufferedFile(FILENAME, fact);

        // action
        sut.write("dummycontent");

        // assert, file is created after write to buffer
        @SuppressWarnings("unchecked")
        List<String> expectedCreatedFilesAfterWrite = (List<String>) ListUtils.union(expectedCreatedFilesOnWrite,
                Arrays.asList(FILENAME));
        Assert.assertEquals("expected files should have been created after write in the expected order",
                expectedCreatedFilesAfterWrite, actualCreatedFiles.asList());
    }

    @Test
    public void testWriteAfterwardsRenameBufferAndRemoveLock() {
        // prepare
        final MockFileSystemFactory fact = new MockFileSystemFactory();

        final List<String> actualCreated = new ArrayList<String>();
        MockFilesystemCallback callback = new MockFilesystemDoNothingCallback() {
            @Override
            public void createNewFile(String name) {
                actualCreated.add(name);
            };

            @Override
            public void write() {
                Assert.assertFalse("during write the file was not yet created", actualCreated.contains(FILENAME));
            }
        };
        fact.setCallback(callback);

        BufferedFile sut = new BufferedFile(FILENAME, fact);

        // action
        sut.write("dummycontent");

        // assert, after write to buffer file is created and renamed from buffer
        Assert.assertTrue("after write the file was created", actualCreated.contains(FILENAME));
    }

    @Test
    public void testWriteRemovesLock() {
        // prepare
        final MutableBoolean lockDeleted = new MutableBoolean(false);
        final FileTestDouble mockLockFile = new FileTestDouble(FILE_LOCK) {
            @Override
            public boolean delete() {
                lockDeleted.setValue(true);
                return true;
            };
        };
        final MockFileSystemFactory fact = new MockFileSystemFactory() {
            @Override
            public FileTestDouble createFile(String filename) {
                if (filename.equals(FILE_LOCK)) {
                    return mockLockFile;
                }
                return super.createFile(filename);
            }
        };

        MockFilesystemCallback callback = new MockFilesystemDoNothingCallback() {
            @Override
            public void write() {
                Assert.assertTrue("lock should exist when write occurs", lockDeleted.isFalse());
            }
        };
        fact.setCallback(callback);

        BufferedFile sut = new BufferedFile(FILENAME, fact);

        // action
        sut.write("dummycontent");

        // assert
        Assert.assertTrue("lock should have been deleted after write has finished", lockDeleted.isTrue());
    }

    @Test
    public void testWriteConcurrentWriteRemovesLockAfterTimeout() {
        final MutableBoolean lockDeleted = new MutableBoolean(false);
        final FileTestDouble mockLockFileExists = new FileTestDouble(FILE_LOCK) {
            @Override
            public boolean exists() {
                return true;
            }

            @Override
            public boolean delete() {
                lockDeleted.setValue(true);
                return super.delete();
            }
        };
        final MockFileSystemFactory fact = new MockFileSystemFactory() {
            @Override
            public FileTestDouble createFile(String filename) {
                if (filename.equals(FILE_LOCK)) {
                    return mockLockFileExists;
                }
                return super.createFile(filename);
            }
        };

        MockFilesystemCallback callback = new MockFilesystemDoNothingCallback() {
            @Override
            public void write() {
                Assert.fail("write should not occur when lock exists");
            }
        };
        fact.setCallback(callback);

        // do not schedule the delete-lock-task, but execute in same thread
        BufferedFile sut = new BufferedFile(FILENAME, fact) {
            @Override
            protected void scheduleTimerTask(TimerTask deleteLockTask, int delayMillisec) {
                deleteLockTask.run();
            }
        };

        // action
        sut.write("dummycontent");

        // assert
        Assert.assertTrue("lock should be deleted after write attempt to locked file", lockDeleted.getValue());
    }
}
