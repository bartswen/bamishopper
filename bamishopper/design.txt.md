IOC and Factories
=================

Although many classes are designed with [DIP](http://en.wikipedia.org/wiki/Dependency_inversion_principle) in mind, BaMiShopper does not use an IOC framework. The `BamishopperFactory` class is responsible for instanciation and coupling of classes with low cohesion. 

Singleton
---------
No class is allowed to implement the [SINGLETON pattern](http://tech.puredanger.com/2007/07/03/pattern-hate-singleton/), except `BamishopperFactory`. Therefore classes of which only one instance must exist should be managed by `BamishopperFactory`.  



Synchronization
===============

Shared access to shared resources in Dropbox exist on two levels:

1. Between two background processes in a running BaMiShopper app
2. Between simoultanuously running apps on several devices

The `BlockingSyncService` is responsible for managing cuncurrency issues in the shared access. It does so by implememting a locking strategy at two levels

1. On the JVM level: operations on the `BlockingSyncService` are synchronized to prevent concurrent access in the app
2. In Dropbox: it creates an `lock` file during the execution of operations; if one `lock` file, younger than 30 sec, already exists then the operation aborts
