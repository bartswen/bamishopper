package bs.bufferedfile;

import java.io.Closeable;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Timer;
import java.util.TimerTask;

import bs.abstractfilesystem.AbstractFile;
import bs.abstractfilesystem.AbstractFileOutputStream;
import bs.abstractfilesystem.FileSystemFactory;
import bs.abstractfilesystem.RealFileSystemFactory;

/**
 * Prevents the file of getting corrupt if the process is interrupted or killed. Implements locking to prevent
 * multiple concurrent writes.
 * <p>
 * If a write occurs when the file is locked then the the BufferedFile will start a timer to remove the lock,
 * and then it just returns false indicating that the write did not take place. The purpose of the scheduled
 * lock removal is to prevent the file to be locked forever. This scenario occurs when the process is killed
 * during a write.
 * <p>
 * Writes to a buffer before renaming the buffer to the target filename. If the process of writing the file is
 * interrupted then the original file, if it existed, will not change. The rename process is atomic so this
 * operation is safe.
 * <p>
 * It creates a temporary lock file during the write process. Concurrent writes will wait for the lock.
 * 
 */
public class BufferedFile {
    private static final String FILEBUFFER_EXT = ".buffer";
    private static final String FILELOCK_EXT = ".lock";
    private static final int DEFAULT_LOCK_TIMEOUT_SEC = 1;
    private static final int MILLISEC_IN_SEC = 1000;

    private final FileSystemFactory<?, ?, ?> factory;
    private final String filename;

    public BufferedFile(String filename, FileSystemFactory<?, ?, ?> factory) {
        this.factory = factory;
        this.filename = filename;
    }

    public BufferedFile(String filename) {
        super();
        factory = createFileSystemFactory();
        this.filename = filename;
    }

    private static FileSystemFactory<?, ?, ?> createFileSystemFactory() {
        return new RealFileSystemFactory();
    }

    public boolean write(String s) {
        return write(s, factory);
    }

    public boolean exists() {
        AbstractFile<?> file = factory.createFile(filename);
        return file.exists();
    }

    private <T extends AbstractFile<T>> boolean write(String s,
            FileSystemFactory<?, T, ? extends AbstractFileOutputStream<T>> factory) {
        if (isLocked()) {
            removeLockAfterTimeout();
            return false;
        }
        T lock = createLock(factory);
        T buffer = factory.createFile(buffername(filename));
        final AbstractFileOutputStream<?> fos = factory.createOutputStream(buffer);
        final OutputStreamWriter osw = new OutputStreamWriter(fos);
        try {
            osw.write(s);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            close(osw);
        }
        T file = factory.createFile(filename);
        buffer.renameTo(file);
        lock.delete();
        return true;
    }

    private void removeLockAfterTimeout() {
        int delayMillisec = DEFAULT_LOCK_TIMEOUT_SEC * MILLISEC_IN_SEC;
        TimerTask deleteLockTask = new TimerTask() {
            @Override
            public void run() {
                AbstractFile<? extends AbstractFile<?>> lock = factory.createFile(lockname(filename));
                lock.delete();
            }
        };
        scheduleTimerTask(deleteLockTask, delayMillisec);

    }

    protected void scheduleTimerTask(TimerTask deleteLockTask, int delayMillisec) {
        Timer timer = new Timer();
        timer.schedule(deleteLockTask, delayMillisec);
    }

    private boolean isLocked() {
        AbstractFile<? extends AbstractFile<?>> lock = factory.createFile(lockname(filename));
        return lock.exists();
    }

    private <T extends AbstractFile<T>> T createLock(FileSystemFactory<?, T, ? extends AbstractFileOutputStream<T>> factory) {
        T lock = factory.createFile(lockname(filename));
        lock.createNewFile();
        return lock;
    }

    private static void close(Closeable closable) {
        if (closable != null) {
            try {
                closable.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private static String buffername(String filename) {
        return filename + FILEBUFFER_EXT;
    }

    private static String lockname(String filename) {
        return filename + FILELOCK_EXT;
    }
}
