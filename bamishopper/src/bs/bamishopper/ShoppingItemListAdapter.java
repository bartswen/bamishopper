package bs.bamishopper;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import bs.boli.domain.Bo;

public class ShoppingItemListAdapter extends ArrayAdapter<Bo> {

	private final Activity activity;
	private final List<Bo> bs = new ArrayList<Bo>();

	public interface MoveListener {
		void moveTo(Bo bo, int index);
	}

	public ShoppingItemListAdapter(Activity activity) {
		super(activity, 0);
		this.activity = activity;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {

		final View row;
		if (convertView != null) {
			row = convertView;
		} else {
			LayoutInflater inflater = activity.getLayoutInflater();
            row = inflater.inflate(R.layout.item_count, null);
		}

		// if(position%2==0){
		// row.setBackgroundColor(android.R.color.darker_gray);
		// }
		//
        TextView naamView = (TextView) row.findViewById(R.id.naam);
		naamView.setText(bs.get(position).getNaam());
		TextView aantalView = (TextView) row.findViewById(R.id.aantal);
		aantalView.setText(Integer.toString(bs.get(position).getAantal()));

		return row;
	}

	public static String findBonaam(View bsListItem, int index) {
		TextView boTextView = (TextView) bsListItem.findViewById(R.id.naam);
		return new StringBuilder(boTextView.getText()).toString();
	}

	@Override
	public int getCount() {
		return bs.size();
	}

	@Override
	public Bo getItem(int position) {
		return bs.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public void update(List<Bo> bs) {
		clearSilent();
		this.bs.addAll(bs);
		notifyDataSetChanged();
	}

	@Override
	public void clear() {
		clearSilent();
		notifyDataSetChanged();
	}

	private void clearSilent() {
		this.bs.clear();
	}
}
