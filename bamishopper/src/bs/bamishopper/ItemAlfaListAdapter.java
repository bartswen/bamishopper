package bs.bamishopper;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import bs.boli.domain.Bo;

public class ItemAlfaListAdapter extends AbstractBoLiAdapter {
	private Activity activity;

	public ItemAlfaListAdapter(Activity activity) {
		super();
		this.activity = activity;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = activity.getLayoutInflater();
		View row = inflater.inflate(R.layout.item_count, null);
		Bo bo = boli.getBoByAlfaIndex(position);

		TextView naam = (TextView) row.findViewById(R.id.naam);
		naam.setText(bo.getNaam());

		TextView aantal = (TextView) row.findViewById(R.id.aantal);
		aantal.setText(Integer.toString(bo.getAantal()));

		return row;
	}
	
	@Override
	public int getCount() {
		return boli.sizeOfBs();
	}

	@Override
	public Object getItem(int position) {
		return boli.getBoByAlfaIndex(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public int indexOf(Bo bo){
	    return boli.getBsAlphabetic().indexOf(bo);
	}
}
