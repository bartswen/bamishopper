package bs.bamishopper;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;
import bs.boli.domain.BoLi;
import bs.boli.domain.Wnk;
import bs.boli.persist.NoStorageAvailable;

import com.commonsware.cwac.tlv.TouchListView;

/**
 * Beheert items in shop. Selecteer de boodschappen die in een bepaalde winkel gekocht worden. Sorteer de
 * geselecteerde boodschappen per winkel op loopvolgorde.
 */
public class ShopItemsActivity extends Activity {
    private BoLiManager boliManager;
    private BoLi boli;

    private ArrayAdapter<String> wnkDropdownAdapter;

    private WnkItemOrderAdapter orderAdapter;

    private WnkItemSelectionAdapter selectionAdapter;

    private TouchListView.DropListener onDrop = new TouchListView.DropListener() {
        public void drop(int from, int to) {
            String item = orderAdapter.getItem(from);
            selectedWnk().toIndex(item, to);
            refresh();
        }
    };

    private TouchListView.RemoveListener onRemove = new TouchListView.RemoveListener() {
        public void remove(int index) {
            String item = orderAdapter.getItem(index);
            boli.removeBoFromWnk(item, selectedWnkNaam());
            Toast.makeText(ShopItemsActivity.this,
                    String.format("%s is uit %s verwijderd", item, selectedWnkNaam()), Toast.LENGTH_SHORT)
                    .show();
            refresh();
        }
    };

    public ShopItemsActivity() {
        super();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityUtils.checkFilesystem(this);
        try {
            BoLiManager.initialize(ActivityUtils.getExternalFilesDir(this));
        } catch (NoStorageAvailable e) {
            ActivityUtils.finishAppWithNostorageMessage(this);
        }
        boliManager = BoLiManager.getInstance();
        boli = boliManager.getBoLi();

        setContentView(R.layout.shop_items_act);

        // TODO gebruik de huidige winkel
        String curwnk = boli.getWnks().get(0).getNaam();
        selectionAdapter = new WnkItemSelectionAdapter(this, boli, curwnk);
        orderAdapter = new WnkItemOrderAdapter(this, boli, curwnk);

        // Restore state en settings
    }

    @Override
    protected void onStart() {
        super.onStart();

        // winkel dropdown
        wnkDropdownAdapter = new ArrayAdapter<String>(this, R.layout.shop) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                TextView view = (TextView) super.getView(position, convertView, parent);
                view.setTextColor(Color.BLACK);
                return view;
            }

            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                TextView view = (TextView) super.getDropDownView(position, convertView, parent);
                view.setTextColor(Color.BLACK);
                return view;
            }
        };

        Spinner spinner = findWnkSpinner();
        spinner.setAdapter(wnkDropdownAdapter);
        spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> arg0, View arg1, int pos, long id) {
                refreshBs();
            }

            public void onNothingSelected(AdapterView<?> arg0) {
                refreshBs();
            }
        });

        // radio buttons and item-lists
        final ViewSwitcher switcher = (ViewSwitcher) findViewById(R.id.switcher);
        RadioGroup selectOrOrder = (RadioGroup) findViewById(R.id.radioGroupSelectOrder);
        selectOrOrder.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                selectionAdapter.notifyDataSetChanged();
                orderAdapter.notifyDataSetChanged();
                switcher.showNext();
            }
        });

        ListView bsList = findBsList();
        bsList.setAdapter(selectionAdapter);

        TouchListView bsTouchList = findBsTouchList();
        bsTouchList.setDropListener(onDrop);
        bsTouchList.setRemoveListener(onRemove);
        bsTouchList.setAdapter(orderAdapter);

        // done button
        Button done = (Button) findViewById(R.id.done);
        done.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_OK);
                finish();
            }
        });

        refresh();
    };

    @Override
    protected void onPause() {
        super.onStop();
        Log.i(this.getClass().getName(), "onPause, isFinishing: " + isFinishing());

        BoLiPersistTask boLiPersistTask = new BoLiPersistTask();
        boLiPersistTask.execute();
    };

    private ListView findBsList() {
        ListView bsList = (ListView) findViewById(R.id.bslist);
        return bsList;
    }

    private TouchListView findBsTouchList() {
        TouchListView bsList = (TouchListView) findViewById(R.id.bstouchlist);
        return bsList;
    }

    private Spinner findWnkSpinner() {
        Spinner spinner = (Spinner) findViewById(R.id.winkel);
        return spinner;
    }

    private String selectedWnkNaam() {
        String wnknaam = (String) findWnkSpinner().getSelectedItem();
        return wnknaam;
    }

    private Wnk selectedWnk() {
        return boli.getWnk(selectedWnkNaam());
    }

    private void refresh() {
        refreshWnk();
        refreshBs();
    }

    private void refreshWnk() {
        String selectedWnkNaam = selectedWnkNaam();

        wnkDropdownAdapter.clear();
        String[] bsNames = Converter.asWnkNames(boli.getWnks());
        for (String bsName : bsNames) {
            wnkDropdownAdapter.add(bsName);
        }
        wnkDropdownAdapter.notifyDataSetChanged();

        int pos = wnkDropdownAdapter.getPosition(selectedWnkNaam);
        findWnkSpinner().setSelection(pos >= 0 ? pos : 0);
    }

    private void refreshBs() {
        String wnknaam = selectedWnkNaam();
        selectionAdapter.setWnk(wnknaam);
        orderAdapter.setWnk(wnknaam);
    }

}
