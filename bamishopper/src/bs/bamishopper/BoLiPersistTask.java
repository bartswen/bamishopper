package bs.bamishopper;

import android.os.AsyncTask;

public class BoLiPersistTask extends AsyncTask<Void, Void, Void> {

    public BoLiPersistTask() {
        super();
    }

    @Override
    protected Void doInBackground(Void... params) {
        BoLiManager.getInstance().persist();
        return null;
    }
}