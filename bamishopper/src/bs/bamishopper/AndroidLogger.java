package bs.bamishopper;

import android.util.Log;
import bs.boli.domain.Logger;

public class AndroidLogger implements Logger {

    @Override
    public void fatal(String context, String message) {
        // compile error!? cannot find symbol while compiling against v.10, wtf() was included in v.8
        // Log.wtf(context, message);
        
        Log.e(context, message);
    }
    @Override
    public void error(String context, String message) {
        Log.e(context, message);
    }

    @Override
    public void warn(String context, String message) {
        Log.w(context, message);
    }

    @Override
    public void info(String context, String message) {
        Log.i(context, message);
    }

    @Override
    public void debug(String context, String message) {
        Log.d(context, message);
    }

    @Override
    public void trace(String context, String message) {
        Log.v(context, message);
    }
}
