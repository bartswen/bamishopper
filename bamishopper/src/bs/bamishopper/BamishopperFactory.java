package bs.bamishopper;

import android.content.Context;
import bs.bamishopper.sync.SyncService;
import bs.bamishopper.sync.DropboxShareService;
import bs.bamishopper.sync.ShareService;
import bs.boli.domain.Clock;
import bs.boli.domain.Logger;
import bs.boli.domain.SystemClock;

/**
 * Instanciates managed objects for BaMiShopper. Only this class is responsible for singletons management.
 */
public final class BamishopperFactory {
    private static BamishopperFactory INSTANCE;

    private final Clock clock;
    private final ShareService shareService;
    private final SyncService syncService;
    private final Logger logger;

    private BamishopperFactory(Context ctx) {
        super();
        clock = new SystemClock();
        shareService = createShareService(ctx);
        logger = new AndroidLogger();
        syncService = createSyncService(shareService, clock, logger);
    }

    public static synchronized void init(Context ctx) {
        if (INSTANCE == null) {
            INSTANCE = new BamishopperFactory(ctx);
        }
    }

    public static synchronized void reinit(Context ctx) {
        INSTANCE = new BamishopperFactory(ctx);
    }

    private static ShareService createShareService(Context ctx) {
        final ShareService shareService;
        if (ActivityUtils.hasDropboxKeys(ctx)) {

            String userKey = ActivityUtils.getStringPref(ctx, R.string.DROPBOX_KEY_KEY);
            String userSecret = ActivityUtils.getStringPref(ctx, R.string.DROPBOX_SECRET_KEY);

            shareService = new DropboxShareService(ActivityUtils.DROPBOX_APP_KEY,
                    ActivityUtils.DROPBOX_APP_SECRET, userKey, userSecret);
        } else {
            shareService = null;
        }

        return shareService;
    }

    private static SyncService createSyncService(ShareService share, Clock clc, Logger l) {
        final SyncService syncService;
        if (share != null && clc != null) {
            syncService = new SyncService(share, clc, l);
        } else {
            syncService = null;
        }
        return syncService;
    }

    public static synchronized BamishopperFactory getInstance() {
        if (INSTANCE == null) {
            throw new RuntimeException("BamishopperFactory was not initialized");
        }
        return INSTANCE;
    }

    public Clock getClock() {
        return clock;
    }

    /** null if factory was initialized without dropbox credentials in preferences */
    public ShareService getShareService() {
        return shareService;
    }

    /** null if factory was initialized without dropbox credentials in preferences */
    public SyncService getSyncService() {
        return syncService;
    }

    public Logger getLogger() {
        return logger;
    }
}
