package bs.bamishopper;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import bs.boli.domain.BoLi;
import bs.boli.domain.BoLiEvent;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.widget.Toast;

public class ActivityUtils {
    // Dropbox
    final static public String DROPBOX_APP_KEY = "nii8uq8b47erodx";
    final static public String DROPBOX_APP_SECRET = "79tz4delny468bc";

    /** Returns null if not present. */
    public static File getExternalFilesDir(Activity act) {
        File externalFilesDir = Environment.getExternalStorageDirectory();
        return externalFilesDir;
    };

    public static void checkFilesystem(Activity act) {
        File externalFilesDir = ActivityUtils.getExternalFilesDir(act);
        if (externalFilesDir == null) {
            ActivityUtils.finishAppWithNostorageMessage(act);
            return;
        }
    }

    public static void finishAppWithNostorageMessage(Activity act) {
        String msg = "Subo: Geen external storage in write modus gevonden. Exit";
        Toast.makeText(act, msg, Toast.LENGTH_LONG).show();
        Intent result = new Intent();
        act.setResult(Activity.RESULT_CANCELED, result);
        act.finish();
    }

    /** Returns true if the dropbox credentials exist in the app preferences. **/
    public static boolean hasDropboxKeys(Context context) {
        String key = getStringPref(context, R.string.DROPBOX_KEY_KEY);
        String secret = getStringPref(context, R.string.DROPBOX_SECRET_KEY);
        return key != null && secret != null;
    }

    public static String getVersionProperty() {
        Properties props = createProps();
        return (String) props.get("version");
    }

    public static Object getBuildProperty() {
        Properties props = createProps();
        return (String) props.get("build");
    }

    /**
     * Loads an int value from the application preferences. The keyname of the preference must be defined in
     * the values/strings resource. This method accepts the resource id of this string as a parameter.
     * 
     * @param context Android context to load the name of the preference-key
     * @param stringResId the resource id of the string representing the preference keyname
     * @return the int value from the preferences, defaults to zero
     */
    public static int getIntPref(Context context, int stringResId) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        String prefKeyName = context.getString(stringResId);
        int intValue = settings.getInt(prefKeyName, 0);
        return intValue;
    }

    /**
     * Loads an long value from the application preferences. The keyname of the preference must be defined in
     * the values/strings resource. This method accepts the resource id of this string as a parameter.
     * 
     * @param context Android context to load the name of the preference-key
     * @param stringResId the resource id of the string representing the preference keyname
     * @return the long value from the preferences, defaults to zero
     */
    public static long getLongPref(Context context, int stringResId) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        String prefKeyName = context.getString(stringResId);
        long longValue = settings.getLong(prefKeyName, 0);
        return longValue;
    }

    /**
     * Sets an int value in the application preferences. The keyname of the preference must be defined in the
     * values/strings resource. This method accepts the resource id of this string as a parameter.
     * 
     * @param context Android context to load the name of the preference-key
     * @param stringResId the resource id of the string representing the preference keyname
     * @param intValue the value to set
     */
    public static void storeIntPref(Context context, int stringResId, int intValue) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = settings.edit();

        String prefKeyName = context.getString(stringResId);
        editor.putInt(prefKeyName, intValue);

        editor.commit();
    }

    /**
     * Sets an long value in the application preferences. The keyname of the preference must be defined in the
     * values/strings resource. This method accepts the resource id of this string as a parameter.
     * 
     * @param context Android context to load the name of the preference-key
     * @param stringResId the resource id of the string representing the preference keyname
     * @param longValue the value to set
     */
    public static void storeLongPref(Context context, int stringResId, long longValue) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = settings.edit();

        String prefKeyName = context.getString(stringResId);
        editor.putLong(prefKeyName, longValue);

        editor.commit();
    }

    /**
     * Sets a string value in the application preferences. The keyname of the preference must be defined in
     * the values/strings resource. This method accepts the resource id of this string as a parameter.
     * 
     * @param context Android context to load the name of the preference-key
     * @param stringResId the resource id of the string representing the preference keyname
     * @param stringValue the value to set
     */
    public static void storeStringPref(Context context, int stringResId, String stringValue) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = settings.edit();

        String prefKeyName = context.getString(stringResId);
        editor.putString(prefKeyName, stringValue);

        editor.commit();
    }

    /**
     * Sets an boolean value in the application preferences. The keyname of the preference must be defined in
     * the values/strings resource. This method accepts the resource id of this string as a parameter.
     * 
     * @param context Android context to load the name of the preference-key
     * @param stringResId the resource id of the string representing the preference keyname
     * @param boolValue the value to set
     */
    public static void storeBooleanPref(Context context, int stringResId, boolean boolValue) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = settings.edit();

        String prefKeyName = context.getString(stringResId);
        editor.putBoolean(prefKeyName, boolValue);

        editor.commit();
    }

    /**
     * Loads a boolean value from the application preferences. The keyname of the preference must be defined
     * in the values/strings resource. This method accepts the resource id of this string as a parameter.
     * 
     * @param context Android context to load the name of the preference-key
     * @param stringResId the resource id of the string representing the preference keyname
     * @return the boolean value from the preferences, defaults to false
     */
    public static boolean getBooleanPref(Context context, int stringResId) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        String prefKeyName = context.getString(stringResId);
        boolean boolValue = settings.getBoolean(prefKeyName, false);
        return boolValue;
    }

    /**
     * Loads a string value from the application preferences. The keyname of the preference must be defined in
     * the values/strings resource. This method accepts the resource id of this string as a parameter.
     * 
     * @param context Android context to load the name of the preference-key
     * @param stringResId the resource id of the string representing the preference keyname
     * @return the string value from the preferences, defaults to null
     */
    public static String getStringPref(Context context, int stringResId) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        String prefKeyName = context.getString(stringResId);
        String stringValue = settings.getString(prefKeyName, null);
        return stringValue;
    }

    private static Properties createProps() {
        Properties props = new Properties();
        InputStream is = ActivityUtils.class.getResourceAsStream("bamishopper.properties");
        try {
            props.load(is);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return props;
    }

    public static String fetchDeviceId(Context context) {
        String id = android.provider.Settings.System.getString(context.getContentResolver(),
                android.provider.Settings.Secure.ANDROID_ID);
        return id;
    }

    public static String eventToastMessage(BoLiEvent e, BoLi b) {
        if (e == null) {
            return "-nada-";
        }
        
        EventFormatter fmt=new EventFormatter(b);
        e.accept(fmt);
        return fmt.format();
    }
}
