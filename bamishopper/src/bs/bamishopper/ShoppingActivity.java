package bs.bamishopper;

import java.util.List;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import bs.boli.domain.Bo;
import bs.boli.domain.BoLi;
import bs.boli.domain.Wnk;

/**
 * Screen controller for Shopping screen. The BoLiManager must be initialized before instanciating this class.
 */
public class ShoppingActivity extends Activity implements BoLiChangeListener {
    private static final int SHOP_SPINNER_SELECTION_DELAY = 200;

    private BoLi boli;

    private ShoppingItemListAdapter bsAdapter;

    private ArrayAdapter<String> wnkDropdownAdapter;

    public ShoppingActivity() {
        super();
        boli = BoLiManager.getInstance().getBoLi();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.shopping_act);
        bsAdapter = new ShoppingItemListAdapter(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        wnkDropdownAdapter = new ArrayAdapter<String>(this, R.layout.shop) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                TextView view = (TextView) super.getView(position, convertView, parent);
                view.setTextColor(Color.BLACK);
                return view;
            }

            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                TextView view = (TextView) super.getDropDownView(position, convertView, parent);
                view.setTextColor(Color.BLACK);
                return view;
            }
        };

        Spinner spinner = findWnkSpinner();
        spinner.setAdapter(wnkDropdownAdapter);
        spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> arg0, View arg1, int pos, long id) {
                refreshBs();
                storeCurrentShop();

            }

            public void onNothingSelected(AdapterView<?> arg0) {
                refreshBs();
            }
        });

        ListView bsList = findBsList();
        bsList.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> arg0, View v, int index, long arg3) {
                String bsnaam = ShoppingItemListAdapter.findBonaam(v, index);
                boli.resetBoByNaam(bsnaam);

                Toast.makeText(ShoppingActivity.this, String.format("%s is gekocht", bsnaam),
                        Toast.LENGTH_SHORT).show();
                refreshBs();
            }
        });
        bsList.setAdapter(bsAdapter);

        refresh();
    };

    private void storeCurrentShop() {
        ActivityUtils.storeStringPref(this, R.string.current_shop_pref_key, selectedWnkNaam());
    }

    private void restoreCurrentShop() {
        final String currentShopname = ActivityUtils.getStringPref(this, R.string.current_shop_pref_key);
        final int currentShopIndex = wnkDropdownAdapter.getPosition(currentShopname);

        final Spinner shopSpinner = findWnkSpinner();
        shopSpinner.postDelayed(new Runnable() {
            @Override
            public void run() {
                shopSpinner.setSelection(currentShopIndex);
            }
        }, SHOP_SPINNER_SELECTION_DELAY);
    }

    @Override
    protected void onResume() {
        super.onResume();
        refresh();
        bsAdapter.notifyDataSetChanged();
        this.boli.registerEventsProcessedListener(this);

        restoreCurrentShop();
        restoreListPosition();
    }

    private void restoreListPosition() {
        final int firstVisible = ActivityUtils.getIntPref(this, R.string.shopping_list_position_pref_key);
        Log.i(this.getClass().getName(), "onResume, restore list position: " + firstVisible);
        final ListView list = findBsList();
        list.post(new Runnable() {
            @Override
            public void run() {
                list.setSelection(firstVisible);
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(this.getClass().getName(), "onPause, isFinishing: " + isFinishing());

        this.boli.unregisterEventsProcessedListener();

        storeListPosition();

        BoLiPersistTask boLiPersistTask = new BoLiPersistTask();
        boLiPersistTask.execute();

    }

    private void storeListPosition() {
        int firstVisiblePosition = findBsList().getFirstVisiblePosition();
        Log.i(this.getClass().getName(), "onPause, store list position: " + firstVisiblePosition);
        ActivityUtils.storeIntPref(this, R.string.shopping_list_position_pref_key, firstVisiblePosition);
    };

    private ListView findBsList() {
        ListView bsList = (ListView) findViewById(R.id.bslist);
        return bsList;
    }

    private Spinner findWnkSpinner() {
        Spinner spinner = (Spinner) findViewById(R.id.winkel);
        return spinner;
    }

    private ShoppingItemListAdapter findBsListAdapter() {
        ListView bsListView = findBsList();
        return (ShoppingItemListAdapter) bsListView.getAdapter();
    }

    private void refresh() {
        refreshBs();
        refreshWnk();
    }

    private void refreshWnk() {
        String selectedWnkNaam = selectedWnkNaam();

        wnkDropdownAdapter.clear();
        String[] bsNames = Converter.asWnkNames(boli.getWnks());
        for (String bsName : bsNames) {
            wnkDropdownAdapter.add(bsName);
        }
        wnkDropdownAdapter.notifyDataSetChanged();

        int pos = wnkDropdownAdapter.getPosition(selectedWnkNaam);
        findWnkSpinner().setSelection(pos);
    }

    private void refreshBs() {
        // boodschappen list
        ShoppingItemListAdapter adapter = findBsListAdapter();

        String wnknaam = selectedWnkNaam();
        if (wnknaam == null) {
            adapter.clear();
        } else {
            List<Bo> bs = boli.teKopenVoor(wnknaam);
            adapter.update(bs);
        }
    }

    private String selectedWnkNaam() {
        String wnknaam = (String) findWnkSpinner().getSelectedItem();
        return wnknaam;
    }

    private Wnk selectedWnk() {
        return boli.getWnk(selectedWnkNaam());
    }

    @Override
    public void eventsConsumed(final int numOfEvents) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
            
                bsAdapter.notifyDataSetChanged();
                Toast.makeText(ShoppingActivity.this, String.format("Consumed %d events", numOfEvents),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void fullPull() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                bsAdapter.notifyDataSetChanged();
                Toast.makeText(ShoppingActivity.this, "List was fully updated", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
