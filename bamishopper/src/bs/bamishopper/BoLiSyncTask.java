package bs.bamishopper;

import android.content.Context;
import android.os.AsyncTask;
import bs.bamishopper.sync.ShareException;
import bs.bamishopper.sync.SyncException;
import bs.boli.domain.BoLi;

public class BoLiSyncTask extends AsyncTask<Void, Void, Void> {
    private final Context ctx;
    private final BoLi boli;

    public BoLiSyncTask(Context ctx, BoLi boli) {
        super();
        this.ctx = ctx;
        this.boli = boli;
    }

    @Override
    protected Void doInBackground(Void... params) {
        String deviceId = ActivityUtils.fetchDeviceId(ctx);
        
        try {
            BamishopperFactory.getInstance().getSyncService().sync(ctx, boli, deviceId);
        } catch (SyncException e) {
            // ignore, probably offline
        } catch (ShareException e) {
            // ignore, probably offline
        }
        
        return null;
    }
}