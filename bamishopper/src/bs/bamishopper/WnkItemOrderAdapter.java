package bs.bamishopper;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import bs.boli.domain.Bo;
import bs.boli.domain.BoLi;

// OnClickListener
public class WnkItemOrderAdapter extends ArrayAdapter<String> {

    private final Activity activity;
    private final BoLi boli;
    private String wnk;

    // private OnCheckedChangeListener checkchange = new OnCheckedChangeListener() {
    // };

    public interface MoveListener {
        void moveTo(Bo bo, int index);
    }

    public WnkItemOrderAdapter(Activity activity, BoLi boli, String wnknaam) {
        super(activity, 0);
        this.activity = activity;
        this.boli = boli;
        this.wnk = wnknaam;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        final View row;
        if (convertView != null) {
            row = convertView;
        } else {
            LayoutInflater inflater = activity.getLayoutInflater();
            row = inflater.inflate(R.layout.order_something, null);
        }

        TextView bo = (TextView) row.findViewById(R.id.nameToOrder);

        final String bonaam = getItem(position);

        bo.setText(bonaam);

        return row;
    }

    public static String findBonaam(View bsListItem, int index) {
        TextView boTextView = (TextView) bsListItem.findViewById(R.id.nameToOrder);
        return new StringBuilder(boTextView.getText()).toString();
    }

    @Override
    public int getCount() {
        return boli.getWnk(wnk).collectBsNames().size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public String getItem(int position) {
        String bonaam = boli.getWnk(wnk).collectBsNames().get(position);
        return bonaam;
    };

    public void setWnk(String wnknaam) {
        this.wnk = wnknaam;
        notifyDataSetChanged();
    }

    private String getItemNameInSelectMode(int index) {
        return boli.getBsAlphabetic().get(index).getNaam();
    }
}
