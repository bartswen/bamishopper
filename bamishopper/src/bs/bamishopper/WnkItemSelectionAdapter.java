package bs.bamishopper;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import bs.boli.domain.Bo;
import bs.boli.domain.BoLi;

// OnClickListener
public class WnkItemSelectionAdapter extends ArrayAdapter<String> implements OnClickListener {

    private final Activity activity;
    private final BoLi boli;
    private String wnk;

    // private OnCheckedChangeListener checkchange = new OnCheckedChangeListener() {
    // };

    public interface MoveListener {
        void moveTo(Bo bo, int index);
    }

    public WnkItemSelectionAdapter(Activity activity, BoLi boli, String wnknaam) {
        super(activity, 0);
        this.activity = activity;
        this.boli = boli;
        this.wnk = wnknaam;
    }

    /** Gebonden aan de checkbox */
    @Override
    public void onClick(View v) {
        if (!(v instanceof CheckBox)) {
            return;
        }
        CheckBox check = (CheckBox) v;
        String item = (String) check.getTag();
        if (check.isChecked()) {
            boli.addBoToWnk(item, wnk);
        } else {
            boli.removeBoFromWnk(item, wnk);
        }
        notifyDataSetChanged();
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        final View row;
        if (convertView != null) {
            row = convertView;
        } else {
            LayoutInflater inflater = activity.getLayoutInflater();
            row = inflater.inflate(R.layout.check_something, null);
        }

        // if(position%2==0){
        // row.setBackgroundColor(android.R.color.darker_gray);
        // }
        //
        CheckBox check = (CheckBox) row.findViewById(R.id.check);
        TextView bo = (TextView) row.findViewById(R.id.nameToCheck);

        final String bonaam = getItemNameInSelectMode(position);
        check.setChecked(boli.getWnk(wnk).heeft(bonaam));
        check.setTag(bonaam);

        check.setOnClickListener(this);

        bo.setText(bonaam);

        return row;
    }

    public static String findBonaam(View bsListItem, int index) {
        TextView boTextView = (TextView) bsListItem.findViewById(R.id.nameToCheck);
        return new StringBuilder(boTextView.getText()).toString();
    }

    @Override
    public int getCount() {
        return boli.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public String getItem(int position) {
        return boli.getBsAlphabetic().get(position).getNaam();
    }

    public void setWnk(String wnknaam) {
        this.wnk = wnknaam;
        notifyDataSetChanged();
    }

    private String getItemNameInSelectMode(int index) {
        return boli.getBsAlphabetic().get(index).getNaam();
    }
}
