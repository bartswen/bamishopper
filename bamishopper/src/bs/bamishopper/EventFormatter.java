package bs.bamishopper;

import bs.boli.domain.Bo;
import bs.boli.domain.BoLi;
import bs.boli.domain.EventVisitor;
import bs.boli.domain.ItemAddedEvent;
import bs.boli.domain.ItemCategoryChangedEvent;
import bs.boli.domain.ItemCountChangedEvent;
import bs.boli.domain.ItemDeletedEvent;
import bs.boli.domain.ItemNameChangedEvent;
import bs.boli.domain.ShopAddedEvent;
import bs.boli.domain.ShopDeletedEvent;
import bs.boli.domain.ShopItemAddedEvent;
import bs.boli.domain.ShopItemMovedEvent;
import bs.boli.domain.ShopItemRemovedEvent;
import bs.boli.domain.ShopNameChangedEvent;
import bs.boli.domain.Wnk;
import bs.optional.Optional;

public class EventFormatter implements EventVisitor {

    private final BoLi boli;

    private String msg;

    public EventFormatter(BoLi boli) {
        super();
        this.boli = boli;
    }

    @Override
    public void visit(ItemCountChangedEvent icce) {
        String item = fetchItemNameFromBoliOrDefaultToId(icce.getItemId());
        if (icce.getNewCount() == 0) {
            msg = String.format("Item count '%s' was reset", item);
        } else {
            msg = String.format("Item count '%s' changed from %d to %d", item, icce.getOldCount(),
                    icce.getNewCount());
        }
    }

    @Override
    public void visit(ItemCategoryChangedEvent icce) {
        String item = fetchItemNameFromBoliOrDefaultToId(icce.getItemId());
        if (icce.getOldCat() == null) {
            msg = String.format("Assigned category '%s' to '%s'", icce.getNewCat(), item);
        } else if (icce.getNewCat() == null) {
            msg = String.format("Cleared category '%s' of '%s'", icce.getOldCat(), item);
        } else {
            msg = String.format("Item category of '%s' changed from '%s' to '%s'", item, icce.getOldCat(),
                    icce.getNewCat());
        }
    }

    @Override
    public void visit(ItemNameChangedEvent ince) {
        msg = String.format("Item name '%s' changed to '%s'", ince.getOldName(), ince.getNewName());
    }

    @Override
    public void visit(ItemAddedEvent iae) {
        msg = String.format("Item '%s' added", iae.getBo().getNaam());
    }

    @Override
    public void visit(ItemDeletedEvent ide) {
        msg = String.format("Item '%s' deleted", ide.getBo().getNaam());
    }

    @Override
    public void visit(ShopAddedEvent sae) {
        msg = String.format("Shop '%s' added", sae.getWnk().getNaam());
    }

    @Override
    public void visit(ShopDeletedEvent sde) {
        msg = String.format("Shop '%s' deleted", sde.getWnk().getNaam());
    }

    @Override
    public void visit(ShopNameChangedEvent sce) {
        msg = String.format("Shop name '%s' changed to ", sce.getOldName(), sce.getNewName());
    }

    @Override
    public void visit(ShopItemMovedEvent sire) {
        String shop = fetchShopNameFromBoliOrDefaultToId(sire.getShopId());
        String item = fetchItemNameFromBoliOrDefaultToId(sire.getItemId());
        msg = String.format("Item '%s' moved in '%s'", item, shop);
    }

    @Override
    public void visit(ShopItemAddedEvent siae) {
        String shop = fetchShopNameFromBoliOrDefaultToId(siae.getShopId());
        String item = fetchItemNameFromBoliOrDefaultToId(siae.getItemId());
        msg = String.format("Item '%s' added to '%s'", item, shop);
    }

    @Override
    public void visit(ShopItemRemovedEvent sire) {
        String shop = fetchShopNameFromBoliOrDefaultToId(sire.getShopId());
        String item = fetchItemNameFromBoliOrDefaultToId(sire.getItemId());
        msg = String.format("Item '%s' removed from '%s'", item, shop);
    }

    public String format() {
        return msg;
    }

    private String itemNameOrDefault(Optional<Bo> optionalBo, String defaultValue) {
        if (optionalBo.isPresent()) {
            return optionalBo.get().getNaam();
        } else {
            return defaultValue;
        }
    }

    private String shopNameOrDefault(Optional<Wnk> optionalWnk, String defaultValue) {
        if (optionalWnk.isPresent()) {
            return optionalWnk.get().getNaam();
        } else {
            return defaultValue;
        }
    }

    private String fetchItemNameFromBoliOrDefaultToId(String itemId) {
        Optional<Bo> optionalBo = Optional.fromNullable(boli.getBoById(itemId));
        String item = itemNameOrDefault(optionalBo, itemId);
        return item;
    }

    private String fetchShopNameFromBoliOrDefaultToId(String shopId) {
        Optional<Wnk> optionalWnk = Optional.fromNullable(boli.getWnkById(shopId));
        String shop = shopNameOrDefault(optionalWnk, shopId);
        return shop;
    }
}
