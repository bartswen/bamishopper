package bs.bamishopper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;
import bs.boli.domain.CollectionUtils;

public class WnkSelectionListAdapter extends BaseAdapter {
	private Activity activity;
	private List<CheckableItem> items;

	private static class CheckableItem {
		private boolean checked;
		private String item;

		public CheckableItem(String item) {
			super();
			this.item = item;
		}

		public static CheckableItem newUncheckedItem(String item) {
			return new CheckableItem(item);
		}

		public static CheckableItem newCheckedItem(String item) {
			CheckableItem checked = new CheckableItem(item);
			checked.checked = true;
			return checked;
		}
	}

	private static class CheckableItemNameComparator implements Comparator<CheckableItem>, Serializable {
        private static final long serialVersionUID = 1L;

        public int compare(CheckableItem lhs, CheckableItem rhs) {
			return lhs.item.compareTo(rhs.item);
		}
	}

	public WnkSelectionListAdapter(Activity activity, Collection<String> allItems,
	        Collection<String> checkedItems) {
		super();
		this.activity = activity;
		this.items = sort(toCheckableItems(allItems, checkedItems), new CheckableItemNameComparator());
	}

	private static Collection<CheckableItem> toCheckableItems(Collection<String> allItems,
	        Collection<String> checkedItems) {
		Collection<String> unchecked = CollectionUtils.subtract(allItems, checkedItems);
		Collection<CheckableItem> allCheckables = new ArrayList<CheckableItem>(allItems.size());
		allCheckables.addAll(toUncheckedItems(unchecked));
		allCheckables.addAll(toCheckedItems(checkedItems));

		return allCheckables;
	}

	private static Collection<? extends CheckableItem> toUncheckedItems(Collection<String> unchecked) {
		List<CheckableItem> items = new ArrayList<CheckableItem>(unchecked.size());

		for (String s : unchecked) {
			items.add(CheckableItem.newUncheckedItem(s));
		}
		return items;
	}

	private static Collection<? extends CheckableItem> toCheckedItems(Collection<String> checked) {
		List<CheckableItem> items = new ArrayList<CheckableItem>(checked.size());

		for (String s : checked) {
			items.add(CheckableItem.newCheckedItem(s));
		}
		return items;
	}

	private static List<String> toItemNames(List<CheckableItem> items) {
		List<String> names = CollectionUtils.collect(items,
		        new CollectionUtils.Transformer<CheckableItem, String>() {
			        public String transform(CheckableItem item) {
				        return item.item;
			        }
		        });
		return names;
	}

	private static <T> List<T> sort(Collection<T> toSort, Comparator<T> comparator) {
		List<T> sortList = new ArrayList<T>(toSort);
		Collections.sort(sortList, comparator);
		return sortList;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = activity.getLayoutInflater();
		View row = inflater.inflate(R.layout.check_something, null);

		TextView naam = (TextView) row.findViewById(R.id.nameToCheck);
		naam.setText(items.get(position).item);

		CheckBox checked = (CheckBox) row.findViewById(R.id.check);
		checked.setChecked(items.get(position).checked);
		checked.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				items.get(position).checked = isChecked;
			}
		});

		return row;
	}

	public int getCount() {
		return items.size();
	}

	public Object getItem(int position) {
		return items.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public List<String> checkedItems() {
		List<CheckableItem> itemsToFilter = new ArrayList<CheckableItem>(items);
		CollectionUtils.filter(itemsToFilter, new CollectionUtils.Predicate<CheckableItem>() {
			public boolean evaluate(CheckableItem item) {
				return item.checked;
			}
		});
		List<String> names = toItemNames(itemsToFilter);
		return names;
	}

	public List<String> uncheckedItems() {
		List<CheckableItem> itemsToFilter = new ArrayList<CheckableItem>(items);
		CollectionUtils.filter(itemsToFilter, new CollectionUtils.Predicate<CheckableItem>() {
			public boolean evaluate(CheckableItem item) {
				return !item.checked;
			}
		});
		List<String> names = toItemNames(itemsToFilter);
		return names;
	}

}
