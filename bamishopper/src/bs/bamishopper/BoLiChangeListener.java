package bs.bamishopper;

public interface BoLiChangeListener {
    /** The Boli has updated its state from events */
    public void eventsConsumed(int numOfEvents);
    
    /** The Boli was overwritten with the state of another Boli */
    public void fullPull();
}
