package bs.bamishopper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.Toast;
import bs.boli.domain.Bo;
import bs.boli.domain.BoLi;
import bs.boli.domain.CollectionUtils;
import bs.boli.domain.CollectionUtils.Transformer;
import bs.boli.domain.Wnk;

/**
 * Screen controller for manage Boodschappen screen. The BoLiManager must be initialized before instanciating
 * this class.
 */
public class MakeListActivity extends ListActivity implements BoLiChangeListener {
    private static final int LIST_REPOSITION_DELAY = 500;
    private static final int BO_CONTEXT_DIALOG = 1;
    private static final int BO_CONTEXT_RESET_INDEX = 0;
    private static final int BO_CONTEXT_NEW_INDEX = 1;
    private static final int BO_CONTEXT_CHANGE_INDEX = 2;
    private static final int BO_CONTEXT_DELETE_INDEX = 3;
    private static final int BO_CONTEXT_CANCEL_INDEX = 4;
    private static final String BO_CONTEXT_RESET_STRING = "Reset";
    private static final String BO_CONTEXT_NEW_STRING = "New";
    private static final String BO_CONTEXT_CHANGE_STRING = "Change";
    private static final String BO_CONTEXT_DELETE_STRING = "Delete";
    private static final String BO_CONTEXT_CANCEL_STRING = "Cancel";

    private final BoLi boli;

    private Bo selectedBo;

    /** Deze activity kan zichzelf van een nieuwe adapter voorzien als de gebruiker de instellingen wijzigt */
    private AbstractBoLiAdapter adapter;

    public MakeListActivity() {
        super();
        boli = BoLiManager.getInstance().getBoLi();
    }

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public Bo getSelectedBo() {
        return selectedBo;
    }

    @Override
    protected void onStart() {
        super.onStart();

        boolean showCategorie = PreferenceManager.getDefaultSharedPreferences(this).getBoolean(
                getString(R.string.cat_pref_key), true);
        createAdapter(showCategorie);
        setListAdapter(adapter);

        ListView lv = getListView();
        lv.setOnItemClickListener(createOnItemClickListener(adapter));
        lv.setOnItemLongClickListener(createOnItemLongClickListener(adapter));
    };

    @Override
    protected void onResume() {
        super.onResume();
        adapter.refresh(boli);
        adapter.notifyDataSetChanged();
        this.boli.registerEventsProcessedListener(this);

        restoreListPosition();
    }

    private void restoreListPosition() {
        final int firstVisible = ActivityUtils.getIntPref(this, R.string.make_list_position_pref_key);
        Log.i(this.getClass().getName(), "onResume, restore list position: " + firstVisible);
        final ListView list = getListView();
        list.post(new Runnable() {
            @Override
            public void run() {
                list.setSelection(firstVisible);
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(this.getClass().getName(), "onPause, isFinishing: " + isFinishing());

        this.boli.unregisterEventsProcessedListener();

        storeListPosition();

        BoLiPersistTask boLiPersistTask = new BoLiPersistTask();
        boLiPersistTask.execute();
    };

    private void storeListPosition() {
        int firstVisiblePosition = getListView().getFirstVisiblePosition();
        Log.i(this.getClass().getName(), "onPause, store list position: " + firstVisiblePosition);
        ActivityUtils.storeIntPref(this, R.string.make_list_position_pref_key, firstVisiblePosition);
    };

    /** (her-) instantieer het adapter field met de juiste implementatie, alleen indien nodig **/
    private void createAdapter(boolean showCategorie) {
        if (showCategorie && !(adapter instanceof ItemInCategoryListAdapter)) {
            adapter = new ItemInCategoryListAdapter(this);
        } else if (!showCategorie && !(adapter instanceof ItemAlfaListAdapter)) {
            adapter = new ItemAlfaListAdapter(this);
        }
    }

    private OnItemClickListener createOnItemClickListener(final AbstractBoLiAdapter adapter) {
        return new OnItemClickListener() {
            public void onItemClick(AdapterView<?> arg0, View view, int position, long rowId) {
                Bo bo = (Bo) adapter.getItem(position);
                if (bo != null) {
                    boli.incrementBoByNaam(bo.getNaam());
                    adapter.notifyDataSetChanged();
                } else {
                    // categorie, negeer
                }
            }
        };
    };

    private OnItemLongClickListener createOnItemLongClickListener(final AbstractBoLiAdapter adapter) {
        return new OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int position, long rowId) {
                Bo bo = (Bo) adapter.getItem(position);
                if (bo != null) {
                    selectedBo = bo;
                    showDialog(BO_CONTEXT_DIALOG);
                    return true;
                } else {
                    // categorie, negeer
                    return false;
                }
            }
        };
    }

    /**
     * Let op, android roept deze alleen de eerste keer aan.
     * 
     * {@inheritDoc}
     */
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
        case BO_CONTEXT_DIALOG:
            return createBoContextDialog();

        default:
            throw new RuntimeException("An error occurred");
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
            case BO_CONTEXT_CHANGE_INDEX:
                updateBoFromActivityResult(data.getExtras());
                break;

            case BO_CONTEXT_NEW_INDEX:
                createNieuweBoFromActivityResult(data.getExtras());
                break;

            default:
                break;
            }
        }
    }

    private static Transformer<Wnk, String> createWnkToNaamTransformer() {
        Transformer<Wnk, String> transformer = new CollectionUtils.Transformer<Wnk, String>() {
            public String transform(Wnk t) {
                return t.getNaam();
            }
        };
        return transformer;
    }

    private void updateBoFromActivityResult(Bundle extras) {
        // parse result
        final String naam = extras.getString(ItemChangeActivity.BO_NAAM_KEY);
        if (naam == null || "".equals(naam)) {
            Toast.makeText(this, "Invalide name, aborted change item", 2).show();
            return;
        }

        Collection<String> boWinkels = extras.getStringArrayList(ItemChangeActivity.BO_WINKELS_LIST_KEY);
        boli.changeBoNaam(selectedBo.getNaam(), naam);

        String categorie = extras.getString(ItemChangeActivity.BO_CATEGORIE_KEY);
        selectedBo.setCategorie(categorie);

        // update boli, voeg toe aan checked winkels
        CollectionUtils.forAllDo(boWinkels, new CollectionUtils.Closure<String>() {
            public void execute(String t) {
                boli.addBoToWnk(naam, t);
            }
        });

        // verwijder bij unchecked winkels
        Collection<String> allWinkels = CollectionUtils.collect(boli.getWnks(), createWnkToNaamTransformer());
        Collection<String> unchecked = CollectionUtils.subtract(allWinkels, boWinkels);
        CollectionUtils.forAllDo(unchecked, new CollectionUtils.Closure<String>() {
            public void execute(String t) {
                boli.removeBoFromWnk(naam, t);
            }
        });

        // notify
        ((BaseAdapter) getListAdapter()).notifyDataSetChanged();
        Toast.makeText(this, String.format("%s is gewijzigd", naam), 2).show();
    }

    private void createNieuweBoFromActivityResult(Bundle extras) {
        // parse result
        final String naam = extras.getString(ItemChangeActivity.BO_NAAM_KEY);
        if (naam == null || "".equals(naam)) {
            Toast.makeText(this, "Invalid name, no new item was created", 2).show();
            return;
        }
        if (naamBestaat(naam)) {
            Toast.makeText(this, String.format("Shopping item '%s' already exists", naam), 2).show();
            return;
        }

        String categorie = extras.getString(ItemChangeActivity.BO_CATEGORIE_KEY);
        Collection<String> boWinkels = extras.getStringArrayList(ItemChangeActivity.BO_WINKELS_LIST_KEY);

        // update boli
        Bo bo = new Bo(naam, categorie);
        boli.addBo(bo);
        Log.i(this.getClass().getName(), "***** boli.getBoByNaam('"+bo.getNaam()+"'):" + boli.getBoByNaam(bo.getNaam())+" - boli:" + boli);
        Log.i(this.getClass().getName(), "***** now go add to shops...");
        try { Thread.sleep(100); } catch (InterruptedException e) {throw new RuntimeException(e);}
        CollectionUtils.forAllDo(boWinkels, new CollectionUtils.Closure<String>() {
            public void execute(String t) {
                boli.addBoToWnk(naam, t);
            }
        });

        // notify
        ((BaseAdapter) getListAdapter()).notifyDataSetChanged();

        final int pos = adapter.indexOf(bo);
        final ListView list = getListView();

        // scroll to position of new item, delay to render list first
        // [http://code.google.com/p/android/issues/detail?id=6741]
        list.postDelayed(new Runnable() {
            @Override
            public void run() {
                list.setSelection(pos);
            }
        }, LIST_REPOSITION_DELAY);

        Toast.makeText(this, String.format("%s was added", naam), 2).show();
    }

    private boolean naamBestaat(String naam) {
        Bo found = boli.getBoByNaamIgnoreCase(naam);
        return found != null;
    }

    private Dialog createBoContextDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(new String[] { BO_CONTEXT_RESET_STRING, BO_CONTEXT_NEW_STRING,
                BO_CONTEXT_CHANGE_STRING, BO_CONTEXT_DELETE_STRING, BO_CONTEXT_CANCEL_STRING },
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                        case BO_CONTEXT_RESET_INDEX:
                            resetBo();
                            break;

                        case BO_CONTEXT_NEW_INDEX:
                            nieuweBo();
                            break;

                        case BO_CONTEXT_CHANGE_INDEX:
                            startWijzigBoActivity();
                            break;

                        case BO_CONTEXT_DELETE_INDEX:
                            verwijderBo();
                            break;

                        case BO_CONTEXT_CANCEL_INDEX:
                            break;

                        default:
                            throw new RuntimeException("Error occurred");
                        }
                    }
                });
        AlertDialog alertDialog = builder.create();
        return alertDialog;
    }

    private void resetBo() {
        boli.resetBoByNaam(selectedBo.getNaam());
        ((BaseAdapter) getListAdapter()).notifyDataSetChanged();
        showResetBoToast(selectedBo);
    }

    private void nieuweBo() {
        Intent intent = new Intent(this, ItemChangeActivity.class);
        Bundle extras = createExtrasForNieuweBo();
        intent.putExtras(extras);
        startActivityForResult(intent, BO_CONTEXT_NEW_INDEX);
    }

    private void startWijzigBoActivity() {
        Intent intent = new Intent(this, ItemChangeActivity.class);
        Bundle extras = createExtrasForWijzigBo();
        intent.putExtras(extras);
        startActivityForResult(intent, BO_CONTEXT_CHANGE_INDEX);
    }

    private Bundle createExtrasForWijzigBo() {
        String boNaam = selectedBo.getNaam();

        Transformer<Wnk, String> transformer = createWnkToNaamTransformer();
        List<Wnk> wnksForBo = boli.getWnksForBo(boNaam);
        Collection<String> checkedWnkNames = CollectionUtils.collect(wnksForBo, transformer);
        List<String> allWinkels = CollectionUtils.collect(boli.getWnks(), transformer);

        Bundle extras = new Bundle();
        extras.putString(ItemChangeActivity.BO_NAAM_KEY, boNaam);
        extras.putStringArrayList(ItemChangeActivity.CATEGORIES_LIST_KEY,
                new ArrayList<String>(boli.getCategories()));
        extras.putStringArrayList(ItemChangeActivity.ALL_WINKELS_LIST_KEY, new ArrayList<String>(allWinkels));
        extras.putString(ItemChangeActivity.BO_CATEGORIE_KEY, selectedBo.getCategorie());
        extras.putStringArrayList(ItemChangeActivity.BO_WINKELS_LIST_KEY, new ArrayList<String>(
                checkedWnkNames));
        return extras;
    }

    private Bundle createExtrasForNieuweBo() {
        Transformer<Wnk, String> transformer = createWnkToNaamTransformer();
        List<String> allWinkels = CollectionUtils.collect(boli.getWnks(), transformer);

        Bundle extras = new Bundle();
        extras.putStringArrayList(ItemChangeActivity.CATEGORIES_LIST_KEY,
                new ArrayList<String>(boli.getCategories()));
        extras.putStringArrayList(ItemChangeActivity.ALL_WINKELS_LIST_KEY, new ArrayList<String>(allWinkels));
        return extras;
    }

    private void verwijderBo() {
        boli.deleteBoByNaam(selectedBo.getNaam());
        ((BaseAdapter) getListAdapter()).notifyDataSetChanged();
        Toast.makeText(this, String.format("%s is verwijderd", selectedBo.getNaam()), 2).show();
    }

    private void showResetBoToast(Bo bo) {
        Toast.makeText(this, String.format("%s is gereset", bo.getNaam()), 2).show();
    }

    @Override
    public void eventsConsumed(final int numOfEvents) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                adapter.notifyDataSetChanged();
                Toast.makeText(MakeListActivity.this, String.format("Consumed %d events", numOfEvents),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void fullPull() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                adapter.notifyDataSetChanged();
                Toast.makeText(MakeListActivity.this, "List was fully updated", Toast.LENGTH_SHORT).show();
            }
        });
    }
}