package bs.bamishopper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import bs.boli.domain.Bo;
import bs.boli.domain.CollectionUtils;
import bs.boli.domain.NullSafeStringValueComparator;

public class ItemInCategoryListAdapter extends AbstractBoLiAdapter {
    private static final String GEEN_CAT = "overige";

    private final Activity activity;

    private List<Item> items = new ArrayList<Item>();

    private enum ItemType {
        BO, CAT
    };

    private static class Item {
        private ItemType itemType;
        private String naam;
        private int aantal;

        public Item(ItemType itemType, String naam, int aantal) {
            super();
            this.itemType = itemType;
            this.naam = naam;
            this.aantal = aantal;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((itemType == null) ? 0 : itemType.hashCode());
            result = prime * result + ((naam == null) ? 0 : naam.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            Item other = (Item) obj;
            if (itemType != other.itemType)
                return false;
            if (naam == null) {
                if (other.naam != null)
                    return false;
            } else if (!naam.equals(other.naam))
                return false;
            return true;
        }

    }

    private static CollectionUtils.Transformer<Bo, Item> BO_TO_ITEM_TRAMSFORMER = new CollectionUtils.Transformer<Bo, ItemInCategoryListAdapter.Item>() {
        public Item transform(Bo bo) {
            return new Item(ItemType.BO, bo.getNaam(), bo.getAantal());
        }
    };

    public ItemInCategoryListAdapter(Activity activity) {
        super();
        this.activity = activity;
        updateItems();
    }

    @Override
    public void notifyDataSetChanged() {
        updateItems();
        super.notifyDataSetChanged();
    };

    /** Converteer alle categorieen en boodschappen in de categorien naar lijst items */
    private void updateItems() {
        final Map<String, List<Bo>> bsByCat = boli.getBsGroupByCat();
        items.clear();

        final CollectionUtils.Closure<String> catAndBsInCatToItemTransformer = new CollectionUtils.Closure<String>() {
            public void execute(String cat) {
                String catItemNaam = (cat != null) ? cat : GEEN_CAT;
                items.add(new Item(ItemType.CAT, catItemNaam, 0));
                List<Item> bsInCat = CollectionUtils.collect(bsByCat.get(cat), BO_TO_ITEM_TRAMSFORMER);
                items.addAll(bsInCat);
            }
        };

        Set<String> catSet = bsByCat.keySet();
        List<String> catList = new ArrayList<String>(catSet);
        Collections.sort(catList, new NullSafeStringValueComparator(false));
        CollectionUtils.forAllDo(catList, catAndBsInCatToItemTransformer);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = activity.getLayoutInflater();

        Item item = items.get(position);

        final View row;
        switch (item.itemType) {
        case BO:
            row = inflater.inflate(R.layout.item_count, null);
            TextView bonaam = (TextView) row.findViewById(R.id.naam);
            bonaam.setText(item.naam);
            TextView aantal = (TextView) row.findViewById(R.id.aantal);
            aantal.setText(Integer.toString(item.aantal));
            break;

        case CAT:
            row = inflater.inflate(R.layout.cat_label, null);
            TextView naam = (TextView) row.findViewById(R.id.naam);
            naam.setText(item.naam);
            break;

        default:
            throw new AssertionError("Unknown item type");
        }

        return row;
    }

    public int getCount() {
        return items.size();
    }

    public Object getItem(int position) {
        Item item = items.get(position);
        if (item.itemType == ItemType.CAT) {
            return null;
        }

        return boli.getBoByNaam(item.naam);
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public int indexOf(Bo bo) {
        return items.indexOf(BO_TO_ITEM_TRAMSFORMER.transform(bo));
    }
}
