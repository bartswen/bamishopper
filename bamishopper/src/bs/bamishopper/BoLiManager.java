package bs.bamishopper;

import java.io.File;

import bs.boli.domain.BoLi;
import bs.boli.persist.BoLiPersister;
import bs.boli.persist.NoStorageAvailable;

public class BoLiManager {
    private static volatile BoLiManager INSTANCE;
    
    private final BoLi boli;
    
    private final BoLiPersister persister;

    private BoLiManager(File rootDir) throws NoStorageAvailable {
        persister = BoLiPersister.getInstance(rootDir.getAbsolutePath());
        boli = persister.read();
    }

    public static void initialize(File rootDir) throws NoStorageAvailable {
        if (INSTANCE == null) {
            INSTANCE = new BoLiManager(rootDir);
        }
        
    }

    public static synchronized BoLiManager getInstance() {
        if (INSTANCE == null) {
            throw new RuntimeException("Call initialize before getting the instance.");
        }
        return INSTANCE;
    }

    public BoLi getBoLi() {
        return boli;
    }
    
    public void persist() {
        persister.write(boli);
    }

}
