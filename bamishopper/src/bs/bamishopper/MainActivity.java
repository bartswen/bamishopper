package bs.bamishopper;

import java.util.Date;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TabActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;
import bs.bamishopper.sync.DateDiff;
import bs.bamishopper.sync.ShareException;
import bs.bamishopper.sync.SyncException;
import bs.boli.domain.BoLi;
import bs.boli.domain.BoLiEvent;
import bs.boli.domain.StringUtils;
import bs.boli.domain.Wnk;
import bs.boli.persist.NoStorageAvailable;

/**
 * Contains two tabs. Responsible for the main menu.
 */
public class MainActivity extends TabActivity {
    private static final int ACT_PREFS = 1;
    private static final int ACT_SHOP_LIST = 2;
    private static final int ADD_SHOP_DIALOG = 1;
    private static final int SYNC_DIALOG = 3;
    private static final int ABOUT_DIALOG = 4;

    private BoLiManager boliManager;
    private BoLi boli;

    public MainActivity() {
        super();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // storage
        ActivityUtils.checkFilesystem(this);
        try {
            BoLiManager.initialize(ActivityUtils.getExternalFilesDir(this));
        } catch (NoStorageAvailable e) {
            ActivityUtils.finishAppWithNostorageMessage(this);
        }
        boliManager = BoLiManager.getInstance();
        boli = boliManager.getBoLi();

        // tabs
        TabHost tabHost = getTabHost();
        {
            View bsView = LayoutInflater.from(this).inflate(R.layout.tab, null);
            TextView bsText = (TextView) bsView.findViewById(R.id.tabsText);
            bsText.setText("Make List");
            tabHost.addTab(tabHost.newTabSpec("bs").setIndicator(bsView)
                    .setContent(new Intent(this, MakeListActivity.class)));
        }

        {
            View wnkView = LayoutInflater.from(this).inflate(R.layout.tab, null);
            TextView wnkText = (TextView) wnkView.findViewById(R.id.tabsText);
            wnkText.setText("Shopping");
            tabHost.addTab(tabHost.newTabSpec("two").setIndicator(wnkView)
                    .setContent(new Intent(this, ShoppingActivity.class)));
        }

        // restore state en settings
        int currentTab = ActivityUtils.getIntPref(this, R.string.CURRENT_TAB_KEY);
        getTabHost().setCurrentTab(currentTab);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);

        MenuItem catMenu = menu.findItem(R.id.settings);
        catMenu.setOnMenuItemClickListener(new OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                startActivityForResult(new Intent(MainActivity.this, PrefsActivity.class), ACT_PREFS);
                return false;
            }
        });

        MenuItem shopListItem = menu.findItem(R.id.shopItems);
        shopListItem.setOnMenuItemClickListener(new OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                startActivityForResult(new Intent(MainActivity.this, ShopItemsActivity.class), ACT_SHOP_LIST);
                return false;
            }
        });

        MenuItem addshop = menu.findItem(R.id.addShop);
        addshop.setOnMenuItemClickListener(new OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                showDialog(ADD_SHOP_DIALOG);
                return false;
            }
        });

        MenuItem undo = menu.findItem(R.id.undo);
        undo.setOnMenuItemClickListener(new OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                BoLiEvent undone = boli.undo();
                String eventMsg = ActivityUtils.eventToastMessage(undone, boli);
                String msg = String.format("Undone: %s", eventMsg);
                Toast.makeText(MainActivity.this, msg, Toast.LENGTH_LONG).show();
                if (undone != null) {
                    restartActivity();
                }
                return false;
            }
        });

        setupSyncOption(menu);

        MenuItem about = menu.findItem(R.id.about);
        about.setOnMenuItemClickListener(new OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                showDialog(ABOUT_DIALOG);
                return false;
            }
        });

        return true;
    };

    /** Enable or disable the sync option depending on the sync preference **/
    private void setupSyncOption(Menu menu) {
        MenuItem sync = menu.findItem(R.id.sync);
        boolean syncChecked = isSyncPrefChecked();
        if (syncChecked) {
            sync.setOnMenuItemClickListener(new OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem item) {
                    showDialog(SYNC_DIALOG);
                    return false;
                }
            });
        } else {
            sync.setEnabled(false);
            sync.setVisible(false);
        }
    }

    private boolean isSyncPrefChecked() {
        boolean sync = ActivityUtils.getBooleanPref(this, R.string.sync_pref_key);
        return sync;
    };

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
        case ADD_SHOP_DIALOG:
            return createShopnameDialog();
        case SYNC_DIALOG:
            return createSyncDialog();
        case ABOUT_DIALOG:
            return createAboutDialog();
        default:
            throw new RuntimeException("An error occurred");
        }
    };

    private Dialog createShopnameDialog() {
        final Dialog dialog = new Dialog(this);

        dialog.setContentView(R.layout.shop_name_dialog);
        dialog.setTitle("Shop Name");

        Button ok = (Button) dialog.findViewById(R.id.ok);
        ok.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText textView = (EditText) dialog.findViewById(R.id.shopname);
                String name = textView.getText().toString();
                if (StringUtils.isEmpty(name)) {
                } else if (existShop(name)) {
                    String msg = String.format("Shop %s already exists, not added", name);
                    Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
                } else {
                    boli.addWnk(new Wnk(name));
                    String msg = String.format("Shop %s was added", name);
                    Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
                }
                dialog.dismiss();
            }
        });

        Button cancel = (Button) dialog.findViewById(R.id.annuleer);
        cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        return dialog;
    }

    private boolean existShop(String name) {
        return boli.getWnkIgnoreCase(name) != null;
    }

    private Dialog createSyncDialog() {
        // dialog
        final Dialog dialog = new Dialog(this) {
            @Override
            public void onAttachedToWindow() {
                super.onAttachedToWindow();
                renderSyncDialogLastSyncData(this);
            }
        };

        dialog.setContentView(R.layout.sync_dialog);
        dialog.setTitle("Sync");

        Button push = (Button) dialog.findViewById(R.id.syncnow);
        push.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                synchronizeManually();
            }
        });

        Button pull = (Button) dialog.findViewById(R.id.pull);
        pull.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                pullManually();
            }
        });

        return dialog;
    }

    void renderSyncDialogLastSyncData(final Dialog dialog) {
        TextView lastAction = (TextView) dialog.findViewById(R.id.lastsyncaction);
        long lastSyncMillis = ActivityUtils.getLongPref(this, R.string.sync_last_action);
        if (lastSyncMillis != 0) {
            String lastActionTxt = DateDiff.diff(new Date(lastSyncMillis), BamishopperFactory.getInstance()
                    .getClock().now());
            lastAction.setText(lastActionTxt);
        }

        TextView longestDuration = (TextView) dialog.findViewById(R.id.longestsynduration);
        int longestDurationInt = ActivityUtils.getIntPref(this, R.string.sync_longes_duration);
        if (longestDurationInt != 0) {
            longestDuration.setText(longestDurationInt + " seconds");
        }
    }

    private void synchronizeManually() {
        final ProgressDialog hourglassDialog = createAndShowHourglassDialog();

        AsyncTask<Void, Void, Boolean> task = new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... params) {
                try {
                    return BamishopperFactory.getInstance().getSyncService()
                            .sync(MainActivity.this, boli, ActivityUtils.fetchDeviceId(MainActivity.this));
                } catch (ShareException e) {
                    return false;
                } catch (SyncException e) {
                    return false;
                }
            }

            @Override
            protected void onPostExecute(Boolean result) {

                hourglassDialog.dismiss();

                if (result.booleanValue()) {

                    Toast.makeText(MainActivity.this, "Synchronize successful", Toast.LENGTH_LONG).show();

                    new BoLiPersistTask().execute();

                } else {

                    Toast.makeText(MainActivity.this, "Synchronize failed, check network and try again",
                            Toast.LENGTH_LONG).show();
                }
            };

        };
        task.execute(null);
    }

    private void pullManually() {
        final ProgressDialog hourglassDialog = createAndShowHourglassDialog();

        AsyncTask<Void, Void, Boolean> task = new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... params) {
                try {
                    return BamishopperFactory.getInstance().getSyncService().pull(boli, MainActivity.this);
                } catch (SyncException e) {
                    return false;
                }

            }

            @Override
            protected void onPostExecute(Boolean result) {

                hourglassDialog.dismiss();

                if (result.booleanValue()) {

                    Toast.makeText(MainActivity.this, "Pull shoppinglist successful", Toast.LENGTH_LONG)
                            .show();

                    new BoLiPersistTask().execute();

                } else {

                    Toast.makeText(MainActivity.this,
                            "Pull shoppinglist failed, check network and try again", Toast.LENGTH_LONG)
                            .show();

                }
            };

        };
        task.execute(null);
    }

    private ProgressDialog createAndShowHourglassDialog() {
        ProgressDialog dialog = new ProgressDialog(MainActivity.this);
        dialog.setMessage("Synchronize with Dropbox...");
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        dialog.show();
        return dialog;
    }

    private Dialog createAboutDialog() {
        String sep = System.getProperty("line.separator");
        String id = ActivityUtils.fetchDeviceId(this);
        String msg = new StringBuilder().append("Version: ").append(ActivityUtils.getVersionProperty())
                .append(sep).append("Build: ").append(ActivityUtils.getBuildProperty()).append(sep)
                .append(String.format("Device id: %s", id)).toString();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("About BaMiShopper").setMessage(msg).setCancelable(false)
                .setPositiveButton("Cheers", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog dialog = builder.create();
        return dialog;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
            case ACT_PREFS:
            case ACT_SHOP_LIST:
                restartActivity();
                break;

            default:
                break;
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        BamishopperFactory.init(this);
        sync();
    };

    @Override
    protected void onPause() {
        super.onPause();

        // sync
        try {
            sync();
        } catch (ShareException e) {
            Log.w(this.getClass().getName(), "Share events failed: " + e.toString());
        }

        // Save state en settings
        BoLiPersistTask boLiPersistTask = new BoLiPersistTask();
        boLiPersistTask.execute();

        int currentTab = getTabHost().getCurrentTab();
        ActivityUtils.storeIntPref(this, R.string.CURRENT_TAB_KEY, currentTab);
    };

    /** Synchronizes state if enabled */
    private void sync() {
        fixInconsistentSyncState();

        boolean syncEnabled = ActivityUtils.getBooleanPref(this, R.string.sync_pref_key);
        if (syncEnabled) {

            BoLiSyncTask syncTask = new BoLiSyncTask(this, boli);
            syncTask.execute();

        } else {

            boli.clearAllEventsToPublish();

        }
    }

    /**
     * Set sync pref to false if dropbox credentials not present. This scenario occurs under rare
     * circumstances.
     */
    private void fixInconsistentSyncState() {
        boolean syncEnabled = ActivityUtils.getBooleanPref(this, R.string.sync_pref_key);
        if (syncEnabled && !ActivityUtils.hasDropboxKeys(this)) {
            ActivityUtils.storeBooleanPref(this, R.string.sync_pref_key, false);
            Log.w(this.getClass().getName(),
                    "Fixed inconsistent sync state: could not find dropbox credentials, disabled sync pref ");
        }
    }

    private void restartActivity() {
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }
}
