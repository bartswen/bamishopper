package bs.bamishopper;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import bs.boli.domain.StringUtils;

/**
 * Screen controller for change Bo screen.
 * Api for this activity in the intent extras bundle:
 * - boNaam:	 [in out]	String Bo name
 * - categories: [in]  		List<String> all known categories
 * - allWinkels: [in]  		List<String> all known winkels
 * - categorie:  [out]  	String the chosen or new categorie for the Bo
 * - boWinkels:	 [in out]	List<String> the winkels that Bo is associated with
 */
public class ItemChangeActivity extends Activity {
	public static final String BO_NAAM_KEY = "boNaam";
	public static final String CATEGORIES_LIST_KEY = "categories";
	public static final String ALL_WINKELS_LIST_KEY = "allWinkels";
	public static final String BO_CATEGORIE_KEY = "categorie";
	public static final String BO_WINKELS_LIST_KEY = "boWinkels";

	private static final int NEW_CAT_DIALOG = 0;
	private static final int SEL_WNK_DIALOG = 1;

	// final
	private List<String> categories;
	private List<String> allWinkels;

	// state
	private ArrayList<String> boWinkels;

	public ItemChangeActivity() {
		super();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.item_change_act);

		Intent intent = getIntent();
		Bundle extras = intent.getExtras();
		extractContext(extras);

		String boNaam = StringUtils.emptyIfNull(extras.getString(BO_NAAM_KEY));
		TextView naamTxt = findNaamTextView();
		naamTxt.setText(boNaam);

		Spinner cats = (Spinner) findViewById(R.id.categorie);
		categories.add(0, "-- geen --");
		ArrayAdapter<String> catsAdapter = new ArrayAdapter<String>(this, R.layout.cat, categories) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				TextView view = (TextView) super.getView(position, convertView, parent);
				view.setTextColor(Color.BLACK);
				return view;
			}

			@Override
			public View getDropDownView(int position, View convertView, ViewGroup parent) {
				TextView view = (TextView) super.getDropDownView(position, convertView, parent);
				view.setTextColor(Color.BLACK);
				return view;
			}
		};
		cats.setAdapter(catsAdapter);
		String categorie = extras.getString(BO_CATEGORIE_KEY);
		int currentIndex = categories.indexOf(categorie);
		cats.setSelection(currentIndex);

		Button newcatBtn = (Button) findViewById(R.id.newcat);
		newcatBtn.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				showNewCatDialog();
			}
		});

		Button winkelsBtn = (Button) findViewById(R.id.winkels);
		winkelsBtn.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				showSelectWinkelsDialog();
			}
		});

		View okBtn = findViewById(R.id.ok);
		okBtn.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent result = createResultIntent();
				setResult(RESULT_OK, result);
				finish();
			}

		});

		View cancelBtn = findViewById(R.id.annuleer);
		cancelBtn.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				setResult(RESULT_CANCELED);
				finish();
			}
		});

		refreshView();
	}

	private void refreshView() {
		Button winkelsBtn = (Button) findViewById(R.id.winkels);
		winkelsBtn.setText(createWnkBtnText());

		Spinner cats = (Spinner) findViewById(R.id.categorie);
		ArrayAdapter<String> catsAdapter = (ArrayAdapter<String>) cats.getAdapter();
		catsAdapter.notifyDataSetChanged();
	}

	private Intent createResultIntent() {
		Intent result = new Intent();
		Bundle extras = new Bundle();

		Spinner cats = (Spinner) findViewById(R.id.categorie);
		if (cats.getSelectedItemPosition() > 0) {
			String categorie = (String) cats.getSelectedItem();
			extras.putString(BO_CATEGORIE_KEY, categorie);
		}

		TextView naam = findNaamTextView();
		extras.putString(BO_NAAM_KEY, new StringBuilder(naam.getText()).toString());

		extras.putStringArrayList(BO_WINKELS_LIST_KEY, boWinkels);

		result.putExtras(extras);
		return result;
	}

	private void extractContext(Bundle extras) {
		if (extras == null) {
			throw new RuntimeException(
			        "No context information was sent to this component. Fill the extras fields of the intent.");
		}
		try {
			categories = extras.getStringArrayList(CATEGORIES_LIST_KEY);
			allWinkels = extras.getStringArrayList(ALL_WINKELS_LIST_KEY);
			if (extras.containsKey(BO_WINKELS_LIST_KEY)) {
				boWinkels = new ArrayList<String>(extras.getStringArrayList(BO_WINKELS_LIST_KEY));
			} else {
				boWinkels = new ArrayList<String>();
			}
		} catch (RuntimeException e) {
			throw new RuntimeException(
			        "Wrong context information was sent to this component in the extras fields of the intent.",
			        e);
		}

	}

	private CharSequence createWnkBtnText() {
		StringBuilder sb = new StringBuilder("Winkels [");
		if (boWinkels.isEmpty()) {
			sb.append("geen");
		}
		for (int i = 0; i < boWinkels.size(); i++) {
			String name = boWinkels.get(i);
			if (i > 0) {
				sb.append(", ");
			}
			sb.append(name);
		}
		sb.append("]");
		return sb.toString();
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		final Dialog dialog;
		switch (id) {
		case NEW_CAT_DIALOG:
			dialog = createNewCatDialog();
			break;
		case SEL_WNK_DIALOG:
			dialog = createSelectWinkelsDialog();
			break;
		default:
			throw new RuntimeException("Unknown dialog id: " + id);
		}
		return dialog;
	}

	private void showNewCatDialog() {
		showDialog(NEW_CAT_DIALOG);
	}

	private void showSelectWinkelsDialog() {
		showDialog(SEL_WNK_DIALOG);
	}

	private Dialog createNewCatDialog() {
		final Dialog dialog = new Dialog(this);

		dialog.setContentView(R.layout.cat_dialog);

		View cancel = dialog.findViewById(R.id.annuleer);
		cancel.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				dialog.cancel();
			}
		});

		View ok = dialog.findViewById(R.id.ok);
		ok.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				CharSequence newCatCs = ((TextView) dialog.findViewById(R.id.cat)).getText();
				String newCat = new StringBuilder(newCatCs).toString();
				categories.add(newCat);
				Spinner cats = findCategoriesSpinner();
				ArrayAdapter<?> adapter = (ArrayAdapter<?>) cats.getAdapter();
				adapter.notifyDataSetChanged();
				cats.setSelection(categories.size() - 1);
				dialog.cancel();
			}

		});
		return dialog;
	}

	private Dialog createSelectWinkelsDialog() {
		final Dialog dialog = new Dialog(this);
		dialog.setTitle(findNaamTextView().getText());

		dialog.setContentView(R.layout.select_shop_dialog);

		ListView listView = (ListView) dialog.findViewById(R.id.wnkSelList);

		ListAdapter adapter = new WnkSelectionListAdapter(this, allWinkels, boWinkels);
		listView.setAdapter(adapter);

		View cancel = dialog.findViewById(R.id.annuleer);
		cancel.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				dialog.cancel();
			}
		});

		View ok = dialog.findViewById(R.id.ok);
		ok.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				ListView listView = (ListView) v.getRootView().findViewById(R.id.wnkSelList);
				WnkSelectionListAdapter adapter = (WnkSelectionListAdapter) listView.getAdapter();
				boWinkels = new ArrayList<String>(adapter.checkedItems());
				dialog.cancel();
				refreshView();
			}
		});
		return dialog;
	}

	private TextView findNaamTextView() {
		TextView naam = (TextView) findViewById(R.id.naam);
		return naam;
	}

	private Spinner findCategoriesSpinner() {
		Spinner cats = (Spinner) findViewById(R.id.categorie);
		return cats;
	}
}
