package bs.bamishopper;

import android.widget.BaseAdapter;
import bs.boli.domain.Bo;
import bs.boli.domain.BoLi;

public abstract class AbstractBoLiAdapter extends BaseAdapter {

	protected BoLi boli = BoLiManager.getInstance().getBoLi();
	
	public void refresh(BoLi boli) {
		this.boli = boli;
		notifyDataSetChanged();
	}
	
	public abstract int indexOf(Bo bo);
}
