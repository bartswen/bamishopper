package bs.bamishopper.sync;

/**
 * Log before throwing.
 */
public class ShareException extends RuntimeException {

    public ShareException() {
        super();
    }

    public ShareException(String string) {
        super(string);
    }
}
