package bs.bamishopper.sync;

import java.util.Collection;

import bs.boli.domain.Clock;


public interface ShareService {
    void share(String filename, String content) throws ShareException;

    void shareNewFile(String filename, String content) throws ShareException;

    String pull(String filename) throws ShareException;

    String pullOrCreate(String filename) throws ShareException;

    void delete(String filename) throws ShareException;

    void rename(String from, String to) throws ShareException;

    Collection<String> listFilenames(String dir);

    long giveAge(String file, Clock clock);
}
