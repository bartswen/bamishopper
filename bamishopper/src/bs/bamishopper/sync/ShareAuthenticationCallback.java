package bs.bamishopper.sync;

import android.util.Pair;

/**
 * Connects the app to the synchronize service for a user.
 */
public interface ShareAuthenticationCallback {
    /**
     * Callback from the synchronize service user-login. Store the user credentials for this app to be able
     * to setup a session quickly during synchronize actions.
     * 
     * @return key and secret to store for automatic session setup during synchronize actions
     */
    Pair<String, String> finishAuthentication() throws ShareException;
}
