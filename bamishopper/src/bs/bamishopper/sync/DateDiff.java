package bs.bamishopper.sync;

import java.util.Date;

public class DateDiff {
    private final static int MSEC_IN_SEC = 1000;
    private static final int SEC_IN_MIN = 60;
    private static final int MIN_IN_HOUR = 60;
    private static final int HOURS_IN_DAY = 24;

    public static String diff(Date first, Date latest) {
        long diff = latest.getTime() - first.getTime();
        if (diff < 10 * MSEC_IN_SEC) {
            return "just now";
        } else if (diff < 1 * SEC_IN_MIN * MSEC_IN_SEC) {
            return String.format("%d seconds ago", roundToTenSec(diff));
        } else if (diff < 1 * MIN_IN_HOUR * SEC_IN_MIN * MSEC_IN_SEC) {
            long minutes = roundToMinutes(diff);
            if (minutes == 1) {
                return "1 minute ago";
            } else {
                return String.format("%d minutes ago", minutes);
            }
        } else if (diff < 1 * HOURS_IN_DAY * MIN_IN_HOUR * SEC_IN_MIN * MSEC_IN_SEC) {
            long hours = roundToHours(diff);
            if (hours == 1) {
                return "1 hour ago";
            } else {
                return String.format("%d hours ago", hours);
            }
        } else {
            long days = roundToDays(diff);
            if (days == 1) {
                return "yesterday";
            } else {
                return String.format("%d days ago", days);
            }
        }
    }

    public static long roundDiffToSeconds(Date first, Date latest) {
        long diff = latest.getTime() - first.getTime();
        return diff / MSEC_IN_SEC;
    }

    private static long roundToDays(long diff) {
        return diff / (MSEC_IN_SEC * SEC_IN_MIN * MIN_IN_HOUR * HOURS_IN_DAY);
    }

    private static long roundToHours(long diff) {
        return diff / (MSEC_IN_SEC * SEC_IN_MIN * MIN_IN_HOUR);
    }

    private static long roundToMinutes(long diff) {
        return diff / (MSEC_IN_SEC * SEC_IN_MIN);
    }

    private static long roundToTenSec(long diff) {
        long sec = diff / MSEC_IN_SEC;
        return sec - (sec % 10);
    }
}
