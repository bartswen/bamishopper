package bs.bamishopper.sync;

/**
 * Log before throwing.
 */
public class SyncException extends RuntimeException {

    public SyncException() {
        super();
    }

    public SyncException(String string) {
        super(string);
    }

    public SyncException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public SyncException(Throwable throwable) {
        super(throwable);
    }
}
