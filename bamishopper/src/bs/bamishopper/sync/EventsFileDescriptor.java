package bs.bamishopper.sync;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

/**
 * Parses and formats an events filename. events-2013-01-06T20.27.39-e8473afd4af6f1b2-someotherdevideids.xml
 * <p>
 * DeviceId must not contain a '-'
 */
public final class EventsFileDescriptor {
    private static final String DATEFORMAT = "yyyy-MM-dd'T'HH.mm.ss";
    private static final int DATEFORMAT_LENGTH = DATEFORMAT.length() - 2;

    public static final String EVENTS = "events";

    private static final String DASH = "-";
    private static final String XML_EXT = ".xml";

    private final Date time;
    private final List<String> deviceIds;

    public EventsFileDescriptor(Date time, List<String> deviceIds) {
        super();
        this.time = time;
        this.deviceIds = deviceIds;
    }

    public static EventsFileDescriptor fromFileName(String filename) {
        if (!filename.startsWith(EVENTS)) {
            throw new RuntimeException(String.format(
                    "Not a valid events filename, must start with '%s', was: %s", EVENTS, filename));
        }
        
        String noExt = filename.substring(0, filename.lastIndexOf(XML_EXT));
        
        String stripEvents = noExt.substring((EVENTS+DASH).length());
        
        String dateStr = stripEvents.substring(0, DATEFORMAT_LENGTH);
        DateFormat fmt = new SimpleDateFormat(DATEFORMAT);
        Date d;
        try {
            d = fmt.parse(dateStr);
        } catch (ParseException e) {
            throw new RuntimeException(String.format("Not a valid date in filename: %s", filename));
        }
        
        
        String deviceIdsStr = stripEvents.substring(dateStr.length() + 1);
        Scanner s = new Scanner(deviceIdsStr);
        s.useDelimiter(DASH);
        List<String> devIds = new ArrayList<String>();
        while (s.hasNext()) {
            devIds.add(s.next());
        }

        return new EventsFileDescriptor(d, devIds);
    }

    public EventsFileDescriptor addDeviceId(String id) {
        List<String> newIds = new ArrayList<String>(deviceIds);
        newIds.add(id);
        return new EventsFileDescriptor(time, newIds);
    }

    public String toFilename() {
        DateFormat fmt = new SimpleDateFormat(DATEFORMAT);
        String timestamp = fmt.format(time);
        String filename = EVENTS + DASH + timestamp;
        for (String devId : deviceIds) {
            filename = filename + DASH + devId;
        }
        filename = filename + XML_EXT;
        return filename;
    }

    public Date getTime() {
        return time;
    }

    public List<String> getDeviceIds() {
        return deviceIds;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((deviceIds == null) ? 0 : deviceIds.hashCode());
        result = prime * result + ((time == null) ? 0 : time.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        EventsFileDescriptor other = (EventsFileDescriptor) obj;
        if (deviceIds == null) {
            if (other.deviceIds != null)
                return false;
        } else if (!deviceIds.equals(other.deviceIds))
            return false;
        if (time == null) {
            if (other.time != null)
                return false;
        } else if (!time.equals(other.time))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "EventsFileDescriptor [time=" + time + ", deviceIds=" + deviceIds + "]";
    }

}
