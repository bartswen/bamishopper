package bs.bamishopper.sync;

import android.content.Context;

import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.session.AppKeyPair;
import com.dropbox.client2.session.Session.AccessType;

public class DropboxAuthenticationService {

    private final AppKeyPair appKeys;
    private final static AccessType DROPBOX_ACCESS_TYPE = AccessType.DROPBOX;

    public DropboxAuthenticationService(String appKey, String appSecret) {
        appKeys = new AppKeyPair(appKey, appSecret);
    }

    /**
     * Starts the authentication process and then resumes the passed context.
     * Will invoke the passed Context when done. In the onResume() fetch the credentials from the callback.
     */
    public ShareAuthenticationCallback startAuthentication(Context ctx) {
        AndroidAuthSession session = new AndroidAuthSession(appKeys, DROPBOX_ACCESS_TYPE);
        DropboxAPI<AndroidAuthSession> mDBApi = new DropboxAPI<AndroidAuthSession>(session);
        mDBApi.getSession().startAuthentication(ctx);

        return new DropboxAuthenticationCallback(mDBApi);
    }
}
