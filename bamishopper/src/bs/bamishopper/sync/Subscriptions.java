package bs.bamishopper.sync;

import java.util.HashSet;
import java.util.Set;

public class Subscriptions {

    private static final String SEPARATOR = ",";

    public Set<String> parse(String subscribtionsStr) {
        Set<String> subscribtions = new HashSet<String>();
        for (String token : subscribtionsStr.split(SEPARATOR)) {
            subscribtions.add(token.trim());
        }
        return subscribtions;
    }

    public String encode(Set<String> subscriptionsList) {
        StringBuilder sb = new StringBuilder();
        for (String s : subscriptionsList) {
            if (sb.length() != 0) {
                sb.append(SEPARATOR);
            }
            sb.append(s);
        }
        return sb.toString();
    }

}
