package bs.bamishopper.sync;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Set;

import android.content.Context;
import bs.bamishopper.ActivityUtils;
import bs.bamishopper.R;
import bs.boli.domain.BoLi;
import bs.boli.domain.BoLiEvent;
import bs.boli.domain.Clock;
import bs.boli.domain.CollectionUtils;
import bs.boli.domain.CollectionUtils.Closure;
import bs.boli.domain.Logger;
import bs.boli.domain.StringUtils;
import bs.boli.domain.SystemClock;
import bs.boli.persist.BoLiXmlParser;

/**
 * Implements synchronize logic and manage subscriptions to share service.
 * <p>
 * Uses locks in the share service to manage concurrency between app instances. Uses locking on this service
 * to manage concurrency between background tasks in one app instance.
 * <p>
 * Use it as a singleton.
 */
public class SyncService {
    // tune this
    private static final int MAX_LOCK_TIME_SEC = 60;

    private static final String SLASH = "/";
    private static final String XML_EXT = ".xml";
    private static final String TXT_EXT = ".txt";

    private static final String BAMISHOPPER_DIR = ".BaMiShopper";
//    private static final String BAMISHOPPER_DIR = ".BaMiShopper/test";

    private static final String LOCK_FILE = BAMISHOPPER_DIR + SLASH + "lock";
    private static final String BAMISHOPPER_FILE = BAMISHOPPER_DIR + SLASH + "bamishopper" + XML_EXT;
    private static final String SUBSCRIPTIONS_FILE = BAMISHOPPER_DIR + SLASH + "subscriptions" + TXT_EXT;

    // collaborators
    private final ShareService shareService;
    private final Clock clock;
    private final Logger log;

    // state
    private boolean lock;

    /**
     * Constructs with SynchronizeAsyncTaskFactory.
     */
    public SyncService(ShareService shareService, Clock clock, Logger logger) {
        super();
        this.shareService = shareService;
        this.clock = clock;
        this.log = logger;
    }

    /** Returns if locked, use sync instead */
    @Deprecated
    public boolean share(final BoLi boli) throws ShareException {
        Closure<Void> doInDoubleLockAndLogExecTime = new Closure<Void>() {
            @Override
            public void execute(Void voit) {
                shareBoLiLogic(boli);
            }

            @Override
            public String toString() {
                return "share()";
            }
        };

        return executeInDoubleLockReturnIfLocked(doInDoubleLockAndLogExecTime);
    }

    private void shareBoLiLogic(BoLi boli) {
        String boliXml = BoLiXmlParser.toSharedXml(boli);
        shareService.share(BAMISHOPPER_FILE, boliXml);
    }

    private boolean hasMoreSubscribers() {
        Set<String> subsSet = readSubscribers();
        return subsSet.size() > 1;
    }

    private Set<String> readSubscribers() {
        String subsStr = shareService.pullOrCreate(SUBSCRIPTIONS_FILE);
        Subscriptions subs = new Subscriptions();
        Set<String> subsSet = subs.parse(subsStr);
        return subsSet;
    }

    /**
     * Returns false if sync was aborted because a lock existed
     * 
     * @param ctx only for logging
     */
    public boolean sync(final Context ctx, final BoLi boli, final String deviceId) {
        Date start = clock.now();

        if (!existEventsToConsume(deviceId) && !hasEventsToShare(boli)) {
            recordSyncAction(ctx, start);
            return true;
        }

        Closure<Void> doInDoubleLockAndLogExecTime = new Closure<Void>() {
            @Override
            public void execute(Void voit) {

                consumeEvents(boli, deviceId);
                if (hasEventsToShare(boli)) {
                    shareEvents(boli, deviceId);
                    shareBoLiLogic(boli);
                }

            }

            @Override
            public String toString() {
                return "sync()";
            }
        };

        recordSyncAction(ctx, start);

        return executeInDoubleLockReturnIfLocked(doInDoubleLockAndLogExecTime);
    }

    /** Store sync moment and longest sync duration */
    private void recordSyncAction(final Context ctx, Date start) {
        Date now = clock.now();

        ActivityUtils.storeLongPref(ctx, R.string.sync_last_action, now.getTime());

        int longest = ActivityUtils.getIntPref(ctx, R.string.sync_longes_duration);
        int durationSec = (int) DateDiff.roundDiffToSeconds(start, now);
        if (durationSec > longest) {
            ActivityUtils.storeIntPref(ctx, R.string.sync_longes_duration, durationSec);
        }
    }

    private boolean existEventsToConsume(final String deviceId) {
        Collection<String> eventFiles = listEventsFilesToProcess(deviceId);
        CollectionUtils.Predicate<String> deviceIdMismatch = new CollectionUtils.Predicate<String>() {
            @Override
            public boolean evaluate(String s) {
                return !s.contains(deviceId);
            }
        };
        return CollectionUtils.exists(eventFiles, deviceIdMismatch);
    }

    /** Returns true if lock obtained; if false do not access the shareservice */
    private boolean lockRemote() {
        log.trace(this.getClass().getName(), "lockRemote()...");
        long lockAgeSec = shareService.giveAge(LOCK_FILE, new SystemClock());
        log.trace(this.getClass().getName(), String.format("lockRemote(), age = %d", lockAgeSec));

        if (lockAgeSec > MAX_LOCK_TIME_SEC) {
            stealOrNewRemoteLock();
            return true;
        } else if (lockAgeSec < 0) {
            stealOrNewRemoteLock();
            return true;
        } else {
            return false;
        }
    }

    private void stealOrNewRemoteLock() {
        log.info(this.getClass().getName(), "steal or new remote lock");
        shareService.share(LOCK_FILE, StringUtils.EMPTY);
    }

    private void unlockRemote() {
        try {
            shareService.delete(LOCK_FILE);
        } catch (ShareException e) {

            // Ignore, a 404 can happen if a lock was stolen while another process was still using it

            log.info(this.getClass().getName(),
                    String.format("Error during remote lock remove: %s", e.toString()));
        }
    }

    private boolean hasEventsToShare(BoLi boli) {
        return !boli.getEventsToPublish().isEmpty();
    }

    private void consumeEvents(BoLi boli, String deviceId) {
        Collection<String> eventFiles = listEventsFilesToProcess(deviceId);
        consume(boli, eventFiles);
        markAsConsumedOrRemove(eventFiles, deviceId);
    }

    private Collection<String> listEventsFilesToProcess(final String deviceId) {
        Collection<String> filenames = shareService.listFilenames(BAMISHOPPER_DIR);

        CollectionUtils.filter(filenames, new CollectionUtils.Predicate<String>() {
            @Override
            public boolean evaluate(String filename) {
                return filename.contains(EventsFileDescriptor.EVENTS) && !filename.contains(deviceId);
            }
        });

        return filenames;
    }

    private void consume(BoLi boli, Collection<String> eventsFilenames) {
        List<String> eventsFilenamesList = new ArrayList<String>(eventsFilenames);

        Collections.sort(eventsFilenamesList, new Comparator<String>() {
            @Override
            public int compare(String lhs, String rhs) {
                return lhs.compareTo(rhs);
            }
        });

        List<BoLiEvent> allEvents = new ArrayList();
        for (String filename : eventsFilenamesList) {
            String eventsXml = shareService.pull(filename);

            List<BoLiEvent> events = BoLiXmlParser.fromEventsXml(eventsXml);
            allEvents.addAll(events);
        }
        boli.processEvents(allEvents);
    }

    private void markAsConsumedOrRemove(Collection<String> eventFiles, String deviceId) {
        Set<String> subsSet = readSubscribers();

        for (String eventFile : eventFiles) {
            String filename = stripToFilename(eventFile);

            EventsFileDescriptor dCurrent = EventsFileDescriptor.fromFileName(filename);
            EventsFileDescriptor dConsumed = dCurrent.addDeviceId(deviceId);

            if (dConsumed.getDeviceIds().containsAll(subsSet)) {

                // all other devices have processed the events
                shareService.delete(eventFile);
            } else {

                markAsConsumed(dCurrent, deviceId);
            }

        }
    }

    /** Adds the deviceId to the fileName */
    private void markAsConsumed(EventsFileDescriptor d, String deviceId) {
        EventsFileDescriptor newDescr = d.addDeviceId(deviceId);
        String fullname = toFullName(newDescr);
        shareService.rename(toFullName(d), fullname);
    }

    private static String toFullName(EventsFileDescriptor d) {
        String fullname = BAMISHOPPER_DIR + SLASH + d.toFilename();
        return fullname;
    }

    private static String stripToFilename(String fullFilename) {
        String name = fullFilename.substring(fullFilename.lastIndexOf(SLASH) + 1);
        return name;
    }

    private void shareEvents(BoLi boli, String deviceId) throws ShareException {
        List<BoLiEvent> events = boli.getEventsToPublish();
        if (events.isEmpty()) {
            log.info(this.getClass().getName(), "shareEvents: no events to share");
            return;
        }

        if (!hasMoreSubscribers()) {
            log.info(this.getClass().getName(), "shareEvents: no subscribers to share with");
            boli.removeEventsToPublish(boli.getEventsToPublish());
            return;
        }

        String eventsXml = BoLiXmlParser.toEventsXml(events, boli);

        EventsFileDescriptor descr = new EventsFileDescriptor(clock.now(), Arrays.asList(deviceId));
        String filename = toFullName(descr);

        shareService.shareNewFile(filename, eventsXml);

        boli.removeEventsToPublish(events);

        log.info(this.getClass().getName(), String.format("shareEvents: shared %d events", events.size()));
    }

    /** Returns if locked */
    public boolean pull(final BoLi boli, Context ctx) throws ShareException {
        Closure<Void> doInDoubleLockAndLogExecTime = new Closure<Void>() {
            @Override
            public void execute(Void voit) {
                String boliXml = shareService.pull(BAMISHOPPER_FILE);
                BoLi shared = BoLiXmlParser.fromSharedXml(boliXml);
                boli.merge(shared);
            }

            @Override
            public String toString() {
                return "pull()";
            }
        };

        return executeInDoubleLockReturnIfLocked(doInDoubleLockAndLogExecTime);
    }

    /** Wait indefinitely for this- and remote-locks */
    public void subscribe(final String deviceId) throws ShareException {
        Closure<Void> subscribeClosure = new Closure<Void>() {
            @Override
            public void execute(Void voit) {

                Closure<Set<String>> doWithSubscriptions = new Closure<Set<String>>() {
                    @Override
                    public void execute(Set<String> subs) {
                        subs.add(deviceId);
                    }
                };

                updateSubscriptionsFile(doWithSubscriptions);

            }

            @Override
            public String toString() {
                return "subscribe()";
            }
        };

        executeInDoubleLockWaitForLocks(subscribeClosure);
    }

    /** Wait indefinitely for this- and remote-locks */
    public void unsubscribe(final String deviceId) throws ShareException {
        Closure<Void> unsubscribeClosure = new Closure<Void>() {
            @Override
            public void execute(Void voit) {

                Closure<Set<String>> doWithSubscriptions = new Closure<Set<String>>() {
                    @Override
                    public void execute(Set<String> subs) {
                        subs.remove(deviceId);
                    }
                };

                updateSubscriptionsFile(doWithSubscriptions);

                // TODO clean up events files

            }

            @Override
            public String toString() {
                return "unsubscribe()";
            }
        };

        executeInDoubleLockWaitForLocks(unsubscribeClosure);
    }

    /** Reads and parses the subscriptions file, executes the closure, and then encodes en writes it back. */
    private void updateSubscriptionsFile(Closure<Set<String>> doWithSubscriptions) throws ShareException {
        Set<String> subsSet = readSubscribers();

        doWithSubscriptions.execute(subsSet);

        String updatedSubsStr = new Subscriptions().encode(subsSet);
        shareService.share(SUBSCRIPTIONS_FILE, updatedSubsStr);
    }

    /** Returns true if the closure has executed, false if a lock existed */
    private boolean executeInDoubleLockReturnIfLocked(Closure<Void> closure) {
        log.info(this.getClass().getName(), String.format("%s...", closure.toString()));
        long start = System.currentTimeMillis();

        boolean localLock = lockThis();
        if (!localLock) {
            log.info(this.getClass().getName(),
                    String.format("The sync service is locked, abort %s", closure.toString()));
            return false;
        }

        try {
            boolean remoteLock = lockRemote();
            if (!remoteLock) {
                log.info(this.getClass().getName(),
                        String.format("Could not acquire lock, abort %s", closure.toString()));
                return false;
            }

            closure.execute(null);

            unlockRemote();
            unlockThis();
        } catch (ShareException e) {
            unlockThis();
            throw (new SyncException(e));
        }

        long execTimeSec = (System.currentTimeMillis() - start) / 1000;
        log.info(this.getClass().getName(),
                String.format("Execution of %s took %d sec", closure.toString(), execTimeSec));
        return true;
    }

    private void executeInDoubleLockWaitForLocks(Closure<Void> closure) {
        log.info(this.getClass().getName(), String.format("%s...", closure.toString()));
        long start = System.currentTimeMillis();

        try {
            Wait<Void> waitForLockThis = new Wait<Void>(1, log);
            waitForLockThis.until(new CollectionUtils.Function<Void, Boolean>() {

                @Override
                public Boolean apply(Void na) {

                    boolean localLock = lockThis();
                    return Boolean.valueOf(localLock);

                }
            });
        } catch (InterruptedException e) {
            throw new SyncException();
        }

        try {
            Wait<Void> waitForRemoteLock = new Wait<Void>(3, log);
            waitForRemoteLock.until(new CollectionUtils.Function<Void, Boolean>() {

                @Override
                public Boolean apply(Void na) {

                    boolean remoteLock = lockRemote();
                    return Boolean.valueOf(remoteLock);

                }
            });

            closure.execute(null);
            unlockThis();

            unlockRemote();
        } catch (InterruptedException e) {
            throw new SyncException();
        } catch (ShareException e) {
            unlockThis();
            throw (new SyncException(e));
        }

        long execTimeSec = (System.currentTimeMillis() - start) / 1000;
        log.info(this.getClass().getName(),
                String.format("Execution of %s took %d sec", closure.toString(), execTimeSec));
    }

    private synchronized boolean lockThis() {
        if (lock) {
            return false;
        } else {
            lock = true;
            return true;
        }
    }

    private synchronized void unlockThis() {
        this.lock = false;
    }
}
