package bs.bamishopper.sync;

import android.util.Log;
import android.util.Pair;

import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.session.AccessTokenPair;

public class DropboxAuthenticationCallback implements ShareAuthenticationCallback {

    private final DropboxAPI<AndroidAuthSession> mDBApi;

    public DropboxAuthenticationCallback(DropboxAPI<AndroidAuthSession> mDBApi) {
        this.mDBApi = mDBApi;
    }

    @Override
    public Pair<String, String> finishAuthentication() throws ShareException {
        final Pair<String, String> result;

        if (mDBApi.getSession().authenticationSuccessful()) {
            try {
                // Sets the access token on the session
                mDBApi.getSession().finishAuthentication();

                AccessTokenPair tokens = mDBApi.getSession().getAccessTokenPair();
                result = new Pair<String, String>(tokens.key, tokens.secret);
            } catch (IllegalStateException e) {
                Log.i("DbAuthLog", "Error authenticating", e);
                throw new ShareException("Unknown dropbox error");
            }
        } else {
            Log.i("DbAuthLog", "Authenticating not successful");
            throw new ShareException("Dropbox login failed");
        }

        return result;
    }

}
