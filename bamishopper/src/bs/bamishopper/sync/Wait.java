package bs.bamishopper.sync;

//import android.util.Log;
import bs.boli.domain.CollectionUtils.Function;
import bs.boli.domain.Logger;

/**
 * Blocks until a function evaluates to true or to any non-null value. Improvement: add timeout
 * <p>
 * Borrowed from tools repository.
 * 
 * @param <T> Type of the input for the function to evaluate.
 */
public class Wait<T> {
    private final int intervalsec;
    private final T input;
    private final Logger log;

    public Wait(int intervalSec, Logger logger) {
        this.intervalsec = intervalSec;
        this.input = null;
        this.log = logger;
    }

    public Wait(int interval, T input, Logger logger) {
        this.intervalsec = interval;
        this.input = input;
        this.log = logger;
    }

    public <U> U until(Function<T, U> function) throws InterruptedException {
        while (true) {
            U value = function.apply(input);
            if (value != null && Boolean.class.equals(value.getClass())) {
                if (Boolean.TRUE.equals(value)) {
                    return value;
                }
            } else if (value != null) {
                return value;
            }

            try {
                log.debug(getClass().getName(), String.format("until(): sleep %d sec", intervalsec));
                Thread.sleep(intervalsec * 1000);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                throw e;
            }
        }
    }

}
