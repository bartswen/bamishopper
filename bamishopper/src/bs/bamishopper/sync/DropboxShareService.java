package bs.bamishopper.sync;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringBufferInputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import android.util.Log;
import bs.abstractfilesystem.CollectionUtils;
import bs.boli.domain.Clock;
import bs.boli.domain.StringUtils;
import bs.boli.domain.Utils;

import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.DropboxAPI.DropboxInputStream;
import com.dropbox.client2.DropboxAPI.Entry;
import com.dropbox.client2.ProgressListener;
import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.exception.DropboxException;
import com.dropbox.client2.exception.DropboxServerException;
import com.dropbox.client2.session.AccessTokenPair;
import com.dropbox.client2.session.AppKeyPair;
import com.dropbox.client2.session.Session.AccessType;

/**
 * TODO generify met SynchronizeAuthenticationCallback type
 * 
 */
public class DropboxShareService implements ShareService {

    // private final AccessTokenPair userAccessPair;
    // private final AppKeyPair appKeys;
    // private static final String DROPBOX_BAMISHOPPER_FILE = ".BaMiShopper/bamishopper.xml";
    private final static AccessType DROPBOX_ACCESS_TYPE = AccessType.DROPBOX;
    private final static String DATE_FORMAT = "EEE, dd MMM yyyy kk:mm:ss ZZZZZ";

    private final DropboxAPI<AndroidAuthSession> dbApi;

    public DropboxShareService(String appKey, String appSecret, String userKey, String userSecret) {
        AppKeyPair appKeys = new AppKeyPair(appKey, appSecret);
        AccessTokenPair userAccessPair = new AccessTokenPair(userKey, userSecret);
        dbApi = startDropboxSession(appKeys, userAccessPair);
    }

    private static DropboxAPI<AndroidAuthSession> startDropboxSession(AppKeyPair appKeys,
            AccessTokenPair userAccessPair) {

        AndroidAuthSession session = new AndroidAuthSession(appKeys, DROPBOX_ACCESS_TYPE, userAccessPair);
        final DropboxAPI<AndroidAuthSession> dbApi = new DropboxAPI<AndroidAuthSession>(session);
        return dbApi;
    }

    public void share(String filename, String content) throws ShareException {
        InputStream is = new StringBufferInputStream(content);
        int len = content.getBytes().length;
        ProgressListener progressListenerNull = null;
        try {

            String parentRev = parentrevForExistingOrNew(filename);
            Log.v(this.getClass().getName(), String.format("share(): '%s', parentRev: %s", filename, parentRev));

            dbApi.putFile(filename, is, len, parentRev, progressListenerNull);

        } catch (DropboxException e) {
            Log.e(this.getClass().getName(), e.toString());
            throw new ShareException(String.format("Push %s to Dropbox failed.\n\n" + e.toString(), filename));
        }
    }

    /** Returns null if file does not exist */
    private String parentrevForExistingOrNew(String filename) throws DropboxException {
        String parentRev = null;
        try {

            String nullRevForLatest = null;
            Entry metadata = dbApi.metadata("/" + filename, 1, null, false, nullRevForLatest);
            parentRev = metadata.rev;
            Log.v(this.getClass().getName(), String.format(
                    "parentrevForExistingOrNew(): %s, parentRev: %s, deleted: %b", filename, parentRev,
                    metadata.isDeleted));
        } catch (DropboxServerException e) {
            if (e.error == DropboxServerException._404_NOT_FOUND) {
                parentRev = null;
            } else {
                Log.e(this.getClass().getName(), e.toString());
                throw new ShareException(String.format(
                        "Fetch metadata for %s from Dropbox failed.\n\n" + e.toString(), filename));
            }
        } catch (DropboxException e) {
            Log.e(this.getClass().getName(), e.toString());
            throw new ShareException(String.format(
                    "Fetch metadata for %s from Dropbox failed.\n\n" + e.toString(), filename));
        }
        return parentRev;
    }

    private void createDir(String dir) {
        try {

            dbApi.createFolder(dir);

        } catch (DropboxException e) {
            Log.e(this.getClass().getName(), e.toString());
            throw new ShareException(
                    String.format("Create dir %s in Dropbox failed.\n\n" + e.toString(), dir));
        }
    }

    public void shareNewFile(String filename, String content) throws ShareException {
        InputStream is = new StringBufferInputStream(content);
        int len = content.getBytes().length;
        String nullRevForNew = null;
        ProgressListener progressListenerNull = null;
        try {

            dbApi.putFile(filename, is, len, nullRevForNew, progressListenerNull);

        } catch (DropboxException e) {
            Log.e(this.getClass().getName(), e.toString());
            throw new ShareException(String.format("Push %s to Dropbox failed.\n\n" + e.toString(), filename));
        }
    }

    public String pull(String filename) throws ShareException {
        try {

            return pullLogic(filename, dbApi);
        } catch (DropboxException e) {
            Log.e(this.getClass().getName(), e.toString());
            throw new ShareException(String.format("Pull %s from Dropbox failed.\n\n" + e.toString(),
                    filename));
        } catch (IOException e) {
            Log.e(this.getClass().getName(), e.toString());
            throw new ShareException(String.format("Pull %s from Dropbox failed.\n\n" + e.toString(),
                    filename));
        }
    }

    private String pullLogic(String filename, final DropboxAPI<AndroidAuthSession> dbApi)
            throws DropboxException, IOException {
        String nullParentRef = null;
        DropboxInputStream fileStream = dbApi.getFileStream(filename, nullParentRef);
        String content = Utils.toString(fileStream);
        return content;
    }

    public String pullOrCreate(String filename) throws ShareException {
        try {
            return pullLogic(filename, dbApi);
        } catch (DropboxServerException e) {
            if (e.error == DropboxServerException._404_NOT_FOUND) {
                String content = StringUtils.EMPTY;
                shareNewFile(filename, content);
                return content;
            } else {
                Log.e(this.getClass().getName(), e.toString());
                throw new ShareException(String.format("Pull %s from Dropbox failed.\n\n" + e.toString(),
                        filename));
            }
        } catch (DropboxException e) {
            Log.e(this.getClass().getName(), e.toString());
            throw new ShareException(String.format("Pull %s from Dropbox failed.\n\n" + e.toString(),
                    filename));
        } catch (IOException e) {
            Log.e(this.getClass().getName(), e.toString());
            throw new ShareException(String.format("Pull %s from Dropbox failed.\n\n" + e.toString(),
                    filename));
        }
    }

    @Override
    public Collection<String> listFilenames(String dir) {
        Entry metadata;
        try {
            metadata = dbApi.metadata("/" + dir, 0, null, true, null);
        } catch (DropboxServerException e) {
            if (e.error == DropboxServerException._404_NOT_FOUND) {
                createDir(dir);
                return Arrays.asList();
            } else {
                Log.e(this.getClass().getName(), e.toString());
                throw new ShareException(String.format(
                        "ListFilenames %s from Dropbox failed.\n\n" + e.toString(), dir));
            }
        } catch (DropboxException e) {
            Log.e(this.getClass().getName(), e.toString());
            throw new ShareException(String.format(
                    "ListFilenames %s from Dropbox failed.\n\n" + e.toString(), dir));
        }

        List<String> filenames = CollectionUtils.collect(metadata.contents,
                new CollectionUtils.Transformer<DropboxAPI.Entry, String>() {
                    @Override
                    public String transform(Entry t) {
                        return t.path;
                    }
                });

        return filenames;
    }

    public void delete(String filename) throws ShareException {
        try {
            dbApi.delete(filename);
        } catch (DropboxException e) {
            Log.e(this.getClass().getName(), e.toString());
            throw new ShareException(String.format("Delete %s from Dropbox failed.\n\n" + e.toString(),
                    filename));
        }
    }

    public void rename(String from, String to) throws ShareException {
        try {
            dbApi.copy(from, to);
            dbApi.delete(from);
        } catch (DropboxException e) {
            Log.e(this.getClass().getName(), e.toString());
            throw new ShareException(String.format("Rename %s to %s in Dropbox failed.\n\n" + e.toString(),
                    from, to));
        }
    }

    /**
     * Geeft -1 als file niet bestaat.
     */
    @Override
    public long giveAge(String file, Clock clock) {
        long age = -1;
        Entry metadata;
        try {
            metadata = dbApi.metadata("/" + file, 0, null, true, null);
            if (metadata.isDeleted) {
                Log.v(this.getClass().getName(), "giveAge(): lock metadata: deleted");
                age = -1;
            } else {
                Log.v(this.getClass().getName(), "giveAge(): calc age");
                Date d = parseDate(metadata.modified);
                age = diffInSec(clock.now(), d);
            }
        } catch (DropboxServerException e) {
            if (e.error == DropboxServerException._404_NOT_FOUND) {
                Log.v(this.getClass().getName(), "giveAge(): lock not found (404)");
                age = -1;
            } else {
                Log.e(this.getClass().getName(), e.toString());
                throw new ShareException(String.format("GiveAge %s from Dropbox failed.\n\n" + e.toString(),
                        file));
            }
        } catch (DropboxException e) {
            Log.e(this.getClass().getName(), e.toString());
            throw new ShareException(
                    String.format("GiveAge %s from Dropbox failed.\n\n" + e.toString(), file));
        }

        return age;
    }

    private long diffInSec(Date latest, Date earlier) {
        return (long) ((latest.getTime() - earlier.getTime()) / 1000);
    }

    /**
     * @param d 'EEE, dd MMM yyyy kk:mm:ss ZZZZZ'
     */
    private Date parseDate(String d) {
        DateFormat fmt = new SimpleDateFormat(DATE_FORMAT);
        Date date;
        try {
            date = fmt.parse(d);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }

        return date;
    }
}
