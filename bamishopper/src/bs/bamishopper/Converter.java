package bs.bamishopper;

import java.util.List;

import bs.boli.domain.Bo;
import bs.boli.domain.CollectionUtils;
import bs.boli.domain.Wnk;

public class Converter {
    public static String[] asWnkNames(List<Wnk> wnks) {
        List<String> namesList = CollectionUtils.collect(wnks,
                new CollectionUtils.Transformer<Wnk, String>() {
                    public String transform(Wnk wnk) {
                        return wnk.getNaam();
                    }
                });
        return namesList.toArray(new String[] {});
    }

    public static String[] asBoNames(List<Bo> bs) {
        List<String> namesList = CollectionUtils.collect(bs,
                new CollectionUtils.Transformer<Bo, String>() {
                    public String transform(Bo bo) {
                        return bo.getNaam();
                    }
                });
        return namesList.toArray(new String[] {});
    }

}
