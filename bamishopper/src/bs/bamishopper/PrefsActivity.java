package bs.bamishopper;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.util.Log;
import android.util.Pair;
import android.widget.Toast;
import bs.bamishopper.sync.SyncService;
import bs.bamishopper.sync.DropboxAuthenticationService;
import bs.bamishopper.sync.ShareAuthenticationCallback;
import bs.bamishopper.sync.ShareException;
import bs.bamishopper.sync.SyncException;
import bs.boli.domain.CollectionUtils.Closure;

public class PrefsActivity extends PreferenceActivity {
    private ShareAuthenticationCallback syncAuthCallback;

    @Override
    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
        if (preference.getKey().equals(getString(R.string.sync_pref_key))) {

            // sync clicked..
            boolean sync = isSyncPrefChecked();
            if (sync) {

                // .. and checked
                DropboxAuthenticationService dbAuthSvc = PrefsActivity.this
                        .createDropboxAuthenticationService();
                this.syncAuthCallback = dbAuthSvc.startAuthentication(this);

            } else {

                // .. and unchecked
                unsubscribe();

            }
        }

        return false;
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_OK);
        finish();

        super.onStop();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.settings);

    }

    protected void onResume() {
        super.onResume();

        if (comeBackFromSyncServiceAuthentication()) {

            // we come back from dropbox login, finish authentication here
            if (syncAuthCallback == null) {
                Log.e(this.getClass().getName(), "Assertion failed, syncAuthCallback null in onResume ");
                return;
            }

            try {
                Pair<String, String> tokenPair = syncAuthCallback.finishAuthentication();
                storeDropboxKeys(tokenPair.first, tokenPair.second);
                BamishopperFactory.reinit(PrefsActivity.this);

            } catch (ShareException e) {
                uncheckDropboxSync();
                Toast.makeText(this, "Dropbox authentication finish failed: " + e.getMessage(),
                        Toast.LENGTH_LONG).show();
                return;
            }

            try {
                subscribe();
            } catch (ShareException e) {
                uncheckDropboxSync();
                Toast.makeText(this, "Dropbox device subscription failed: " + e.getMessage(),
                        Toast.LENGTH_LONG).show();
                return;
            }
        }
    }

    /**
     * Parameter wrapper for the invocation of the AsyncTask.
     */
    private static final class SyncServiceSubscriptionClosures {
        private final Closure<SyncService> syncInvokation;
        private final Closure<Exception> onError;
        private final Closure<Void> onSuccess;

        /**
         * @param onError will be invoked if the share operation fails, can be null
         * @param onSuccess will be invoked if the share operation succeeds, can be null
         */
        public SyncServiceSubscriptionClosures(Closure<SyncService> syncInvokation,
                Closure<Exception> onError, Closure<Void> onSuccess) {
            super();
            this.syncInvokation = syncInvokation;
            this.onError = onError;
            this.onSuccess = onSuccess;
        }
    }

    /**
     * For async invocation of the subscribe and unsubscribe operations.
     */
    private static class InvokeSyncSubscriptionTask extends
            AsyncTask<SyncServiceSubscriptionClosures, Void, SyncException> {
        protected SyncServiceSubscriptionClosures params = null;

        @Override
        protected SyncException doInBackground(SyncServiceSubscriptionClosures... syncParams) {
            params = syncParams[0];
            try {
                params.syncInvokation.execute(null);
            } catch (ShareException e) {
                return new SyncException(e);
            } catch (SyncException e) {
                return e;
            }
            return null;
        }

        @Override
        protected void onPostExecute(SyncException errorResult) {
            super.onPostExecute(errorResult);
            invokeCallbacks(params, errorResult);
        }
    }

    /**
     * @param errorResult can be null
     */
    private static void invokeCallbacks(SyncServiceSubscriptionClosures callbacks, SyncException errorResult) {
        if (errorResult == null) {
            if (callbacks.onSuccess != null) {
                callbacks.onSuccess.execute(null);
            }
        } else {
            if (callbacks.onError != null) {
                callbacks.onError.execute(errorResult);
            }
        }
    }

    private void subscribe() throws ShareException {

        final String id = ActivityUtils.fetchDeviceId(this);
        Closure<SyncService> syncSubscribeInvocation = new Closure<SyncService>() {
            @Override
            public void execute(SyncService t) {
                BamishopperFactory.getInstance().getSyncService().subscribe(id);
            }
        };

        Closure<Exception> onError = new Closure<Exception>() {
            @Override
            public void execute(Exception t) {
                uncheckDropboxSync();
                Toast.makeText(PrefsActivity.this, "Dropbox device subscription failed: " + t.getMessage(),
                        Toast.LENGTH_LONG).show();
            }

        };

        Closure<Void> onSuccess = new Closure<Void>() {
            @Override
            public void execute(Void t) {
                Toast.makeText(
                        PrefsActivity.this,
                        "Dropbox sync now enabled, device subscription successful. "
                                + "Make sure to share your .BaMiShopper folder and PULL shoppinglist once.",
                        Toast.LENGTH_LONG).show();
            }
        };

        InvokeSyncSubscriptionTask syncServiceSubscribeTask = new InvokeSyncSubscriptionTask();

        SyncServiceSubscriptionClosures params = new SyncServiceSubscriptionClosures(syncSubscribeInvocation,
                onError, onSuccess);

        syncServiceSubscribeTask.execute(params);
    }

    private void unsubscribe() {
        final String id = ActivityUtils.fetchDeviceId(this);
        Closure<SyncService> syncUnsubscribeInvokation = new Closure<SyncService>() {
            @Override
            public void execute(SyncService t) {
                BamishopperFactory.getInstance().getSyncService().unsubscribe(id);
            }
        };

        Closure<Exception> onError = new Closure<Exception>() {
            @Override
            public void execute(Exception t) {
                checkDropboxSync();
                Toast.makeText(PrefsActivity.this, "Dropbox device unsubscribe failed: " + t.getMessage(),
                        Toast.LENGTH_LONG).show();
            }

        };

        Closure<Void> onSuccess = new Closure<Void>() {
            @Override
            public void execute(Void t) {
                removeDropboxKeys();
                BamishopperFactory.reinit(PrefsActivity.this);
                Toast.makeText(PrefsActivity.this,
                        "Dropbox sync is now disabled. You may unshare the .BaMiShopper dropbox-folder.",
                        Toast.LENGTH_LONG).show();
            }

        };

        InvokeSyncSubscriptionTask syncServiceUnsubscribeTask = new InvokeSyncSubscriptionTask();

        SyncServiceSubscriptionClosures params = new SyncServiceSubscriptionClosures(
                syncUnsubscribeInvokation, onError, onSuccess);
        syncServiceUnsubscribeTask.execute(params);
    }

    private DropboxAuthenticationService createDropboxAuthenticationService() {
        DropboxAuthenticationService svc = new DropboxAuthenticationService(ActivityUtils.DROPBOX_APP_KEY,
                ActivityUtils.DROPBOX_APP_SECRET);
        return svc;
    }

    private boolean comeBackFromSyncServiceAuthentication() {
        return isSyncPrefChecked() && !ActivityUtils.hasDropboxKeys(this);
    }

    private void checkDropboxSync() {
        CheckBoxPreference syncpref = findSyncPreference();
        syncpref.setChecked(true);
    }

    private void uncheckDropboxSync() {
        CheckBoxPreference syncpref = findSyncPreference();
        syncpref.setChecked(false);
    }

    private boolean isSyncPrefChecked() {
        CheckBoxPreference syncpref = findSyncPreference();
        return syncpref.isChecked();
    }

    private CheckBoxPreference findSyncPreference() {
        CheckBoxPreference syncpref = (CheckBoxPreference) findPreference(getString(R.string.sync_pref_key));
        return syncpref;
    }

    /** Save the dropbox credentials in app preferences. **/
    private void storeDropboxKeys(String key, String secret) {
        ActivityUtils.storeStringPref(this, R.string.DROPBOX_KEY_KEY, key);
        ActivityUtils.storeStringPref(this, R.string.DROPBOX_SECRET_KEY, secret);
    }

    /** Remove the dropbox credentials in app preferences. **/
    private void removeDropboxKeys() {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = settings.edit();
        editor.remove(getString(R.string.DROPBOX_KEY_KEY));
        editor.remove(getString(R.string.DROPBOX_SECRET_KEY));

        editor.commit();
    }

}