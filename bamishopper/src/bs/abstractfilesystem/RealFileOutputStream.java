package bs.abstractfilesystem;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class RealFileOutputStream extends AbstractFileOutputStream<RealFile> {
    private final FileOutputStream fos;

    public RealFileOutputStream(RealFile file) throws FileNotFoundException {
        super();
        this.fos = new FileOutputStream(file.getFile());
    }

    @Override
    public void write(int b) throws IOException {
        fos.write(b);
    }
    
    @Override
    public void close() throws IOException {
        super.close();
        fos.close();
    }

}
