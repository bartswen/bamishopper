package bs.abstractfilesystem;

/** Abstraction for java.io.FilenameFilter */
public interface AbstractFilenameFilter {
    boolean accept(String filename);
}

