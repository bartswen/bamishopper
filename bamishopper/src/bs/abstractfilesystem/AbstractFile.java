package bs.abstractfilesystem;

/**
 * Abstraction for java.io.File (no directory)
 * 
 * @param <T> the type of the implementor itself
 */
public interface AbstractFile<T extends AbstractFile<T>> {
    long lastModified();

    String getName();

    boolean delete();

    boolean exists();

    boolean renameTo(T dest);

    void createNewFile();
}
