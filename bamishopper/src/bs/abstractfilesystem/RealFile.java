package bs.abstractfilesystem;

import java.io.File;
import java.io.IOException;

public class RealFile implements AbstractFile<RealFile> {
    private File file;

    public RealFile(String filename) {
        this(new File(filename));
    }

    public RealFile(File file) {
        super();
        this.file = file;
        if (file.isDirectory()) {
            throw new RuntimeException(String.format("Is a directory, not a file: %s", file));
        }
    }

    @Override
    public long lastModified() {
        return file.lastModified();
    }

    @Override
    public boolean delete() {
        return file.delete();
    }

    @Override
    public boolean exists() {
        return file.exists();
    }

    @Override
    public String getName() {
        return file.getName();
    }

    @Override
    public boolean renameTo(RealFile dest) {
        return file.renameTo(dest.file);
    }

    @Override
    public void createNewFile() {
        try {
            file.createNewFile();
        } catch (IOException e) {
            // TODO use checked
            throw new RuntimeException(e);
        }
    }

    public File getFile() {
        return file;
    }
}
