package bs.abstractfilesystem;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.Collection;

public class RealDirectory implements Directory<RealFile> {
    private File dir;

    public RealDirectory(String dirname) {
        super();
        dir = new File(dirname);
        if (!dir.isDirectory()) {
            throw new RuntimeException(String.format("Not a directory: %s", dirname));
        }
    }

    @Override
    public RealFile[] listFiles(final AbstractFilenameFilter filter) {
        File[] files = dir.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return filter.accept(name);
            }
        });

        Collection<RealFile> fileWrappers = CollectionUtils.collect(Arrays.asList(files), new CollectionUtils.Transformer<File, RealFile>() {
            @Override
            public RealFile transform(File input) {
                return new RealFile(input);
            }
        });

        return fileWrappers.toArray(new RealFile[] {});
    }
}
