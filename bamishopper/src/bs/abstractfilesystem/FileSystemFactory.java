package bs.abstractfilesystem;

public interface FileSystemFactory<T extends Directory<U>, U extends AbstractFile<U>, V extends AbstractFileOutputStream<U>> {
    T createDirectory(String dirname);
    U createFile(String filename);
    V createOutputStream(U file);
}
