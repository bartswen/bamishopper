package bs.abstractfilesystem;

import java.io.FileNotFoundException;

public class RealFileSystemFactory implements FileSystemFactory<RealDirectory, RealFile, RealFileOutputStream> {

    @Override
    public RealDirectory createDirectory(String dirname) {
        return new RealDirectory(dirname);
    }

    @Override
    public RealFile createFile(String filename) {
        return new RealFile(filename);
    }

    @Override
    public RealFileOutputStream createOutputStream(RealFile file) {
        // TODO define checked
        try {
            return new RealFileOutputStream(file);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
