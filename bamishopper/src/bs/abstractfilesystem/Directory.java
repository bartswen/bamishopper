package bs.abstractfilesystem;

/** Abstraction for java.io.File directory */
public interface Directory<T extends AbstractFile<T>> {
    T[] listFiles(AbstractFilenameFilter filter);
}
