package bs.abstractfilesystem;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public final class CollectionUtils {
	public static interface Predicate<T> {
		public boolean evaluate(T object);
	}

	/**
	 * @param <T> source type
	 * @param <U> target type
	 */
	public static interface Transformer<T, U> {
		public U transform(T t);
	}

	public static interface Closure<T> {
		public void execute(T t);
	}

	private CollectionUtils() {
	}

	public static <T> T find(Iterable<T> iterable, Predicate<T> predicate) {
		for (T t : iterable) {
			if (predicate.evaluate(t)) {
				return t;
			}
		}
		return null;
	}

	public static <T, U> List<U> collect(Iterable<T> iterable, Transformer<T, U> transformer) {
		List<U> ulist = new ArrayList<U>();
		for (T t : iterable) {
			ulist.add(transformer.transform(t));
		}
		return ulist;
	}

	/**
	 * Filter the collection by applying a Predicate to each element. If the predicate returns false, remove
	 * the element. If the input collection or predicate is null, there is no change made.
	 */
	public static <T> void filter(Iterable<T> iterable, Predicate<T> predicate) {
		if (iterable == null || predicate == null) {
			return;
		}
		for (Iterator<T> iter = iterable.iterator();iter.hasNext();) {
			T t = iter.next();
			if (!predicate.evaluate(t)) {
				iter.remove();
			}
		}
	}

	public static <T> void forAllDo(Iterable<T> iterable, Closure<T> closure) {
		for (T t : iterable) {
			closure.execute(t);
		}
	}

	public static <T> List<T> subtract(Collection<T> allItems, Collection<T> toSubtract) {
		List<T> subtract = new ArrayList<T>();
		for (T t : allItems) {
			if (!toSubtract.contains(t)) {
				subtract.add(t);
			}
		}
		return subtract;
	}
}
