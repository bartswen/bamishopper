package bs.boli.persist;

import java.io.StringWriter;
import java.util.Collection;
import java.util.List;

import bs.boli.domain.Bo;
import bs.boli.domain.BoLi;
import bs.boli.domain.BoLiEvent;
import bs.boli.domain.ItemAddedEvent;
import bs.boli.domain.ItemCategoryChangedEvent;
import bs.boli.domain.ItemCountChangedEvent;
import bs.boli.domain.ItemDeletedEvent;
import bs.boli.domain.ItemInShop;
import bs.boli.domain.ItemNameChangedEvent;
import bs.boli.domain.ShopAddedEvent;
import bs.boli.domain.ShopDeletedEvent;
import bs.boli.domain.ShopItemAddedEvent;
import bs.boli.domain.ShopItemMovedEvent;
import bs.boli.domain.ShopItemRemovedEvent;
import bs.boli.domain.ShopNameChangedEvent;
import bs.boli.domain.Stack;
import bs.boli.domain.Wnk;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.reflection.PureJavaReflectionProvider;
import com.thoughtworks.xstream.converters.reflection.ReflectionProvider;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.io.xml.PrettyPrintWriter;

/**
 * BoLi Local: all state in BoLi, includes eventsToPublish. BoLi Shared: excludes eventsToPublish, these are
 * pushed out separately
 */
public final class BoLiXmlParser {
    
    private BoLiXmlParser() {
        super();
    }

    public static String toLocalXml(BoLi boli) {
        XStream xstream = createShopListLocalXStream();

        StringWriter sw = new StringWriter();
        PrettyPrintWriter ppw = new BoLiWriter(sw, boli);

        xstream.marshal(boli, ppw);
        String sholiXml = sw.toString();
        return sholiXml;
    }

    public static BoLi fromLocalXml(String boliXml) {
        XStream xstream = createShopListLocalXStream();
        BoLi boli = (BoLi) xstream.fromXML(boliXml);
        ReflectionProvider refl = xstream.getReflectionProvider();

        writeTransientBolis(boli.getBsAlphabetic(), boli, refl);
        writeTransientBolis(boli.getWnks(), boli, refl);

        return boli;
    }

    public static String toSharedXml(BoLi boli) {
        XStream xstream = createShopListSharedXStream();

        StringWriter sw = new StringWriter();
        PrettyPrintWriter ppw = new BoLiWriter(sw, boli);

        xstream.marshal(boli, ppw);
        String sholiXml = sw.toString();
        return sholiXml;
    }

    public static BoLi fromSharedXml(String sharedXml) {
        XStream xstream = createShopListSharedXStream();
        BoLi boli = (BoLi) xstream.fromXML(sharedXml);
        ReflectionProvider refl = xstream.getReflectionProvider();

        writeTransientBolis(boli.getBsAlphabetic(), boli, refl);
        writeTransientBolis(boli.getWnks(), boli, refl);

        return boli;
    }

    public static String toEventsXml(List<BoLiEvent> events, BoLi boli) {
        XStream xstream = createShopListLocalXStream();

        StringWriter sw = new StringWriter();
        PrettyPrintWriter ppw = new BoLiWriter(sw, boli);

        xstream.marshal(events, ppw);
        String sholiXml = sw.toString();
        return sholiXml;
    }

    @SuppressWarnings("unchecked")
    public static List<BoLiEvent> fromEventsXml(String eventsXml) {
        XStream xstream = createShopListLocalXStream();
        return (List<BoLiEvent>) xstream.fromXML(eventsXml);
    }

    private static void writeTransientBolis(Collection<?> objectsWithBoliField, BoLi boli, ReflectionProvider refl) {
        for (Object o : objectsWithBoliField) {
            refl.writeField(o, "boli", boli, null);
        }
    }

    private static XStream createShopListSharedXStream() {
        XStream xstream = createShopListLocalXStream();
        xstream.omitField(BoLi.class, "eventsToPublish");
        xstream.omitField(BoLi.class, "undoStack");
        return xstream;
    }

    private static XStream createShopListLocalXStream() {
        // PureJavaReflectionProvider to force pure Java mode and not enhanced mode. Enhanced mode depends on
        // JVM and is unavailable on android.
        // VerifyError here with StaxDriver, so use DomDriver
        XStream xstream = new XStream(new PureJavaReflectionProvider(), new DomDriver());
        configureBoLi(xstream);
        configureBo(xstream);
        configureWnk(xstream);
        configureEvents(xstream);
        return xstream;
    }

    private static void configureBoLi(XStream xstream) {
        xstream.alias("shoplist", BoLi.class);
        xstream.omitField(BoLi.class, "clock");
        xstream.registerLocalConverter(BoLi.class, "bs", new BoLiBsConverter());
        xstream.aliasField("items", BoLi.class, "bs");
        xstream.aliasField("shops", BoLi.class, "wnks");
    }

    private static void configureBo(XStream xstream) {
        xstream.alias("item", Bo.class);
        xstream.omitField(Bo.class, "boli");
    }

    private static void configureWnk(XStream xstream) {
        xstream.alias("shop", Wnk.class);
        xstream.addImplicitCollection(Wnk.class, "bs", "itemId", String.class);
    }

    private static void configureEvents(XStream xstream) {
        xstream.setMode(XStream.NO_REFERENCES);

        xstream.addImplicitCollection(Stack.class, "list");
        
        xstream.aliasField("name", Bo.class, "naam");
        xstream.aliasField("count", Bo.class, "aantal");
        xstream.aliasField("category", Bo.class, "categorie");
        xstream.aliasField("name", Wnk.class, "naam");

        xstream.aliasField("shop", ShopAddedEvent.class, "wnk");
        xstream.aliasField("shop", ShopDeletedEvent.class, "wnk");
        xstream.aliasField("item", ItemAddedEvent.class, "bo");
        xstream.aliasField("item", ItemDeletedEvent.class, "bo");

        xstream.addImplicitCollection(Wnk.class, "bs", "itemId", String.class);

        xstream.alias("itemAdded", ItemAddedEvent.class);
        xstream.alias("itemCategoryChanged", ItemCategoryChangedEvent.class);
        xstream.alias("itemCountChanged", ItemCountChangedEvent.class);
        xstream.alias("itemDeleted", ItemDeletedEvent.class);
        xstream.alias("itemNameChanged", ItemNameChangedEvent.class);

        xstream.alias("itemInShop", ItemInShop.class);
        xstream.alias("shopAdded", ShopAddedEvent.class);
        xstream.alias("shopDeleted", ShopDeletedEvent.class);
        xstream.alias("shopItemAdded", ShopItemAddedEvent.class);
        xstream.alias("shopItemMoved", ShopItemMovedEvent.class);
        xstream.alias("shopItemRemoved", ShopItemRemovedEvent.class);
        xstream.alias("shopNameChanged", ShopNameChangedEvent.class);
    }

}
