package bs.boli.persist;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import bs.boli.domain.BoLi;

/**
 * Wrapper voor de BoLi met de eventuele boodschappen die zijn aangepast in de herstel-actie.
 * 
 * Als gebruiker fouten heeft geintroduceerd in de opgeslagen state kan de app dit tot op zekere hoogte
 * herstellen.
 */
public class FixedBoli {
	private BoLi boLi;
	private Set<String> fixedBs = new HashSet<String>();

	public boolean hasFixes() {
		return !fixedBs.isEmpty();
	}

	public BoLi getBoLi() {
		return boLi;
	}

	public void setBoLi(BoLi boLi) {
		this.boLi = boLi;
	}

	public Set<String> getFixedBs() {
		return fixedBs;
	}

	public void addFixedBs(Collection<String> fixedBs) {
		this.fixedBs.addAll(fixedBs);
	}
}
