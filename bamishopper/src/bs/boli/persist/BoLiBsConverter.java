package bs.boli.persist;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bs.boli.domain.Bo;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

/**
 * Converts between a Map&lt;bonaam, bo&gt; and a List&lt;bo&gt;. Register this converter for the specific field.
 * <p>
 * This converter enables us to store the items in a map in the BoLi, and to represent the items in the xml
 * just as a list. The map in the BoLi is efficient, while the list in the xml is more human readable.
 * 
 */
public class BoLiBsConverter implements Converter {

    @Override
    public boolean canConvert(Class type) {
        return true;
    }

    /**
     * Converts the map to a list and marshal the list.
     */
    @Override
    public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
        Map<String, Bo> bsmap = (Map<String, Bo>) source;
        List<Bo> bs = new ArrayList<Bo>(bsmap.values());

        Collections.sort(bs, new Comparator<Bo>() {
            @Override
            public int compare(Bo lhs, Bo rhs) {
                return lhs.getNaam().compareTo(rhs.getNaam());
            }
        });

        context.convertAnother(bs);
    }

    /**
     * Unmarshal the list and convert to a map.
     */
    @Override
    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
        Class type = ArrayList.class;
        List<Bo> bs = (List<Bo>) context.convertAnother(context, type);

        Map<String, Bo> bsmap = toNameMap(bs);

        return bsmap;
    }

    /** Maps the bs to their names. */
    private static Map<String, Bo> toNameMap(List<Bo> bs) {
        Map<String, Bo> bsmap = new HashMap<String, Bo>();
        for (Bo b : bs) {
            bsmap.put(b.getNaam(), b);
        }
        return bsmap;
    }

}
