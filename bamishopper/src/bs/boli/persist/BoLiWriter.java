package bs.boli.persist;

import java.io.IOException;
import java.io.Writer;
import java.util.Stack;

import bs.boli.domain.Bo;
import bs.boli.domain.BoLi;
import bs.boli.domain.Wnk;

import com.thoughtworks.xstream.io.xml.PrettyPrintWriter;

/**
 * Adds the item-name and shop-name in xml comment after an itemId or shopId element.
 */
public class BoLiWriter extends PrettyPrintWriter {

    private static final String ITEMID = "itemId";
    private static final String SHOPID = "shopId";

    private Stack<String> tags = new Stack<String>();

    private String lastItemId;
    private String lastShopId;

    private final Writer writer;
    private final BoLi boli;

    public BoLiWriter(Writer writer, BoLi boli) {
        super(writer);
        this.writer = writer;
        this.boli = boli;
    }

    @Override
    public void startNode(String name) {
        super.startNode(name);
        this.tags.push(name);
    }

    @Override
    public void endNode() {
        super.endNode();

        if (atItemId()) {
            try {
                super.flush();
                Bo bo = boli.getBoById(lastItemId);
                String itemName = bo != null ? bo.getNaam() : "?";
                writer.write(String.format("   <!-- %s -->", itemName));
                writer.flush();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } else if (atShopId()) {
            try {
                super.flush();
                Wnk wnk = boli.getWnkById(lastShopId);
                String shopName = wnk != null ? wnk.getNaam() : "?";
                writer.write(String.format("   <!-- %s -->", shopName));
                writer.flush();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        this.tags.pop();
    }

    private boolean atItemId() {
        return tags.lastElement().equals(ITEMID);
    }

    private boolean atShopId() {
        return tags.lastElement().equals(SHOPID);
    }

    @Override
    public void setValue(String text) {
        super.setValue(text);

        if (atItemId()) {
            lastItemId = text;
        } else if (atShopId()) {
            lastShopId = text;
        }
    }
}
