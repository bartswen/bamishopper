package bs.boli.persist;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import android.os.Environment;
import android.util.Log;
import bs.boli.domain.Bo;
import bs.boli.domain.BoLi;
import bs.boli.domain.Utils;
import bs.boli.domain.Wnk;
import bs.bufferedfile.BufferedFile;

public class BoLiPersister {
    private static final String DIRNAME = "BaMiShopper";
    private static final String FILENAME = "bamishopper.xml";

    private final BufferedFile file;
    private final String rootDir;

    private static Map<String, BoLiPersister> INSTANCE_MAP = new HashMap<String, BoLiPersister>();

    private BoLiPersister(String rootDir) throws NoStorageAvailable {
        this.rootDir = rootDir;
        this.file = new BufferedFile(createFilepath(rootDir));
        checkStorageAvailability();
        checkAndCreateDir();
    }

    private static String createFilepath(String rootDir) {
        String filePath = rootDir + "/" + DIRNAME + "/" + FILENAME;
        return filePath;
    }

    public static synchronized BoLiPersister getInstance(String rootDir) throws NoStorageAvailable {
        BoLiPersister persister = INSTANCE_MAP.get(rootDir);
        if (persister == null) {
            persister = new BoLiPersister(rootDir);
            INSTANCE_MAP.put(rootDir, persister);
        }
        return persister;
    }

    /** Returns new BoLi if no data exists. */
    public BoLi read() {
        Log.i(this.getClass().getName(), "read()");

        final BoLi boli;
        if (file.exists()) {

            File appFile = new File(createFilepath(rootDir));
            FileInputStream fis = null;
            String boliXml = null;
            try {
                fis = new FileInputStream(appFile);
                boliXml = Utils.toString(fis);
            } catch (IOException e) {
                throw new AssertionError();
            } finally {
                close(fis);
            }

            boli = BoLiXmlParser.fromLocalXml(boliXml);
        } else {

            boli = createInitialBoLi();
        }

        return boli;
    }

    private static BoLi createInitialBoLi() {
        final BoLi boli;
        boli = new BoLi();
        boli.addBo(new Bo("kaas", "zuivel"));
        boli.addBo(new Bo("melk", "zuivel"));
        boli.addBo(new Bo("brood", "broodafdeling"));
        boli.addBo(new Bo("jus d'orange", "sappen & dranken"));
        boli.addBo(new Bo("koriander plantje"));
        boli.addWnk(new Wnk("winkel 1"));
        boli.addWnk(new Wnk("winkel 2"));
        boli.addBoToWnk("kaas", "winkel 1");
        boli.addBoToWnk("melk", "winkel 1");
        boli.addBoToWnk("jus d'orange", "winkel 1");
        boli.addBoToWnk("koriander plantje", "winkel 2");
        boli.addBoToWnk("jus d'orange", "winkel 2");
        boli.incrementBoByNaam("koriander plantje");
        boli.incrementBoByNaam("kaas");
        boli.incrementBoByNaam("jus d'orange");
        boli.incrementBoByNaam("jus d'orange");
        boli.clearAllEventsToPublish();
        return boli;
    }

    /** Returns true if write took place */
    public boolean write(BoLi boli) {
            Log.i(this.getClass().getName(), "write()");

            // defensive
            if (boli == null) {
                return false;
            }

            String boliXml = BoLiXmlParser.toSharedXml(boli);

            boolean write = file.write(boliXml);
            
            return write;
    }

    private static void close(Closeable closable) {
        if (closable != null) {
            try {
                closable.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private static void checkStorageAvailability() throws NoStorageAvailable {
        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state)) {
            // We can read and write the media
        } else {
            throw new NoStorageAvailable();
        }
    }
    
    private void checkAndCreateDir() {
        File appDir = new File(rootDir, DIRNAME);
        if (!appDir.exists()) {
            appDir.mkdir();
        }
    }
}
