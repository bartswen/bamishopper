package bs.boli.persist;

/**
 * External storage is in an insuited state. Maybe it is not present, read-only or USB is mounted to an host
 * system.
 */
public class NoStorageAvailable extends Exception {

	private static final long serialVersionUID = 1L;

}
