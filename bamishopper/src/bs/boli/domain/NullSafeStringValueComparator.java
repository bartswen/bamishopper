package bs.boli.domain;

import java.io.Serializable;
import java.util.Comparator;

public class NullSafeStringValueComparator implements Comparator<Object>, Serializable {
    private static final long serialVersionUID = 1L;
    
    private final boolean nullsFirst;

    public NullSafeStringValueComparator() {
        super();
        this.nullsFirst = true;
    }

    public NullSafeStringValueComparator(boolean nullsFirst) {
        super();
        this.nullsFirst = nullsFirst;
    }

    @Override
    public int compare(Object o1, Object o2) {
        if (o2 == null && o1 == null) {
            return 0;
        } else if (o1 == null) {
            return nullsFirst ? -1 : +1;
        } else if (o2 == null) {
            return nullsFirst ? 1 : -1;
        } else {
            return o1.toString().compareTo(o2.toString());
        }
    }
}
