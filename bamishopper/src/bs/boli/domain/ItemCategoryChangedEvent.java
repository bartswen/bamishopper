package bs.boli.domain;

import java.util.Date;

public final class ItemCategoryChangedEvent extends BoLiEvent {
    private final String itemId;
    private final String oldCat, newCat;

    /** For XStream, runs in 'Pure Java' mode on android */
    private ItemCategoryChangedEvent() {
        super(null);
        this.itemId = null;
        this.oldCat = null;
        this.newCat = null;
    }

    public ItemCategoryChangedEvent(Date eventTime, String itemId, String oldCat, String newCat) {
        super(eventTime);
        this.itemId = itemId;
        this.oldCat = oldCat;
        this.newCat = newCat;
    }

    @Override
    public void accept(EventVisitor v) {
        v.visit(this);
    }

    public String getItemId() {
        return itemId;
    }

    public String getOldCat() {
        return oldCat;
    }

    public String getNewCat() {
        return newCat;
    }

    @Override
    public BoLiEvent createCompensation(Date eventTime) {
        BoLiEvent comp = new ItemCategoryChangedEvent(eventTime, itemId, newCat, oldCat);
        return comp;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((itemId == null) ? 0 : itemId.hashCode());
        result = prime * result + ((newCat == null) ? 0 : newCat.hashCode());
        result = prime * result + ((oldCat == null) ? 0 : oldCat.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        ItemCategoryChangedEvent other = (ItemCategoryChangedEvent) obj;
        if (itemId == null) {
            if (other.itemId != null)
                return false;
        } else if (!itemId.equals(other.itemId))
            return false;
        if (newCat == null) {
            if (other.newCat != null)
                return false;
        } else if (!newCat.equals(other.newCat))
            return false;
        if (oldCat == null) {
            if (other.oldCat != null)
                return false;
        } else if (!oldCat.equals(other.oldCat))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "ItemCategoryChangedEvent [itemId=" + itemId + ", oldCat=" + oldCat + ", newCat=" + newCat
                + "]";
    }
}
