package bs.boli.domain;

public class Bo implements EqualsByValues<Bo> {
    private String id;
    private String naam;
    private int aantal;
    private String categorie;
    private transient BoLi boli;

    public Bo(String naam) {
        this.naam = naam;
    }

    public Bo(String naam, int aantal) {
        this.naam = naam;
        this.aantal = aantal;
    }

    public Bo(String naam, String cat) {
        this.naam = naam;
        this.categorie = cat;
    }

    public Bo() {
    }

    public Bo cloneAsOrphan() {
        Bo b = new Bo();
        b.id = id;
        b.naam = naam;
        b.aantal = aantal;
        b.categorie = categorie;
        return b;
    }

    public String getId() {
        return id;
    }

    void setId(String id) {
        this.id = id;
    }

    public String getNaam() {
        return naam;
    }

    public int getAantal() {
        return aantal;
    }

    public String getCategorie() {
        return categorie;
    }

    void setAantal(int aantal) {
        this.aantal = aantal;
    }

    public void setCategorie(String categorie) {
        if (!StringUtils.equal(categorie, this.categorie)) {
            String oldCat = this.categorie;
            setCategoryLogic(categorie);
            notifyBoliCategoryChanged(categorie, oldCat);
        } else {
            return;
        }
    }

    void notifyBoliCategoryChanged(String categorie, String oldCat) {
        if (this.boli != null) {
            this.boli.boCategoryChanged(getId(), oldCat, categorie);
        }
    }

    /** Triggers no event, just update category */
    void setCategoryLogic(String categorie) {
        this.categorie = categorie;
    }

    void setNaam(String naam) {
        this.naam = naam;
    }

    void setBoli(BoLi boli) {
        this.boli = boli;
    }

    public String suggestId() {
        return Utils.suggestId(getNaam());
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + aantal;
        result = prime * result + ((categorie == null) ? 0 : categorie.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((naam == null) ? 0 : naam.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Bo other = (Bo) obj;
        if (aantal != other.aantal)
            return false;
        if (categorie == null) {
            if (other.categorie != null)
                return false;
        } else if (!categorie.equals(other.categorie))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (naam == null) {
            if (other.naam != null)
                return false;
        } else if (!naam.equals(other.naam))
            return false;
        return true;
    }

    /**
     * Does not compare id's.
     */
    public boolean equalsValues(Bo obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Bo other = (Bo) obj;
        if (aantal != other.aantal)
            return false;
        if (categorie == null) {
            if (other.categorie != null)
                return false;
        } else if (!categorie.equals(other.categorie))
            return false;
        if (naam == null) {
            if (other.naam != null)
                return false;
        } else if (!naam.equals(other.naam))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Bo [id=" + id + ", naam=" + naam + ", aantal=" + aantal + ", categorie=" + categorie + "]";
    }
}
