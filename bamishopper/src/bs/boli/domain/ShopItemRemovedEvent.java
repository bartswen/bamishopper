package bs.boli.domain;

import java.util.Date;

public final class ShopItemRemovedEvent extends BoLiEvent {
    private final String shopId, itemId;
    private final int itemPosition;

    /** For XStream, runs in 'Pure Java' mode on android */
    private ShopItemRemovedEvent() {
        super(null);
        this.shopId = null;
        this.itemId = null;
        this.itemPosition = 0;
    }

    public ShopItemRemovedEvent(Date eventTime, String shopId, String itemId, int itemPosition) {
        super(eventTime);
        this.shopId = shopId;
        this.itemId = itemId;
        this.itemPosition = itemPosition;
    }

    @Override
    public void accept(EventVisitor v) {
        v.visit(this);
    }

    public String getShopId() {
        return shopId;
    }

    public String getItemId() {
        return itemId;
    }

    int getItemPosition() {
        return itemPosition;
    }

    @Override
    public BoLiEvent createCompensation(Date eventTime) {
        BoLiEvent comp = new ShopItemAddedEvent(eventTime, shopId, itemId);
        return comp;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((itemId == null) ? 0 : itemId.hashCode());
        result = prime * result + itemPosition;
        result = prime * result + ((shopId == null) ? 0 : shopId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        ShopItemRemovedEvent other = (ShopItemRemovedEvent) obj;
        if (itemId == null) {
            if (other.itemId != null)
                return false;
        } else if (!itemId.equals(other.itemId))
            return false;
        if (itemPosition != other.itemPosition)
            return false;
        if (shopId == null) {
            if (other.shopId != null)
                return false;
        } else if (!shopId.equals(other.shopId))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "ShopItemRemovedEvent [shopId=" + shopId + ", itemId=" + itemId + ", itemPosition="
                + itemPosition + "]";
    }

}
