package bs.boli.domain;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;

public final class Utils {
	private static final String DEFAULT_ID = "x";
    private static final int BUFFER_SIZE = 1024;

	public static boolean isEmpty(String s) {
		return s == null || s.equals("");
	}

	public static String toString(InputStream input) throws IOException {
		StringWriter sw = new StringWriter();
		InputStreamReader in = new InputStreamReader(input);
		Utils.copy(in, sw);
		return sw.toString();
	}

    public static String toString(Object o) {
        if (o !=null) {
            return o.toString();
        } else {
            return "null";
        }
    }

	public static long copy(Reader input, Writer output) throws IOException {
		char[] buffer = new char[BUFFER_SIZE];
		long count = 0;
		int n = 0;
		while (-1 != (n = input.read(buffer))) {
			output.write(buffer, 0, n);
			count += n;
		}
		return count;
	}

    /** Suggest an identifier related to the name of an object. Returns 'x' if the id would be empty. */
	public static String suggestId(String name) {
        String id = StringUtils.filterConsonants(name);
        if (StringUtils.isEmpty(id)){
            id = DEFAULT_ID;
        }
        return id;
    }
}
