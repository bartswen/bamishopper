package bs.boli.domain;

/** For ItemDeletedEvent and ItemAddedEvent */
public class ItemInShop {
    private final String shopId;
    private final int itemPosition;

    /** For XStream, runs in 'Pure Java' mode on android */
    @SuppressWarnings("unused")
    private ItemInShop() {
        super();
        this.shopId = null;
        this.itemPosition = 0;
    }

    public ItemInShop(String shopId, int itemPosition) {
        super();
        this.shopId = shopId;
        this.itemPosition = itemPosition;
    }

    public String getShopId() {
        return shopId;
    }

    public int getItemPosition() {
        return itemPosition;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + itemPosition;
        result = prime * result + ((shopId == null) ? 0 : shopId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ItemInShop other = (ItemInShop) obj;
        if (itemPosition != other.itemPosition)
            return false;
        if (shopId == null) {
            if (other.shopId != null)
                return false;
        } else if (!shopId.equals(other.shopId))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "ItemInShop [shopId=" + shopId + ", itemPosition=" + itemPosition + "]";
    }
}
