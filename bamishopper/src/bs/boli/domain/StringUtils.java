package bs.boli.domain;

public class StringUtils {
    public static String EMPTY = "";

    public static String emptyIfNull(String string) {
        return string == null ? EMPTY : string;
    }

    public static String defaultString(String str, String defaultStr) {
        return str == null ? defaultStr : str;
    }

    public static boolean isEmpty(String s) {
        return s == null || EMPTY.equals(s);
    }

    public static String filterConsonants(String s) {
        char[] chars = s.toCharArray();
        final StringBuilder sb = new StringBuilder();

        for (char c : chars) {
            if (isConsonant(c)) {
                sb.append(c);
            }
        }
        return sb.toString();
    }

    public static boolean equal(String left, String right) {
        if (left == null && right == null) {
            return true;
        } else if (left != null && right != null) {
            return left.equals(right);
        }
        return false;
    }

    private static boolean isVowel(char c) {
        char l = Character.toLowerCase(c);
        return (l == 'a' || l == 'e' || l == 'i' || l == 'o' || l == 'u');
    }

    private static boolean isConsonant(char c) {
        return (((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')) && !isVowel(c));
    }

}
