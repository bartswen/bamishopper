package bs.boli.domain;

public interface EqualsByValues<T> {
    boolean equalsValues(T other);
}
