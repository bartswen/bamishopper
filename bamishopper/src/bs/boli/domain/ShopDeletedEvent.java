package bs.boli.domain;

import java.util.Date;

public final class ShopDeletedEvent extends BoLiEvent {
    private final Wnk wnk;

    /** For XStream, runs in 'Pure Java' mode on android */
    private ShopDeletedEvent() {
        super(null);
        this.wnk = null;
    }

    /** Pass the original Wnk, this conctructor will clone it */
    public ShopDeletedEvent(Date eventTime, bs.boli.domain.Wnk wnk) {
        super(eventTime);
        this.wnk = wnk.cloneAsOrphan();
    }

    @Override
    public void accept(EventVisitor v) {
        v.visit(this);
    }

    public Wnk getWnk() {
        return wnk;
    }

    @Override
    public BoLiEvent createCompensation(Date eventTime) {
        BoLiEvent comp = new ShopAddedEvent(eventTime, wnk);
        return comp;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((wnk == null) ? 0 : wnk.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        ShopDeletedEvent other = (ShopDeletedEvent) obj;
        if (wnk == null) {
            if (other.wnk != null)
                return false;
        } else if (!wnk.equals(other.wnk))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "ShopDeletedEvent [Wnk=" + wnk + "]";
    }

}
