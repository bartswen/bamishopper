package bs.boli.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import bs.boli.domain.CollectionUtils.Transformer;

public class Wnk implements EqualsByValues<Wnk> {
    private String id;
    private String naam;
    private List<String> bs = new ArrayList<String>(); // bo id's
    private transient BoLi boli;

    private transient Transformer<String, String> bsIdToNameTransformer = new Transformer<String, String>() {
        @Override
        public String transform(final String boId) {
            Bo bo = boli.getBoById(boId);
            if (bo == null) {
                throw new AssertionError(String.format("Unknown item id '%s' in shop '%s'", boId,
                        Wnk.this.naam));
            }
            return bo.getNaam();
        }
    };

    private transient Transformer<String, String> bsNameToIdTransformer = new Transformer<String, String>() {
        @Override
        public String transform(final String boName) {
            Bo bo = boli.getBoByNaam(boName);
            if (bo == null) {
                return null;
            }
            return bo.getId();
        }
    };

    public Wnk(String naam) {
        this.naam = naam;
    }

    public Wnk() {
        super();
    }

    public Wnk cloneAsOrphan() {
        Wnk w = new Wnk();
        w.id = id;
        w.naam = naam;
        w.bs.clear();
        w.bs.addAll(bs);
        return w;
    }

    public String getId() {
        return id;
    }

    void setId(String id) {
        this.id = id;
    }

    public String getNaam() {
        return naam;
    }

    public List<String> collectBsNames() {
        this.bs.removeAll(Collections.singleton(null));
        return CollectionUtils.collect(this.bs, bsIdToNameTransformer);
    }

    public boolean heeft(String boNaam) {
        String boId = bsNameToIdTransformer.transform(boNaam);
        return bs.contains(boId);
    }

    /**
     * Moves the first item to the position of the second
     */
    public void moveFirstAfterSecond(String firstItemName, String secondItemName) {
        String secondId = bsNameToIdTransformer.transform(secondItemName);
        int i = bs.indexOf(secondId);
        toIndex(firstItemName, i);
    }

    public void setNaam(String naam) {
        if (!StringUtils.equal(naam, this.naam)) {
            String oldName = this.naam;
            setNaamLogic(naam);
            notifyBoliWnkNameChanged(naam, oldName);
        }
    }

    /** just the state change */
    void setNaamLogic(String naam) {
        this.naam = naam;
    }

    void notifyBoliWnkNameChanged(String naam, String oldName) {
        if (boli != null) {
            boli.wnkNameChanged(id, oldName, naam);
        }
    }

    boolean addBo(String naam) {
        String boId = bsNameToIdTransformer.transform(naam);
        if (!bs.contains(boId)) {
            bs.add(0, boId);
            return true;
        } else {
            return false;
        }
    }

    boolean addBo(String naam, int index) {
        String boId = bsNameToIdTransformer.transform(naam);
        if (!bs.contains(boId)) {
            bs.add(index, boId);
            return true;
        } else {
            return false;
        }
    }

    void setBoli(BoLi boli) {
        this.boli = boli;
    }

    public String suggestId() {
        return Utils.suggestId(getNaam());
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((bs == null) ? 0 : bs.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((naam == null) ? 0 : naam.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Wnk other = (Wnk) obj;
        if (bs == null) {
            if (other.bs != null)
                return false;
        } else if (!bs.equals(other.bs))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (naam == null) {
            if (other.naam != null)
                return false;
        } else if (!naam.equals(other.naam))
            return false;
        return true;
    }

    /**
     * Does not compare ids.
     */
    public boolean equalsValues(Wnk w) {
        if (this == w)
            return true;
        if (w == null)
            return false;
        if (getClass() != w.getClass())
            return false;
        Wnk other = (Wnk) w;
        if (bs == null) {
            if (other.bs != null)
                return false;
        } else if (!bs.equals(other.bs))
            return false;
        if (naam == null) {
            if (other.naam != null)
                return false;
        } else if (!naam.equals(other.naam))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Wnk [id=" + id + ", naam=" + naam + ", bs=" + bs + "]";
    }

    public void toIndex(String bonaam, int index) {
        if (index < 0 || index > bs.size() - 1) {
            return;
        }

        String boId = bsNameToIdTransformer.transform(bonaam);
        int i = bs.indexOf(boId);
        if (i < 0 || i == bs.size() - 1) {
            return;
        }

        toIndexLogic(bonaam, index);

        boli.boMoved(this.naam, bonaam, i, index);
    }

    /** just the state change */
    int toIndexLogic(String bonaam, int index) {
        String boId = bsNameToIdTransformer.transform(bonaam);
        int i = bs.indexOf(boId);
        bs.remove(i);
        bs.add(index, boId);
        return i;
    }

    int remove(String bonaam) {
        String boId = bsNameToIdTransformer.transform(bonaam);
        int pos = bs.indexOf(boId);
        bs.remove(boId);
        return pos;
    }

    public List<String> collectBoNames() {
        return Collections.unmodifiableList(CollectionUtils.collect(this.bs, bsIdToNameTransformer));
    }

    public int findFirstItemOfCat(final String categorie) {
        if (categorie == null) {
            return -1;
        }
        
        return CollectionUtils.indexOf(this.bs, new CollectionUtils.Predicate<String>() {
            @Override
            public boolean evaluate(String itemId) {
                Bo bo = boli.getBoById(itemId);
                return categorie.equals(bo.getCategorie());
            }
        });
    }

    public int itemsSize() {
        return bs.size();
    }
}
