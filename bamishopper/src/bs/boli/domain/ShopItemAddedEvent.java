package bs.boli.domain;

import java.util.Date;

public final class ShopItemAddedEvent extends BoLiEvent {
    private final String shopId, itemId;

    /** For XStream, runs in 'Pure Java' mode on android */
    private ShopItemAddedEvent() {
        super(null);
        this.shopId = null;
        this.itemId = null;
    }

    public ShopItemAddedEvent(Date eventTime, String shopId, String itemId) {
        super(eventTime);
        this.shopId = shopId;
        this.itemId = itemId;
    }

    @Override
    public void accept(EventVisitor v) {
        v.visit(this);
    }

    public String getShopId() {
        return shopId;
    }

    public String getItemId() {
        return itemId;
    }

    @Override
    public BoLiEvent createCompensation(Date eventTime) {
        BoLiEvent comp = new ShopItemRemovedEvent(eventTime, shopId, itemId, 0);
        return comp;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((itemId == null) ? 0 : itemId.hashCode());
        result = prime * result + ((shopId == null) ? 0 : shopId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        ShopItemAddedEvent other = (ShopItemAddedEvent) obj;
        if (itemId == null) {
            if (other.itemId != null)
                return false;
        } else if (!itemId.equals(other.itemId))
            return false;
        if (shopId == null) {
            if (other.shopId != null)
                return false;
        } else if (!shopId.equals(other.shopId))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "ShopItemAddedEvent [shopId=" + shopId + ", itemId=" + itemId + "]";
    }
}
