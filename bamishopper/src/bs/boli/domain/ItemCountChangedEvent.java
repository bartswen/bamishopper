package bs.boli.domain;

import java.util.Date;

public final class ItemCountChangedEvent extends BoLiEvent {
    private final String itemId;
    private int oldCount, newCount;

    /** For XStream, runs in 'Pure Java' mode on android */
    private ItemCountChangedEvent() {
        super(null);
        this.itemId = null;
    }

    public ItemCountChangedEvent(Date eventTime, String itemId, int oldCount, int newCount) {
        super(eventTime);
        this.itemId = itemId;
        this.oldCount = oldCount;
        this.newCount = newCount;
    }

    @Override
    public void accept(EventVisitor v) {
        v.visit(this);
    }
    
    public String getItemId() {
        return itemId;
    }

    public int getOldCount() {
        return oldCount;
    }

    public int getNewCount() {
        return newCount;
    }

    @Override
    public BoLiEvent createCompensation(Date eventTime) {
        BoLiEvent comp = new ItemCountChangedEvent(eventTime, itemId, newCount, oldCount);
        return comp;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((itemId == null) ? 0 : itemId.hashCode());
        result = prime * result + newCount;
        result = prime * result + oldCount;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ItemCountChangedEvent other = (ItemCountChangedEvent) obj;
        if (itemId == null) {
            if (other.itemId != null)
                return false;
        } else if (!itemId.equals(other.itemId))
            return false;
        if (newCount != other.newCount)
            return false;
        if (oldCount != other.oldCount)
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "ItemCountChangedEvent [itemId=" + itemId + ", oldCount=" + oldCount + ", newCount="
                + newCount + "]";
    }


}
