package bs.boli.domain;

import java.util.Date;

public final class ShopNameChangedEvent extends BoLiEvent {
    private final String shopId;
    private final String oldName, newName;

    /** For XStream, runs in 'Pure Java' mode on android */
    private ShopNameChangedEvent() {
        super(null);
        this.shopId = null;
        this.oldName = null;
        this.newName = null;
    }

    public ShopNameChangedEvent(Date eventTime, String shopId, String oldName, String newName) {
        super(eventTime);
        this.shopId = shopId;
        this.oldName = oldName;
        this.newName = newName;
    }

    @Override
    public void accept(EventVisitor v) {
        v.visit(this);
    }

    public String getShopId() {
        return shopId;
    }

    public String getOldName() {
        return oldName;
    }

    public String getNewName() {
        return newName;
    }

    @Override
    public BoLiEvent createCompensation(Date eventTime) {
        BoLiEvent comp = new ShopNameChangedEvent(eventTime, shopId, newName, oldName);
        return comp;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((newName == null) ? 0 : newName.hashCode());
        result = prime * result + ((oldName == null) ? 0 : oldName.hashCode());
        result = prime * result + ((shopId == null) ? 0 : shopId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        ShopNameChangedEvent other = (ShopNameChangedEvent) obj;
        if (newName == null) {
            if (other.newName != null)
                return false;
        } else if (!newName.equals(other.newName))
            return false;
        if (oldName == null) {
            if (other.oldName != null)
                return false;
        } else if (!oldName.equals(other.oldName))
            return false;
        if (shopId == null) {
            if (other.shopId != null)
                return false;
        } else if (!shopId.equals(other.shopId))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "ShopNameChangedEvent [shopId=" + shopId + ", oldName=" + oldName + ", newName=" + newName
                + "]";
    }
}