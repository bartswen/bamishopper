package bs.boli.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import bs.bamishopper.BoLiChangeListener;
import bs.boli.domain.CollectionUtils.Closure;
import bs.boli.domain.ItemInShop;
import bs.optional.Optional;

/**
 * The only thread-safe method is processEvents(). This one may be invoked from a background process.
 */
public class BoLi implements EqualsByValues<BoLi>, EventVisitor {
    private static final int UNDO_SIZE = 10;

    private final Map<String, Bo> bs; // bonaam to bo
    private final List<Wnk> wnks;

    private final List<BoLiEvent> eventsToPublish; // in eventtime order
    private final OverflowStack<BoLiEvent> undoStack;
    private final Clock clock;

    private transient Optional<BoLiChangeListener> changeListener = Optional.absent();

    public BoLi() {
        this(new SystemClock());
    }

    public BoLi(Clock clock) {
        bs = new HashMap<String, Bo>(); // bonaam to bo
        wnks = new ArrayList<Wnk>();
        eventsToPublish = new ArrayList<BoLiEvent>();
        this.clock = clock;
        this.undoStack = new OverflowStack<BoLiEvent>(UNDO_SIZE);
    }

    public BoLi(BoLi copyFrom) {
        bs = copyFrom.bs;
        wnks = copyFrom.wnks;
        eventsToPublish = copyFrom.eventsToPublish;
        clock = copyFrom.clock;
        undoStack = copyFrom.undoStack;
    }

    /** Replaces any existing listener */
    public void registerEventsProcessedListener(BoLiChangeListener l) {
        this.changeListener = Optional.of(l);
    }

    public void unregisterEventsProcessedListener() {
        this.changeListener = Optional.absent();
    }

    public void addBo(Bo bo) {
        if (bs.containsKey(bo.getNaam())) {
            return;
        }

        generateAndSetBoId(bo);
        addBoLogic(bo);

        ItemAddedEvent e = new ItemAddedEvent(clock.now(), bo);
        registerEvent(e);
    }

    /** Triggers no event and does not generate new id, just the state change */
    void addBoLogic(Bo bo) {
        bo.setBoli(this);
        bs.put(bo.getNaam(), bo);
    }

    /** Store the fact that a user action has occurred, for later publication or possible undo. */
    private void registerEvent(BoLiEvent e) {
        eventsToPublish.add(e);
        undoStack.push(e);
    }

    /**
     * Generates a new id for the Bo if necessary. If the passed bo has a unique id then this method has no
     * effect.
     */
    private void generateAndSetBoId(Bo bo) {
        if (isBoIdUnique(bo.getId())) {
            return;
        }
        String id = bo.suggestId();
        if (!isBoIdUnique(id)) {
            id = createUniqueBoId(id, 1);
        }
        bo.setId(id);
    }

    private String createUniqueBoId(String suggestedId, int suffix) {
        String idWithSuffix = suggestedId + suffix;
        if (isBoIdUnique(idWithSuffix)) {
            return idWithSuffix;
        } else {
            return createUniqueBoId(suggestedId, suffix + 1);
        }
    }

    /**
     * Returns true if id is not null and unique in this BoLi. Accepts a null id, returns false in this case.
     */
    private boolean isBoIdUnique(String id) {
        if (id == null) {
            return false;
        }
        return getBoById(id) == null;
    }

    private boolean hasBo(String itemId) {
        return getBoById(itemId) != null;
    }

    public Bo getBoById(final String id) {
        Bo found = CollectionUtils.find(bs.values(), new CollectionUtils.Predicate<Bo>() {
            @Override
            public boolean evaluate(Bo bo) {
                return id.equals(bo.getId());
            }
        });
        return found;
    }

    /**
     * Generates a new id for the Wnk if necessary. If the passed wnk has a unique id then this method has no
     * effect.
     */
    private void generateAndSetWnkId(Wnk wnk) {
        if (isWnkIdUnique(wnk.getId())) {
            return;
        }
        String id = wnk.suggestId();
        if (!isWnkIdUnique(id)) {
            id = createUniqueWnkId(id, 1);
        }
        wnk.setId(id);
    }

    private String createUniqueWnkId(String suggestedId, int suffix) {
        String idWithSuffix = suggestedId + suffix;
        if (isWnkIdUnique(idWithSuffix)) {
            return idWithSuffix;
        } else {
            return createUniqueWnkId(suggestedId, suffix + 1);
        }
    }

    /**
     * Returns true if id is not null and unique in this BoLi. Accepts a null id, returns false in this case.
     */
    private boolean isWnkIdUnique(String id) {
        if (id == null) {
            return false;
        }
        return getWnkById(id) == null;
    }

    private boolean hasWnk(String shopId) {
        return getWnkById(shopId) != null;
    }

    public Wnk getWnkById(final String id) {
        Wnk found = CollectionUtils.find(wnks, new CollectionUtils.Predicate<Wnk>() {
            @Override
            public boolean evaluate(Wnk wnk) {
                return id.equals(wnk.getId());
            }
        });
        return found;
    }

    /** Returns the total number of shopping items. */
    public int size() {
        return bs.size();
    }

    public void addWnk(Wnk wnk) {
        if (getWnk(wnk.getNaam()) != null) {
            return;
        }

        generateAndSetWnkId(wnk);
        addWnkLogic(wnk);

        ShopAddedEvent e = new ShopAddedEvent(clock.now(), wnk);
        registerEvent(e);
    }

    /** Triggers no event, no id generation, just the state change */
    void addWnkLogic(Wnk wnk) {
        wnk.setBoli(this);
        wnks.add(wnk);
    }

    /**
     * Return shops in the order they were added.
     */
    public List<Wnk> getWnks() {
        return Collections.unmodifiableList(wnks);
    }

    public List<Wnk> getWnksForBo(String boNaam) {
        List<Wnk> wnksForBo = new ArrayList<Wnk>();
        for (Wnk wnk : wnks) {
            if (wnk.heeft(boNaam)) {
                // TODO WTF???
                wnksForBo.add(wnk);
            }
        }
        return wnksForBo;
    }

    public Wnk getWnk(final String naam) {
        return CollectionUtils.find(wnks, new CollectionUtils.Predicate<Wnk>() {
            public boolean evaluate(Wnk wnk) {
                return wnk.getNaam().equals(naam);
            }
        });
    }

    public Wnk getWnkIgnoreCase(final String name) {
        Wnk found = CollectionUtils.find(wnks, new CollectionUtils.Predicate<Wnk>() {
            @Override
            public boolean evaluate(Wnk wnk) {
                return wnk.getNaam().equalsIgnoreCase(name);
            }
        });

        return found;
    }

    /**
     * Add bo first in the pickorder list.
     * 
     * @param bonaam
     * @param wnknaam
     */
    public void addBoToWnk(String bonaam, String wnknaam) {
        if (!bs.containsKey(bonaam)) {
            return;
        }

        Bo bo = getBoByNaam(bonaam);
        Wnk wnk = getWnk(wnknaam);
        int pos = wnk.findFirstItemOfCat(bo.getCategorie());
        boolean added = addBoToWnkLogic(bonaam, wnk, pos);

        if (added) {
            ShopItemAddedEvent e = new ShopItemAddedEvent(clock.now(), wnk.getId(), bo.getId());
            registerEvent(e);
        }
    }

    /** Triggers no event, just the state change */
    boolean addBoToWnkLogic(String bonaam, Wnk wnk) {
        return wnk.addBo(bonaam);
    }

    /** Triggers no event, just the state change */
    boolean addBoToWnkLogic(String bonaam, Wnk wnk, int position) {
        if (position > 0 && position <= wnk.itemsSize()) {
            return wnk.addBo(bonaam, position);
        } else {
            return wnk.addBo(bonaam);
        }
    }

    public void removeBoFromWnk(String bonaam, String wnknaam) {
        if (!bs.containsKey(bonaam)) {
            return;
        }

        Wnk wnk = getWnk(wnknaam);
        Optional<ItemInShop> removedShopItem = removeBoFromWnkLogic(bonaam, wnk);

        if (removedShopItem.isPresent()) {
            ItemInShop itemInShop = removedShopItem.get();
            Bo bo = getBoByNaam(bonaam);
            ShopItemRemovedEvent e = new ShopItemRemovedEvent(clock.now(), wnk.getId(), bo.getId(),
                    itemInShop.getItemPosition());
            registerEvent(e);
        }
    }

    /**
     * Triggers no event, just the state change
     */
    Optional<ItemInShop> removeBoFromWnkLogic(String bonaam, Wnk wnk) {
        int position = wnk.remove(bonaam);
        ItemInShop removedShopItem = position < 0 ? null : new ItemInShop(wnk.getId(), position);
        return Optional.fromNullable(removedShopItem);
    }

    public List<Bo> getBsForWnk(String wnknaam) {
        return Collections.unmodifiableList((List<Bo>) CollectionUtils.collect(getWnk(wnknaam)
                .collectBsNames(), new CollectionUtils.Transformer<String, Bo>() {
            public Bo transform(String bsnaam) {
                return bs.get(bsnaam);
            }
        }));
    }

    public Set<String> getMappedBsNames() {
        return bs.keySet();
    }

    public List<Bo> getBsAlphabetic() {
        List<Bo> bsAlpha = new ArrayList<Bo>(bs.values());
        Collections.sort(bsAlpha, new Comparator<Bo>() {
            public int compare(Bo b1, Bo b2) {
                return b1.getNaam().compareTo(b2.getNaam());
            }
        });
        return bsAlpha;
    }

    /**
     * Ongecategoriseerde boodschappen onder de null key.
     */
    public Map<String, List<Bo>> getBsGroupByCat() {
        final Map<String, List<Bo>> bsByCat = new HashMap<String, List<Bo>>();
        CollectionUtils.forAllDo(getBsAlphabetic(), new CollectionUtils.Closure<Bo>() {
            public void execute(Bo bo) {
                List<Bo> catBs = bsByCat.get(bo.getCategorie());
                if (catBs == null) {
                    catBs = new ArrayList<Bo>();
                    bsByCat.put(bo.getCategorie(), catBs);
                }
                catBs.add(bo);

            }
        });
        return bsByCat;
    }

    public void deleteWnk(String naam) {
        if (getWnk(naam) == null) {
            return;
        }

        Wnk wnk = getWnk(naam);
        deleteWnkLogic(naam);

        ShopDeletedEvent e = new ShopDeletedEvent(clock.now(), wnk);
        registerEvent(e);
    }

    /** Triggers no event, just the state change */
    void deleteWnkLogic(String naam) {
        wnks.remove(getWnk(naam));
    }

    public Bo getBoByAlfaIndex(int index) {
        List<Bo> alfa = getBsAlphabetic();
        return alfa.get(index);
    }

    public List<Bo> teKopenVoor(String wnknaam) {
        final List<Bo> tekopen = new ArrayList<Bo>();
        Wnk wnk = getWnk(wnknaam);

        Closure<String> voegTeKopenBsToeAanLijstClosure = new CollectionUtils.Closure<String>() {
            public void execute(String bonaam) {
                Bo bo = BoLi.this.getBoByNaam(bonaam);
                if (bo.getAantal() > 0) {
                    tekopen.add(bo);
                }
            }
        };
        CollectionUtils.forAllDo(wnk.collectBsNames(), voegTeKopenBsToeAanLijstClosure);

        return tekopen;
    }

    public void incrementBoByAlfaIndex(int index) {
        List<Bo> alfa = getBsAlphabetic();
        Bo bo = alfa.get(index);
        incrementBo(bo);
    }

    public void incrementBoByNaam(String naam) {
        Bo bo = getBoByNaam(naam);
        incrementBo(bo);
    }

    private void incrementBo(Bo bo) {
        int countOrig = bo.getAantal();
        setBoCount(bo, countOrig + 1);
    }

    public void resetBoByAlfaIndex(int index) {
        List<Bo> alfa = getBsAlphabetic();
        Bo bo = alfa.get(index);
        setBoCount(bo, 0);
    }

    public void resetBoByNaam(String naam) {
        Bo bo = getBoByNaam(naam);
        setBoCount(bo, 0);
    }

    private void setBoCount(Bo bo, int count) {
        int countOrig = bo.getAantal();
        bo.setAantal(count);

        ItemCountChangedEvent e = new ItemCountChangedEvent(clock.now(), bo.getId(), countOrig, count);
        registerEvent(e);
    }

    public void deleteBoByAlfaIndex(int index) {
        List<Bo> alfa = getBsAlphabetic();
        Bo bo = alfa.get(index);
        deleteBo(bo);
    }

    public void deleteBoByNaam(String naam) {
        Bo bo = getBoByNaam(naam);
        deleteBo(bo);
    }

    private void deleteBo(Bo bo) {
        Set<ItemInShop> removedShopItems = deleteBoLogic(bo);

        ItemDeletedEvent e = new ItemDeletedEvent(clock.now(), bo, removedShopItems);
        registerEvent(e);
    }

    /** Triggers no event, just the state change, returns removed shopitems */
    Set<ItemInShop> deleteBoLogic(Bo bo) {
        Set<ItemInShop> removedShopItems = new HashSet<ItemInShop>();

        for (Wnk wnk : wnks) {
            Optional<ItemInShop> removedShopItem = removeBoFromWnkLogic(bo.getNaam(), wnk);
            CollectionUtils.addIgnoreNull(removedShopItems, removedShopItem.orNull());
        }

        bs.remove(bo.getNaam());

        return removedShopItems;
    }

    public int sizeOfBs() {
        return bs.values().size();
    }

    public Bo getBoByNaam(String naam) {
        return bs.get(naam);
    }

    public Bo getBoByNaamIgnoreCase(final String naam) {
        Set<String> names = bs.keySet();
        String found = CollectionUtils.find(names, new CollectionUtils.Predicate<String>() {
            @Override
            public boolean evaluate(String n) {
                return n.equalsIgnoreCase(naam);
            }
        });

        if (found == null) {
            return null;
        }

        return bs.get(found);
    }

    public void changeBoNaam(String van, String naar) {
        if (!StringUtils.equal(van, naar)) {
            Bo bo = bs.get(van);
            changeBoNaamLogic(van, naar, bo);

            ItemNameChangedEvent e = new ItemNameChangedEvent(clock.now(), bo.getId(), van, naar);
            registerEvent(e);
        }
    }

    /** Triggers no event, just the state change */
    void changeBoNaamLogic(String van, String naar, Bo bo) {
        bo.setNaam(naar);
        bs.remove(van);
        bs.put(naar, bo);
    }

    /**
     * Geeft alle unieke bestaande categorien van de boodschappen alfabetisch.
     */
    public List<String> getCategories() {
        Set<String> catsSet = new HashSet<String>();
        for (Bo bo : bs.values()) {
            if (!Utils.isEmpty(bo.getCategorie())) {
                catsSet.add(bo.getCategorie());
            }
        }
        List<String> catsList = new ArrayList<String>(catsSet);
        Collections.sort(catsList);
        return catsList;
    }

    public boolean canUndo() {
        return !undoStack.isEmpty();
    }

    /**
     * Compensate the last user action with a counter action.
     * 
     * @return the undone event or null if nothing to undo
     */
    public BoLiEvent undo() {
        if (!canUndo()) {
            return null;
        }

        BoLiEvent toUndo = undoStack.pop();
        BoLiEvent compensation = toUndo.createCompensation(clock.now());
        eventsToPublish.add(compensation);
        processEventLogic(compensation);
        return toUndo;
    }

    /** Takes over the shops and items of the toMerge to prevent object creation. */
    public void merge(BoLi toMergeAndBreak) {
        mergeBs(toMergeAndBreak.bs);

        mergeWnks(toMergeAndBreak.wnks);

        this.undoStack.clear();
        this.eventsToPublish.clear();

        if(changeListener.isPresent()) {
            changeListener.get().fullPull();
        }
    }

    /** steal wnks */
    private void mergeWnks(List<Wnk> wnks) {
        CollectionUtils.forAllDo(wnks, new CollectionUtils.Closure<Wnk>() {
            @Override
            public void execute(Wnk w) {
                w.setBoli(BoLi.this);
            }
        });

        this.wnks.clear();
        this.wnks.addAll(wnks);
    }

    /** steal bs */
    private void mergeBs(Map<String, Bo> bs) {
        CollectionUtils.forAllDo(bs.values(), new CollectionUtils.Closure<Bo>() {
            @Override
            public void execute(Bo b) {
                b.setBoli(BoLi.this);
            }
        });

        this.bs.clear();
        this.bs.putAll(bs);
    }

    /**
     * Returns the events for publication.
     */
    public List<BoLiEvent> getEventsToPublish() {
        List<BoLiEvent> result = new ArrayList<BoLiEvent>(eventsToPublish);
        return result;
    }

    // void addEventsToPublish(List<BoLiEvent> events) {
    // this.eventsToPublish.addAll(events);
    // }

    /** Removes events from the publish-events of this boli */
    public void removeEventsToPublish(List<BoLiEvent> events) {
        eventsToPublish.removeAll(events);
    }

    public void clearAllEventsToPublish() {
        eventsToPublish.clear();
    }

    /** Merge the events into the state of this boli. */
    public synchronized void processEvents(List<BoLiEvent> events) {
        for (BoLiEvent evt : events) {
            processEventLogic(evt);
        }

        if (changeListener.isPresent() && events.size() > 0) {
            changeListener.get().eventsConsumed(events.size());
        }
    }

    private void processEventLogic(BoLiEvent evt) {
        evt.accept(this);
    }

    /** Notification from a Bo */
    public void boCategoryChanged(String id, String oldCat, String newCat) {
        ItemCategoryChangedEvent e = new ItemCategoryChangedEvent(clock.now(), id, oldCat, newCat);
        registerEvent(e);
    }

    /** Notification from a Wnk */
    void wnkNameChanged(String id, String oldName, String newName) {
        ShopNameChangedEvent e = new ShopNameChangedEvent(clock.now(), id, oldName, newName);
        registerEvent(e);
    }

    /** Notification from a Wnk */
    void boMoved(String shopname, String boname, int oldIndex, int newIndex) {
        Wnk wnk = getWnk(shopname);
        Bo bo = getBoByNaam(boname);
        ShopItemMovedEvent e = new ShopItemMovedEvent(clock.now(), wnk.getId(), bo.getId(), oldIndex,
                newIndex);
        registerEvent(e);
    }

    @Override
    public void visit(ItemCountChangedEvent icce) {
        if (hasBo(icce.getItemId())) {
            Bo bo = getBoById(icce.getItemId());
            bo.setAantal(icce.getNewCount());
        }
    }

    @Override
    public void visit(ItemCategoryChangedEvent icce) {
        if (hasBo(icce.getItemId())) {
            Bo bo = getBoById(icce.getItemId());
            bo.setCategoryLogic(icce.getNewCat());
        }
    }

    @Override
    public void visit(ItemNameChangedEvent ince) {
        if (hasBo(ince.getItemId())) {
            Bo bo = getBoById(ince.getItemId());
            changeBoNaamLogic(ince.getOldName(), ince.getNewName(), bo);
        }
    }

    @Override
    public void visit(ItemAddedEvent iae) {
        addBoLogic(iae.getBo());
        for (ItemInShop iis : iae.getShopItems()) {
            Wnk wnk = getWnkById(iis.getShopId());
            if (wnk != null) {
                addBoToWnkLogic(iae.getBo().getNaam(), wnk, iis.getItemPosition());
            }
        }
    }

    @Override
    public void visit(ItemDeletedEvent ide) {
        deleteBoLogic(ide.getBo());
    }

    @Override
    public void visit(ShopAddedEvent sae) {
        addWnkLogic(sae.getWnk());
    }

    @Override
    public void visit(ShopDeletedEvent sde) {
        deleteWnkLogic(sde.getWnk().getNaam());
    }

    @Override
    public void visit(ShopNameChangedEvent sce) {
        if (hasWnk(sce.getShopId())) {
            getWnkById(sce.getShopId()).setNaamLogic(sce.getNewName());
        }
    }

    @Override
    public void visit(ShopItemMovedEvent sime) {
        if (hasWnk(sime.getShopId())) {
            Bo bo = getBoById(sime.getItemId());
            getWnkById(sime.getShopId()).toIndexLogic(bo.getNaam(), sime.getNewIndex());
        }
    }

    @Override
    public void visit(ShopItemAddedEvent siae) {
        if (hasWnk(siae.getShopId()) && hasBo(siae.getItemId())) {
            Bo bo = getBoById(siae.getItemId());
            getWnkById(siae.getShopId()).addBo(bo.getNaam());
        }
    }

    @Override
    public void visit(ShopItemRemovedEvent sire) {
        Bo bo = getBoById(sire.getItemId());
        getWnkById(sire.getShopId()).remove(bo.getNaam());
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((bs == null) ? 0 : bs.hashCode());
        result = prime * result + ((wnks == null) ? 0 : wnks.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        BoLi other = (BoLi) obj;
        if (bs == null) {
            if (other.bs != null)
                return false;
        } else if (!bs.equals(other.bs))
            return false;
        if (wnks == null) {
            if (other.wnks != null)
                return false;
        } else if (!wnks.equals(other.wnks))
            return false;
        return true;
    }

    @Override
    public boolean equalsValues(BoLi obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        BoLi other = (BoLi) obj;
        if (bs == null) {
            if (other.bs != null)
                return false;
        } else if (!bs.equals(other.bs))
            return false;
        if (wnks == null) {
            if (other.wnks != null)
                return false;
        } else if (!wnks.equals(other.wnks))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "BoLi [bs=" + bs + ", wnks=" + wnks + "]";
    }
}
