package bs.boli.domain;

/**
 * Removes the oldest item if reaches max size.
 * 
 * @param <E>
 */
public class OverflowStack<E> extends Stack<E> {

    private final int maxSize;

    // this one is for xstream
    @SuppressWarnings("unused")
    private OverflowStack() {
        super();
        maxSize = 3;
    }

    public OverflowStack(int maxSize) {
        super();
        this.maxSize = maxSize;
    }

    public void push(E object) {
        super.push(object);
        if (super.size() > maxSize) {
            list.removeLast();
        }
    }
}
