package bs.boli.domain;

import java.util.Date;

public final class ShopItemMovedEvent extends BoLiEvent {
    private final String shopId, itemId;
    private final int oldIndex, newIndex;

    /** For XStream, runs in 'Pure Java' mode on android */
    private ShopItemMovedEvent() {
        super(null);
        this.shopId = null;
        this.itemId = null;
        this.oldIndex = 0;
        this.newIndex = 0;
    }

    public ShopItemMovedEvent(Date eventTime, String shopId, String itemId, int oldIndex, int newIndex) {
        super(eventTime);
        this.shopId = shopId;
        this.itemId = itemId;
        this.oldIndex = oldIndex;
        this.newIndex = newIndex;
    }

    @Override
    public void accept(EventVisitor v) {
        v.visit(this);
    }

    public String getShopId() {
        return shopId;
    }

    public int getOldIndex() {
        return oldIndex;
    }

    public int getNewIndex() {
        return newIndex;
    }

    public String getItemId() {
        return itemId;
    }

    @Override
    public BoLiEvent createCompensation(Date eventTime) {
        BoLiEvent comp = new ShopItemMovedEvent(eventTime, shopId, itemId, newIndex, oldIndex);
        return comp;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((itemId == null) ? 0 : itemId.hashCode());
        result = prime * result + newIndex;
        result = prime * result + oldIndex;
        result = prime * result + ((shopId == null) ? 0 : shopId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        ShopItemMovedEvent other = (ShopItemMovedEvent) obj;
        if (itemId == null) {
            if (other.itemId != null)
                return false;
        } else if (!itemId.equals(other.itemId))
            return false;
        if (newIndex != other.newIndex)
            return false;
        if (oldIndex != other.oldIndex)
            return false;
        if (shopId == null) {
            if (other.shopId != null)
                return false;
        } else if (!shopId.equals(other.shopId))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "ShopItemMovedEvent [shopId=" + shopId + ", itemId=" + itemId + ", oldIndex=" + oldIndex
                + ", newIndex=" + newIndex + "]";
    }

}
