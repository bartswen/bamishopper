package bs.boli.domain;

import java.util.Date;

public final class ShopAddedEvent extends BoLiEvent {
    private final Wnk wnk;

    /** For XStream, runs in 'Pure Java' mode on android */
    private ShopAddedEvent() {
        super(null);
        this.wnk = null;
    }

    /** Pass the original Wnk, this conctructor will clone it */
    public ShopAddedEvent(Date eventTime, Wnk wnk) {
        super(eventTime);
        this.wnk = wnk.cloneAsOrphan();
    }

    public Wnk getWnk() {
        return wnk;
    }

    @Override
    public void accept(EventVisitor v) {
        v.visit(this);
    }

    @Override
    public BoLiEvent createCompensation(Date eventTime) {
        BoLiEvent comp = new ShopDeletedEvent(eventTime, wnk);
        return comp;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((wnk == null) ? 0 : wnk.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        ShopAddedEvent other = (ShopAddedEvent) obj;
        if (wnk == null) {
            if (other.wnk != null)
                return false;
        } else if (!wnk.equals(other.wnk))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "ShopAddedEvent [wnk=" + wnk + "]";
    }

}
