package bs.boli.domain;

import java.lang.reflect.Field;


/**
 * Represents persistence format.
 */
public final class Item {
    public String id;
    public String name;
    public int count;
    public String category;

    public static Item fromBo(Bo bo) {
        Item i = new Item();
        i.id = bo.getId();
        i.name = bo.getNaam();
        i.count = bo.getAantal();
        i.category = bo.getCategorie();
        return i;
    }

    public Bo toBo() {
        Bo bo = new Bo(name, count);
        bo.setCategorie(category);
        Field boIdField = FieldUtils.getField(Bo.class, "id", true);
        try {
            FieldUtils.writeField(boIdField, bo, id, true);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
        return bo;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getCount() {
        return count;
    }

    public String getCategory() {
        return category;
    }
}
