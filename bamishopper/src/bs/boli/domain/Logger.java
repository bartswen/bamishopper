package bs.boli.domain;

public interface Logger {
    void fatal(String context, String message);

    void error(String context, String message);

    void warn(String context, String message);

    void info(String context, String message);

    void debug(String context, String message);

    void trace(String context, String message);
}
