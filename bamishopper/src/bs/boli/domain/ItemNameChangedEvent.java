package bs.boli.domain;

import java.util.Date;

public final class ItemNameChangedEvent extends BoLiEvent {
    private String itemId;
    private String oldName, newName;

    /** For XStream, runs in 'Pure Java' mode on android */
    private ItemNameChangedEvent() {
        super(null);
    }

    public ItemNameChangedEvent(Date eventTime, String itemId, String oldName, String newName) {
        super(eventTime);
        this.itemId = itemId;
        this.oldName = oldName;
        this.newName = newName;
    }

    @Override
    public void accept(EventVisitor v) {
        v.visit(this);
    }

    public String getItemId() {
        return itemId;
    }

    public String getOldName() {
        return oldName;
    }

    public String getNewName() {
        return newName;
    }

    @Override
    public BoLiEvent createCompensation(Date eventTime) {
        BoLiEvent comp = new ItemNameChangedEvent(eventTime, itemId, newName, oldName);
        return comp;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((itemId == null) ? 0 : itemId.hashCode());
        result = prime * result + ((newName == null) ? 0 : newName.hashCode());
        result = prime * result + ((oldName == null) ? 0 : oldName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        ItemNameChangedEvent other = (ItemNameChangedEvent) obj;
        if (itemId == null) {
            if (other.itemId != null)
                return false;
        } else if (!itemId.equals(other.itemId))
            return false;
        if (newName == null) {
            if (other.newName != null)
                return false;
        } else if (!newName.equals(other.newName))
            return false;
        if (oldName == null) {
            if (other.oldName != null)
                return false;
        } else if (!oldName.equals(other.oldName))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "ItemNameChangedEvent [itemId=" + itemId + ", oldName=" + oldName + ", newName=" + newName
                + "]";
    }
}
