package bs.boli.domain;

import java.util.LinkedList;

public class Stack<E> {
    protected LinkedList<E> list = new LinkedList<E>();

    public void push(E object) {
        list.addFirst(object);
    }

    public E pop() {
        return list.removeFirst();
    }

    public E peek() {
        return list.getFirst();
    }

    public int size() {
        return list.size();
    }

    public boolean isEmpty() {
        return list.isEmpty();
    }

    public void clear() {
        list.clear();
    }
}
