package bs.boli.domain;

public interface EventVisitor {
    void visit(ItemCountChangedEvent icce);
    void visit(ItemCategoryChangedEvent icce);
    void visit(ItemNameChangedEvent ince);
    void visit(ItemAddedEvent iae);
    void visit(ItemDeletedEvent ide);

    void visit(ShopAddedEvent sae);
    void visit(ShopDeletedEvent sde);
    void visit(ShopNameChangedEvent sce);
    void visit(ShopItemMovedEvent sire);
    void visit(ShopItemAddedEvent siae);
    void visit(ShopItemRemovedEvent sire);
}
