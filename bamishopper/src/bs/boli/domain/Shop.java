package bs.boli.domain;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;


/**
 * Represents persistence format.
 */
public final class Shop {
    private String id;
    private String name;
    private List<String> itemIds = new ArrayList<String>();

    /**
     * Converts a Wnk to a Shop.
     * @param wnk the wnk to convert
     * @param boli the BoLi to read the Bo's from when converting the bo's in the wnk to ShopItems
     */
    public static Shop fromWnk(Wnk wnk, final BoLi boli) {
        Shop s = new Shop();
        s.id = wnk.getId();
        s.name = wnk.getNaam();

        CollectionUtils.Transformer<String, String> bonameToItemId = new CollectionUtils.Transformer<String, String>() {
            @Override
            public String transform(String boname) {
                Bo bo = boli.getBoByNaam(boname);
                String itemId = bo.getId();
                return itemId;
            }
        };
        List<String> itemsInShop = CollectionUtils.collect(wnk.collectBoNames(), bonameToItemId);
        s.itemIds = itemsInShop;
        return s;
    }

    public Wnk toWnk(final BoLi boli) {
        Wnk wnk = new Wnk(name);
        idToWnk(wnk);
        itemsToWnk(wnk, boli);
        return wnk;
    }

    private void itemsToWnk(Wnk wnk, final BoLi boli) {
        CollectionUtils.Transformer<String, String> itemIdToBoname = new CollectionUtils.Transformer<String, String>() {
            @Override
            public String transform(String itemId) {
                Bo bo = boli.getBoById(itemId);
                String boname = bo.getNaam();
                return boname;
            }
        };
        List<String> bonames = CollectionUtils.collect(itemIds, itemIdToBoname);

        Field wnkItemsField = FieldUtils.getField(Wnk.class, "bs", true);
        try {
            FieldUtils.writeField(wnkItemsField, wnk, bonames, true);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    private void idToWnk(Wnk wnk) {
        Field wnkIdField = FieldUtils.getField(Wnk.class, "id", true);
        try {
            FieldUtils.writeField(wnkIdField, wnk, id, true);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<String> getItemIds() {
        return itemIds;
    }
    
}
