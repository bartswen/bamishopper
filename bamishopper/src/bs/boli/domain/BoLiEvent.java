package bs.boli.domain;

import java.util.Date;

public abstract class BoLiEvent {
    private Date eventTime;

    public BoLiEvent(Date eventTime) {
        super();
        this.eventTime = eventTime;
    }
    
    public abstract void accept(EventVisitor v);

    public abstract BoLiEvent createCompensation(Date eventTime);

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((eventTime == null) ? 0 : eventTime.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        BoLiEvent other = (BoLiEvent) obj;
        if (eventTime == null) {
            if (other.eventTime != null)
                return false;
        } else if (!eventTime.equals(other.eventTime))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "BoLiEvent [eventTime=" + eventTime + "]";
    }
}
