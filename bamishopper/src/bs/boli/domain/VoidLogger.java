package bs.boli.domain;

public class VoidLogger implements Logger {

    @Override
    public void fatal(String context, String message) {
    }

    @Override
    public void error(String context, String message) {
    }

    @Override
    public void warn(String context, String message) {
    }

    @Override
    public void info(String context, String message) {
    }

    @Override
    public void debug(String context, String message) {
    }

    @Override
    public void trace(String context, String message) {
    }
}
