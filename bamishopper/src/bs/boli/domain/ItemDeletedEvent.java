package bs.boli.domain;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public final class ItemDeletedEvent extends BoLiEvent {
    private final Bo bo;
    private final Set<ItemInShop> shopItems=new HashSet<ItemInShop>();

    /** For XStream, runs in 'Pure Java' mode on android */
    private ItemDeletedEvent() {
        super(null);
        this.bo = null;
    }

    /** Pass the original Bo, this conctructor will clone it */
    public ItemDeletedEvent(Date eventTime, Bo bo, Set<ItemInShop> shopItems) {
        super(eventTime);
        this.bo = bo.cloneAsOrphan();
        this.shopItems.addAll(shopItems);
    }

    @Override
    public void accept(EventVisitor v) {
        v.visit(this);
    }

    public Bo getBo() {
        return bo;
    }

    public Set<ItemInShop> getShopItems() {
        return shopItems;
    }

    @Override
    public BoLiEvent createCompensation(Date eventTime) {
        BoLiEvent comp = new ItemAddedEvent(eventTime, bo, shopItems);
        return comp;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((bo == null) ? 0 : bo.hashCode());
        result = prime * result + ((shopItems == null) ? 0 : shopItems.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        ItemDeletedEvent other = (ItemDeletedEvent) obj;
        if (bo == null) {
            if (other.bo != null)
                return false;
        } else if (!bo.equals(other.bo))
            return false;
        if (shopItems == null) {
            if (other.shopItems != null)
                return false;
        } else if (!shopItems.equals(other.shopItems))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "ItemDeletedEvent [bo=" + bo + ", shopItems=" + shopItems + "]";
    }
}
