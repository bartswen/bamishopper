Bamishopper is an Android 2.3 app to manage a shopping list. It works but it is not in the Play store. There are still a lot of possible improvements, but I was happy to get it in a 'good enough' shape to actually start using it with multiple users.

**I wrote it because**

1. The existing apps did not meet my requirements
2. I wanted to get experience with designing a medium-complex Android app
3. I wanted to get experience with event based systems

**Main features**

*Note: Shopping items are the things that you possibly need to buy*

1. Two lists, one for managing the shopping items list and one to use while doing the shopping
2. Specify which shops to bind a shopping item to
3. Synchronize between multiple Android devices

**Main challenges**

1. Asynchronous processing because UI *must always* be responsive
2. Using Dropbox as messaging middleware to register app instances and to exchange events
3. Multiple users cause conflicting events. Define a strategy to resolve these conflicts
4. The Android OS is in control, not the app. How to make it really robust. For example, how to prevent file-corruption when Android kills the app while it is writing a file.


